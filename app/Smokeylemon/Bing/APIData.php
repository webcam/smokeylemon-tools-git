<?php


namespace Smokeylemon\Bing;


use DOMDocument;

/**
 *  PHP class for downloading CSV files from Google Webmaster Tools.
 *
 *  This class does NOT require the Zend gdata package be installed
 *  in order to run.
 *
 *  Copyright 2012 eyecatchUp UG. All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 * @author: Stephan Schmitz <eyecatchup@gmail.com>
 * @link:   https://code.google.com/p/php-webmaster-tools-downloads/
 * @link:   https://github.com/eyecatchup/php-webmaster-tools-downloads/
 */
class APIData
{

    private $apiKey;

    function __construct()
    {
        $this->apiKey = '23d04dcfcff74da7921bb6517c563846';
    }



    public function GetData($service, $extra = false)
    {

        if (isset($extra)) {
            $extraParms = $extra;
        } else {
            $extraParms = '';
        }

        $host = 'ssl.bing.com';
        // if (self::IsLoggedIn() === true) {
        $url = $host . '/webmaster/api.svc/json/' . $service . '?apikey=' . $this->apiKey . $extraParms;
        //$head = array("Authorization: GoogleLogin auth=" . $this->_auth,
        //    "GData-Version: 2");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, true);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
        $result = curl_exec($ch);

        // dd($result);

        $info = curl_getinfo($ch);
        curl_close($ch);


        return ($info['http_code'] != 200) ? false : $result;
        //  } else {
        //      return false;
        //  }
    }


}

?>