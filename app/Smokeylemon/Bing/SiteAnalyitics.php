<?php
/**
 * Created by PhpStorm.
 * User: cam
 * Date: 30/01/14
 * Time: 1:06 PM
 */

namespace Smokeylemon\Bing;

use Smokeylemon\Bing\APIData;
use utilphp\util;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;


/**
 * Class SiteAnalyitics
 * @package Smokeylemon\Bing
 */
class SiteAnalyitics
{


    /**
     * @return array
     */
    public function build()
    {

        $filecacheName = 'BingSites';


        if (Cache::has($filecacheName)) {

            $domian = Cache::get($filecacheName);


        } else {

            $expiresAt = Carbon::now()->addMinutes(240);

            $bdata = new APIData();

            $sites = $bdata->GetData('GetUserSites');

            $jsite = json_decode($sites, true);

            $domian = array();

            foreach ($jsite['d'] as $site) {
                // $filename = basename($realfile);
                $domian[] = $site['Url'];
            }

            Cache::put($filecacheName, $domian, $expiresAt);

        }

        return $domian;

    }

    /**
     * @param $url
     * @return mixed
     */
    public function GetAService($serviceName, $url)
    {
        $apiData = new APIData();

        $rawData = $apiData->GetData($serviceName, '&siteUrl=' . $url);

        $data = json_decode($rawData, true);

        // Read the json results to find out our headers....

        $headers = array();

        foreach ($data['d'][0] as $key => $val) {
            $headers[] = $key;
        }

        // now read the rest to add in the data required...

        $siteData = array();

        foreach ($data['d'] as $newK => $value) {
            $siteData[][$newK] = $value;
        }


        //dd($siteData);

        self::generateCSV($headers, $siteData, $serviceName, self::sanitize($url));

        return $data;

    }

    /**
     * @param array $headers
     * @param array $list
     * @param $filename
     * @param $url
     */
    private function generateCSV(array $headers, array $list, $filename, $url)
    {
        $myCachedPath = storage_path() . '/cache/BingAPI/' . $url . '/';
        $myCachedFile = $myCachedPath . $filename . '.csv';

        if (!is_dir($myCachedPath)) {
            mkdir($myCachedPath, 0777, true);
        }

        $file = fopen($myCachedFile, "w");

        fputcsv($file, $headers);

        // dd($list);

        //dd($headers);

        //dd($list);

        dd($list);
        foreach ($list as $line) {

            //dd($line);
            fputcsv($file, $line);
        }

        fclose($file);
    }

    /**
     * @param $string
     * @return mixed|string
     */
    private function sanitize($string)
    {
        $match = array("/\s+/", "/[^a-zA-Z0-9\-]/", "/-+/", "/^-+/", "/-+$/");
        $replace = array("-", "", "-", "", "");
        $string = preg_replace($match, $replace, $string);
        $string = strtolower($string);
        return $string;
    }

    /**
     * @param $url
     * @return mixed
     */
    public function GetRankAndTrafficStats($url)
    {

        $apidata = new APIData();

        $rawdata = $apidata->GetData('GetRankAndTrafficStats', '&siteUrl=' . $url);

        $data = json_decode($rawdata, true);

        $siteData = array();

        foreach ($data['d'] as $site) {
            $siteData[] = array(
                $site['Clicks'],
                $site['Date'],
                $site['Impressions']
            );
        }

        self::generateCSV(array('Clicks', 'Date', 'Impressions'), $siteData, 'GetRankAndTrafficStats', self::sanitize($url));

        return $data;

    }

    /**
     * @param $domain
     * @return array
     */
    public function generateGraph($domain)
    {

        $filesWanted = array(
            'Content Keywords' => 'CONTENT_KEYWORDS-' . $domain,
            'External Links' => 'EXTERNAL_LINKS-' . $domain,
            'Internal Links' => 'INTERNAL_LINKS-' . $domain,
            'Top Pages' => 'TOP_PAGES-' . $domain,
            'Top Queries' => 'TOP_QUERIES-' . $domain
        );

        $domain = util::sanitize_string($domain);

        $result = array();

        foreach ($filesWanted as $type => $file) {

            $thefiles = storage_path() . '/cache/BingAPI/' . $domain . '/' . $file . '*';


            foreach (glob($thefiles) as $csv) {
                if (file_exists($csv)) {
                    $result[][$type] = $this->readCSV($csv);
                }

            }

            $newArray = array_merge_recursive($result, $filesWanted);

        }

        return $newArray;

    }

    /**
     * @param $csvFile
     * @return array
     */
    private function readCSV($csvFile)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle);
        }

        fclose($file_handle);

        return $line_of_text;
    }


}