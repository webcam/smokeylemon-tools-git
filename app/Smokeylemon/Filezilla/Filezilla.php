<?php
/**
 * Created by PhpStorm.
 * User: cam
 * Date: 4/04/14
 * Time: 9:59 AM
 */

namespace Smokeylemon\Filezilla;


class Filezilla
{

    public $username;
    public $password;
    public $server;
    public $accountName;


    public function createAccount($username, $password, $server, $accountName)
    {

        $this->username = $username;
        $this->password = $password;
        $this->server = $server;
        $this->accountName = $accountName;

        //dd(phpinfo());
        $currentFilezillaFile = app_path() . '/Smokeylemon/Dropbox/Config/sitemanager.xml';
        //$currentFilezillaFile = app_path() . '/sitemanagerDummy.xml';

        $sites = new \DomDocument();
        $sites->loadXML(file_get_contents($currentFilezillaFile));

        $sites->formatOutput = true;
        foreach ($sites->getElementsByTagName('Servers') as $key => $node) {

            $element = $sites->createElement("Server");
            $server = $node->appendChild($element);
            $this->nodeCreator('Host', $this->server, $server, $sites);
            $this->nodeCreator('Port', '21', $server, $sites);
            $this->nodeCreator('Protocol', 0, $server, $sites);
            $this->nodeCreator('Port', '21', $server, $sites);
            $this->nodeCreator('Type', '0', $server, $sites);
            $this->nodeCreator('User', $this->username, $server, $sites);
            $this->nodeCreator('Pass', $this->password, $server, $sites);
            $this->nodeCreator('Logontype', 1, $server, $sites);
            $this->nodeCreator('TimezoneOffset', 1, $server, $sites);
            $this->nodeCreator('PasvMode', 1, $server, $sites);
            $this->nodeCreator('MaximumMultipleConnections', 0, $server, $sites);
            $this->nodeCreator('EncodingType', 0, $server, $sites);
            $this->nodeCreator('PasvMode', 0, $server, $sites);
            $this->nodeCreator('Name', $this->accountName, $server, $sites);
            $this->nodeCreator('Comments', false, $server, $sites);
            $this->nodeCreator('LocalDir', false, $server, $sites);
            $this->nodeCreator('RemoteDir', false, $server, $sites);
            $this->nodeCreator('SyncBrowsing', '0', $server, $sites);
        }
        /// $results = $sites->saveXML();
        $result = $sites->save($currentFilezillaFile);
        if ($result === false) {
            error_log("Filezilla threw error: $result");
        } else {
            return 'success';
        }
    }


    public function nodeCreator($Name, $Value = false, $Parent, $DomParent)
    {

        // Port Element
        $element = $DomParent->createElement($Name);
        $tmpName = $Parent->appendChild($element);
        $element = $DomParent->createTextNode(($Value ? $Value : ''));
        $tmpName->appendChild($element);
    }


    public function updateAccount($username, $password)
    {

        //$this->server = $server;
        //$this->accountName = $accountName;

        //dd(phpinfo());
        $currentFilezillaFile = app_path() . '/Smokeylemon/Dropbox/Config/sitemanager.xml';
        //$currentFilezillaFile = app_path() . '/sitemanagerDummy.xml';

        $sites = new \DomDocument();
        $sites->loadXML(file_get_contents($currentFilezillaFile));
        $xpath = new \DOMXpath($sites);
        $Query = '//Server[User="' . $username . '"]';
        $currentAccount = $xpath->query($Query);

        $user = $xpath->query('.//User', $currentAccount->item(0));
        foreach ($user as $u) {
            $u->nodeValue = $username;
        }
        $pass = $xpath->query('.//Pass', $currentAccount->item(0));
        foreach ($pass as $p) {
            $p->nodeValue = $password;
        }

        $result = $sites->save($currentFilezillaFile);
        if ($result === false) {
            error_log("Filezilla threw error: $result");
        } else {
            return 'success';
        }
    }


}