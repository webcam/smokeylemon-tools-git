<?php
/**
 * Created by PhpStorm.
 * User: cam
 * Date: 18/12/13
 * Time: 2:17 PM
 */

namespace Smokeylemon\Server;

use Smokeylemon\Config\initServer;
use Symfony\Component\Console\Helper\Helper;

/**
 * Class Account
 * @package Smokeylemon\Server
 */
class EditAccount extends LiveServer
{


    /**
     * @var array
     */
    public $account_name;
    /**
     * @var
     */
    public $account_domain_name;
    /**
     * @var
     */
    public $server;
    /**
     * @var
     */
    public $dbname;


    function __construct($account_name, $server)
    {
        parent::__construct(initServer::configServer($server));

        $this->account_name = $account_name;

        $this->account_domain_name = $this->getMyDomain($this->account_name);

    }

    public function updateAccount($account_name, $password)
    {
        // deal with account base password first...
        $stringToSend = 'passwd?api.version=1&user='.$account_name.'&password='.$password.'&db_pass_update=1';
        $this->getServerAPIRequest($stringToSend);

        // deal with mysql password(s) ...


    }


}