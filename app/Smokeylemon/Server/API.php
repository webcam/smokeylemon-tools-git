<?php
/**
 * Created by PhpStorm.
 * User: Cameron
 * Date: 28/11/13
 * Time: 9:19 PM
 */


namespace Smokeylemon\Server;

use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;
use \utilphp\util;
use WfmTools;


/**
 * Class LiveServer
 *
 * @package Smokeylemon\Server
 */
class LiveServer
{

    /**
     * @var
     */
    protected $username;
    /**
     * @var
     */
    protected $hash;
    /**
     * @var
     */
    protected $domain;
    /**
     * @var
     */
    protected $ip;
    /**
     * @var
     */
    protected $keycase;

    /**
     * @param array $theServer
     */
    function __construct(Array $theServer)
    {
        $this->domain = $theServer['domain'];
        $this->hash = $theServer['hash'];
        $this->username = $theServer['username'];
        $this->ip = $theServer['ip'];
        $this->keycase = $theServer['keycase'];

    }

    public function getIp()
    {
        return $this->ip;
    }

    public function loadAverage()
    {

        $statusResult = $this->getServerAPIRequest('loadavg');

        $result = json_decode($statusResult);

        $endResult = $result->one . '&nbsp;' . $result->five . '&nbsp;' . $result->fifteen;

        return $endResult;


    }

    /**
     * @param      $queryToSend
     * @param bool $file
     * @param      $type
     * @param      $payload
     *
     * @return mixed
     */
    public function getServerAPIRequest($queryToSend, $file = false, $payload = false, $type = 'nonUAPI')
    {

        if ($type == 'nonUAPI') {
            $query = 'https://' . $this->domain . '/json-api/' . $queryToSend;
        } else {
            $query = 'https://' . str_replace("2087", "2083", $this->domain) . '/' . $queryToSend;
        }
        // if $file...
        if ($file) {
            $fp = fopen($file, 'w+');
            if ($fp === false) {
                return "Cannot open file => " . $file;
            }
        }

        $curl = curl_init();
        # Create Curl Object
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        # Allow certs that do not match the domain
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        # Allow self-signed certs
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        # Return contents of transfer on curl_exec
        $header[0] = "Authorization: WHM " . $this->username . ":" . preg_replace("'(\r|\n)'", "", $this->hash);
        # Remove newlines from the hash
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        //#dont cache..
        //curl_setopt($curl, CURLOPT_FRESH_CONNECT, TRUE);
        //Follow redirection
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        # Set curl header
        curl_setopt($curl, CURLOPT_URL, $query);
        # Set your URL
        // Set up a POST request with the payload.

        if ($payload) {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        }
        
        // if $file...
        if ($file) {
            curl_setopt($curl, CURLOPT_FILE, $fp);
        }

        $result = curl_exec($curl);

        # Execute Query, assign to $result
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }

        curl_close($curl);
        if ($file) {
            fclose($fp);
        }

        return $result;
    }


    public function service($serviceRequest)
    {


        $statusResult = $this->getServerAPIRequest('servicestatus');

        $result = json_decode($statusResult);


        //$serviceRequest = Input::get('service');

        if ($serviceRequest == 'mysql') {

            //14 is the mysql JSON DOM element...
            $selectedStatus = $result->service[14]->enabled;


            if ($selectedStatus == 1) {

                return '<span class="label label-success">Running</span>';

            } else {

                return 'Not Enabled';
            }


        }


        if ($serviceRequest == 'httpd') {
            //17 is the httpd JSON DOM element...
            $selectedStatus = $result->service[17]->enabled;

            if ($selectedStatus == 1) {

                return '<span class="label label-success">Running</span>';

            } else {

                return 'Not Enabled';
            }


        }


    }

    /**
     * @param int $cachedDays
     *
     * @return mixed|string
     */
    public function listAccounts($cachedDays = 4)
    {

        $friendlyFile = util::slugify($this->domain) . 'cache.json';

        $myCachedFile = storage_path() . '/cache/' . $friendlyFile;

        if ((!file_exists($myCachedFile)) or ((time() - filemtime($myCachedFile)) > ($cachedDays * 86400))) {
            // If older than 4 days, then lets get new info from cpanel...
            return $this->getServerAPIRequest('listaccts', $myCachedFile);
        }

        return $myCachedFile;

    }


    /**
     * Searches haystack for needle and
     * returns an array of the key path if
     * it is found in the (multidimensional)
     * array, FALSE otherwise.
     *
     * @mixed array_searchRecursive ( mixed needle,
     * array haystack [, bool strict[, array path]] )
     */

    /**
     * @return array
     */
    public function getMyDomains($nocache = false)
    {

        if ($nocache) {
            $cachedDays = -1; // days to cache
        } else {
            $cachedDays = 2; // days to cache
        }

        $friendlyFile = util::slugify($this->domain) . 'cache.json';

        $myCachedFile = storage_path() . '/cache/' . $friendlyFile;

        if ((!file_exists($myCachedFile)) or ((time() - filemtime($myCachedFile)) > ($cachedDays * 86400))) {
            // If older than 4 days, then lets get new info from cpanel...
            $this->getServerAPIRequest('listaccts', $myCachedFile);
        }

        // due to some issues we will run into later, we need to tap into WFM and grab the ID of the accounts..

        $WFM = WfmTools::getWFMAccountsIdXMLtoJSON();

        $json_b = json_decode($WFM, true);

        $wfmArray = $json_b;

        // dd($wfmArray);

        // First array of results collected into tmp array..

        // Lets get second array of results and tmp store them (tmp array)

        $doc = file_get_contents($myCachedFile);

        $json_a = json_decode($doc, true);

        $acct = $json_a['acct'];

        $DomainResults = array();

        function array_find_deep($array, $search, $keys = array())
        {
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $sub = array_find_deep($value, $search, array_merge($keys, array($key)));
                    if (count($sub)) {
                        return $value;
                        // return array_keys($key);
                    }


                } elseif (preg_match("/($search)/i", $value)) {
                    return array_merge($keys, array($key));
                }


                //  } elseif ($value === $search) {
                //      return array_merge($keys, array($key));
                //  }
            }

            return array();
        }


        if (is_array($acct)) {
            foreach ($acct as $value) {

                $whatToFind = $value['domain'];

                // does a search to retrive the numeric postion of the WFM Results array...

                $doSearch = array_find_deep($wfmArray, $whatToFind);

                $id = '';

                foreach ($doSearch as $found) {


                    if (in_array($found['id'], $found)) {
                        $id = $found['id'];
                    } else {
                        $id = '';
                    }
                }


                $tmp2 = array(
                    'domain' => $value["domain"],
                    'user'   => $value["user"],
                    'wfmId'  => $id
                );


                array_push($DomainResults, $tmp2);
            }


            return $DomainResults;

        }
    }

    /**
     * @param      $url
     * @param bool $vcheck
     *
     * @return mixed
     */
    public function getData($url, $vcheck = false)
    {

        if (Cache::has($url)) {

            $results = Cache::get($url);

            return $results;

        } else {

            $expiresAt = Carbon::now()->addMinutes(240);


            // create a new curl resource
            $ch = curl_init();
            // set URL and other appropriate options
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            //curl_setopt($ch, CURLOPT_VERBOSE, true);
            //$info = curl_getinfo($ch);
            // grab URL and pass it to the browser
            $content = curl_exec($ch);
            // I need the HTTP codes to see if 301 302 0r 404
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($http_code == '404' || $http_code == '401' || $http_code == '403') {
                $results['status'] = false;
                $results['message'] = '<b>Check not found</b>';
                Cache::put($url, $results, $expiresAt);
                return $results;
            }
            if ($vcheck === true) {
                if (strpos($content, 'VERSION:') === false) {
                    $results['status'] = false;
                    $results['message'] = '<b>Check not found</b>';
                    Cache::put($url, $results, $expiresAt);
                    return $results;
                }
            }

            // close curl resource, and free up system resources
            curl_close($ch);
            if ($content !== false) {

                $results['status'] = true;
                $results['message'] = $content;
                Cache::put($url, $results, $expiresAt);
                return $results;
            } else {
                $results['status'] = false;
                $results['message'] = '<b>Failed to get data</b>';
                Cache::put($url, $results, $expiresAt);
                return $results;
            }
        }

    }

    /**
     *
     * REQUIRED:
     *
     * PHP Method:
     *
     * Return servicestatus
     *
     * Add an Apache and MYSQL services restart button(s) if the load gets too high.
     * To be authenticated via JSON - have Success or Failure return display. Will be neat.
     *
     * So, create a restart service with service name of:
     *
     * named
     * interchange
     * ftpd
     * httpd
     * imap
     * cppop
     * exim
     * mysql
     * postgresql
     * sshd
     * tomcat
     *
     * More info at what each service does here: http://etwiki.cpanel.net/twiki/bin/view/11_30/WHMDocs/RestartServices
     *
     * just use the ftp (ftpd) debugging purposes..
     *
     *
     * Jquery AJAX Method:
     *
     * Perhaps do a 'post' mehod to restartService via a button... then return the staus to show sucess or failure...
     *
     * perhaps an 'Are you sure?' dailogue?
     *
     */


    function restartService($servicePosted)
    {


        $servicesAllowed = array("ftpd", "mysql", "httpd");

        if (in_array($servicePosted, $servicesAllowed)) {

            $this->getServerAPIRequest("restartservice?service=$servicePosted");

            $result = "$servicePosted has been restarted";

        } elseif ($servicesAllowed == 'inodes') {
            shell_exec("sh " . app_path() . "/Smokeylemon/Bash/sshClearInodes.sh $this->ip");
            $result = "Innodes have been cleared";
        } else {
            $result = "$servicePosted is not allowed in array";
        }
        return $result;

    }

    protected function get($name)
    {
        return $this->$name;
    }


}