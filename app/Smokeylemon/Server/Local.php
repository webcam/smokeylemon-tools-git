<?php
/*/**
 * Created by PhpStorm.
 * User: cam
 * Date: 17/12/13
 * Time: 2:40 PM
 */

namespace Smokeylemon\Server;

use Illuminate\Support\Facades\Crypt;
use ZipArchive;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use MiscTools;
use Smokeylemon\Config\initServer;
use Smokeylemon\Server\LiveServer;
use Smokeylemon\Server\Account;
use DOMDocument;

class Local
{

    public static function buildLocalDir($localDir, $type = false)
    {
        $localDir = '/home/Clients/' . $localDir . '/WWW/_site/';
        if ($type) {
            $localDir = $localDir . $type . '/';
        }
        return $localDir;
    }

    public static function getLastLineOfFile($file)
    {
        $line = '';

        $f = fopen($file, 'r');
        $cursor = -1;

        fseek($f, $cursor, SEEK_END);
        $char = fgetc($f);

        /**
         * Trim trailing newline chars of the file
         */
        while ($char === "\n" || $char === "\r") {
            fseek($f, $cursor--, SEEK_END);
            $char = fgetc($f);
        }

        /**
         * Read until the start of file or first newline char
         */
        while ($char !== false && $char !== "\n" && $char !== "\r") {
            /**
             * Prepend the new char
             */
            $line = $char . $line;
            fseek($f, $cursor--, SEEK_END);
            $char = fgetc($f);
        }

        return $line;
    }

    public static function getListings()
    {
// no caching for now..
        // $expiresAt = Carbon::now()->addMinutes(1);

        // if (Cache::has('localsites')) {
//dd('has cache');
        //     $theDir = Cache::get('localsites');


        // } else {
        //$dropdown = WfmTools::getWFMAccounts();

        $dir = "/home/Clients/*";
        // $dir = "C:/tmp/*";

        $theDir = array();
        foreach (glob($dir, GLOB_ONLYDIR) as $file) {
            $tmpFile = basename($file);
            $theDir[$tmpFile] = basename($file);
        };

        //    Cache::put('localsites', $theDir, $expiresAt);

        // }

        return $theDir;

    }

    public static function Zip($source, $destination)
    {


        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true) {
            $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST
            );

            foreach ($files as $file) {
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..'))) {
                    continue;
                }

                $file = realpath($file);

                if (is_dir($file) === true) {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                } else {
                    if (is_file($file) === true) {
                        $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                    }
                }
            }
        } else {
            if (is_file($source) === true) {
                $zip->addFromString(basename($source), file_get_contents($source));
            }
        }

        return $zip->close();
    }

    public static function remoteFileExists($url)
    {
        $curl = curl_init($url);

        //don't fetch the actual page, you only want to check the connection is ok
        curl_setopt($curl, CURLOPT_NOBODY, true);

        //do request
        $result = curl_exec($curl);

        $ret = false;

        //if request did not fail
        if ($result !== false) {
            //if request was ok, check response code
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($statusCode == 200) {
                $ret = true;
            }
        }

        curl_close($curl);

        return $ret;
    }


    public static function moveLocalToBackup($localDir, $type)
    {

        $today = date("d.m.y"); // 03.10.01

        $oldDirectory = Local::buildLocalDir($localDir, $type);

        if (is_dir($oldDirectory)) {
            $charcount = strlen($type) + 1; // count characters... and add an extra for the direcorry seperator
            $newDirectory = substr($oldDirectory, 0, -$charcount);
            $newDirectory = $newDirectory . $type . '_before_' . $today;
            if (is_dir($newDirectory)) {
                MiscTools::rmdir_recursive($newDirectory);
            }
            // export the local DB for backup purposes..
            if (!is_dir(dirname($newDirectory))) {
                mkdir(dirname($newDirectory), 0777, true);
            }
            self::exportSQLDB($localDir, $type);
            rename($oldDirectory, $newDirectory);
            if (is_dir($newDirectory) and !is_dir($oldDirectory)) {
                $result = true;
            } else {
                $result = false;
            }
            if (file_exists($oldDirectory)) {
                $doDelete = rmdir($oldDirectory);
                if ($doDelete) {
                    $result = true;
                }
            }

            return $result;

        }


    }

    public static function moveLiveToLocal($localDir, $type)
    {
        $oldDirectory = $localDir;
        $newDirectory = $localDir . '../WWW/_site/' . $type;

        if (is_dir($oldDirectory)) {

            if (!is_dir(dirname($newDirectory))) {
                mkdir(dirname($newDirectory), 0777, true);
            }

            rename($oldDirectory, $newDirectory);

            if (is_dir($oldDirectory)) {
                MiscTools::rmdir_recursive($oldDirectory);
            }

            $result = true;

            return $result;

        }

    }


    public static function extractFile($localDir, $zipFile, $type)
    {
        $fullZipFile = Local::buildLocalDir($localDir, $type) . $zipFile;
        $whereToExtract = Local::buildLocalDir($localDir, $type);
        $cmd = "tar -xf $fullZipFile -C $whereToExtract";
        shell_exec($cmd);
        return 'Success';

    }


    public static function exportSQLDB($localDir, $type)
    {

        $dbPath = Local::buildLocalDir($localDir, $type);

        $dbfile = 'SQLdump.sql.gz';

        $database = $localDir . '_' . $type;

        shell_exec('mysqldump -u smokeyexport -pninja2010 ' . $database . ' >' . $dbPath . $dbfile);

        $result = false;

        if (file_exists($dbPath . $dbfile)) {
            $result = true;
        } else {
            $result = false;
            trigger_error("Error with exportSQLDB DB:" . $database . ' DBPath:' . $dbPath . $dbfile, E_USER_ERROR);
        }

        return $result;


    }

    public static function fixPermissions($localDir)
    {

        return shell_exec('/home/SmokeyTools/bin/run_command.py fixpermissions ' . $localDir);

    }

    public static function importSQLDB($localDir, $type)
    {
        $dbfile = Local::buildLocalDir($localDir, $type) . 'SQLdump.sql.gz';
        $database = $localDir . '_' . $type;

        //TODO just replace this OTT method with a python command for database import
        //return shell_exec('/home/SmokeyTools/bin/run_command.py sqlimport ' . $database .' '. $dbfile);

        // done, so lets delete it
        unlink($dbfile);


        if (!file_exists($dbfile)) {
            $result = true;
        } else {
            $result = false;
            trigger_error("Error with importSQLDB", E_USER_ERROR);
        }

        return $result;


    }


    public static function disableHtaccessAndIndex($localDir, $type)
    {

        $path = Local::buildLocalDir($localDir, $type);

        $orgFile = $path . '.htaccess';

        if (file_exists($orgFile)) {
            $newFile = $path . '.htaccessORG';
            rename($orgFile, $newFile);
        }

        // any index.html/htm files??
        if (file_exists($path . 'index.html')) {
            unlink($path . 'index.html');
        } elseif (file_exists($path . 'index.htm')) {
            unlink($path . 'index.htm');
        }

        $result = true;

        return $result;


    }

    public static function deleteGzFiles($localDir, $type, $tmpFilename, $tmpSqlFilename)
    {
        $tmpFile = Local::buildLocalDir($localDir, $type) . $tmpFilename;
        $tmpSqlFile = Local::buildLocalDir($localDir, $type) . $tmpSqlFilename;

        if (is_file($tmpFile)) {
            unlink($tmpFile);
        }

        if (is_file($tmpSqlFile)) {
            unlink($tmpSqlFile);
        }

        return;

    }

    public static function deleteTmpFiles($localDir)
    {
        $type = 'cms';
        $cPath = Local::buildLocalDir($localDir, $type) . 'tmp/cache/';
        $tPath = Local::buildLocalDir($localDir, $type) . 'tmp/templates_c/';
        $ssTmp = Local::buildLocalDir($localDir, $type) . 'silverstipe_tmp/';

        if (file_exists($cPath)) {
            $files = glob($cPath . '*'); // get all file names
            foreach ($files as $file) { // iterate files
                if (is_file($file)) {
                    unlink($file);
                } // delete file
            }

            $files = glob($tPath . '*'); // get all file names
            foreach ($files as $file) { // iterate files
                if (is_file($file)) {
                    unlink($file);
                } // delete file
            }
        }
        if (file_exists($ssTmp)) {
            $files = glob($ssTmp . '*'); // get all file names
            foreach ($files as $file) { // iterate files
                if (is_file($file)) {
                    unlink($file);
                } // delete file
            }
        }

        return;

    }


    public static function listDirectoryAsTable($directory, $ext)
    {
        if (is_dir($directory)) {

            $files = glob($directory . '/' . '*.{' . $ext . '}', GLOB_BRACE);

            if ($files) {
                //print each file name
                $listing = '<table>';
                $listing .= '<thead>';
                $listing .= '<tr>';
                $listing .= '<th class="table-filename">Filename</th>';
                $listing .= '<th class="table-type">Type</th>';
                $listing .= '<th class="table-view">Quick View</th>';
                $listing .= '<th class="table-print">Print File</th>';
                $listing .= '</tr>';
                $listing .= '</thead>';
                foreach ($files as $file) {

                    if (is_file($file)) {

                        $theFile = pathinfo($file);

                        $full_file_name = $theFile['filename'] . '.' . $theFile['extension'];

                        $full_path_to_file = $theFile['dirname'] . DIRECTORY_SEPARATOR . $full_file_name;

                        $new_file_path = public_path() . DIRECTORY_SEPARATOR . 'files';

                        $new_file_url_path = asset('files') . DIRECTORY_SEPARATOR . $full_file_name;

                        if (!is_dir($new_file_path)) {
                            mkdir($new_file_path);
                        }

                        $newFile = $new_file_path . DIRECTORY_SEPARATOR . $full_file_name;

                        if (!copy($full_path_to_file, $newFile)) {
                            var_dump('failed to copy $file...');
                        }

                        $listing .= '<tr>';
                        $listing .= '<td>' . $theFile['filename'] . '</td>';
                        $listing .= '<td>' . $theFile['extension'] . '</td>';
                        if ($theFile['extension'] == 'txt') {
                            $fh = fopen($full_path_to_file, 'r');
                            $theData = fread($fh, filesize($full_path_to_file));
                            fclose($fh);
                            $listing .= '<td style="line-height:1.6em;">' . nl2br(self::makeClickableLinks($theData))
                                . '</td>';

                        } else {
                            $listing .= '<td><a href="' . $new_file_url_path . '" class="embed"></a></td>';
                        }
                        //$listing .= '<td><a href="' . $new_file_url_path . '" class="embed"></a></td>';
                        $listing .= '<td><a onclick="printFile(\'' . $new_file_url_path . '\')">Print File</a></td>';
                        $listing .= '</tr>';

                    } else {
                        $listing .= '';

                    }

                }
                $listing .= '</table>';

                return $listing;

            } else {

                return ' No directory located at ' . $directory;
            }

        }

    }

    public static function makeClickableLinks($s)
    {
        return preg_replace(
            '@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@',
            '<a href="$1" target="_blank">$1</a>', $s
        );
    }

    public static function recurse_dir_copy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    self::recurse_dir_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    public static function recurse_dir_delete($dir)
    {
        // Only allow delete inside client dir
        if (count(preg_grep("/(\/home\/Clients\/)/", explode("\n", $dir)))) {
            // Get all the file - even hidden files
            $files = array_merge(glob($dir . '/*'), glob($dir . '/{,.}[!.,!..]*', GLOB_MARK | GLOB_BRACE));
            // For each file
            foreach ($files as $file) {
                // If dir then recursive call self else del file
                if (is_dir($file)) {
                    self::recurse_dir_delete($file);
                } else {
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }
            }
            // Del directory
            rmdir($dir);
        } else {
            die('Not allowed!');
        }
    }

    public static function resize_image($file, $w, $h, $destfile, $bmp = false, $crop = false)
    {

        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width - ($width * abs($r - $w / $h)));
            } else {
                $height = ceil($height - ($height * abs($r - $w / $h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w / $h > $r) {
                $newwidth = $h * $r;
                $newheight = $h;
            } else {
                $newheight = $w / $r;
                $newwidth = $w;
            }
        }

        $im = ImageCreateFromPNG($file);
        $im_dest = imagecreatetruecolor($newwidth, $newheight);
        imagealphablending($im_dest, false);
        imagecopyresampled($im_dest, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        imagesavealpha($im_dest, true);
        if (file_exists($destfile)) {
            unlink($destfile);
        }
        if (!$bmp) {
            imagepng($im_dest, $destfile);
        } else {
            imagewbmp($im_dest, $destfile);
        }
        imagedestroy($im_dest);

    }

    public static function createLocal($clientPosted, $typePosted, $urlPosted = false)
    {
        //dd('here');

        //  return 'complete';

        if (!is_null($clientPosted)) {

            if ($typePosted == 'client') {
                $frontURL = 'Clients/' . $clientPosted;
            } elseif ($typePosted == 'cms') {
                $frontURL = 'http://' . $clientPosted . '.smokey.rocks/cms/';
                $adminURL = 'http://' . $clientPosted . '.smokey.rocks/cms/cmsadmin/';
            } elseif ($typePosted == 'opencart') {
                $frontURL = 'http://' . $clientPosted . '.smokey.rocks/cart/';
                $adminURL = 'http://' . $clientPosted . '.smokey.rocks/cart/cartadmin/';
            } elseif ($typePosted == 'opencart2') {
                $frontURL = 'http://' . $clientPosted . '.smokey.rocks/cart2/';
                $adminURL = 'http://' . $clientPosted . '.smokey.rocks/cart2/admin/';
            } elseif ($typePosted == 'mobile') {
                $frontURL = 'Clients/' . $clientPosted . '/Apps/projectName/';
            } elseif ($typePosted == 'website') {
                $frontURL = 'http://' . $clientPosted . '.smokey.rocks/cms/';
                $adminURL = 'http://' . $clientPosted . '.smokey.rocks/cms/admin/';
            } elseif ($typePosted == 'silverstripe') {
                $frontURL = 'http://' . $clientPosted . '.smokey.rocks/cms/';
                $adminURL = 'http://' . $clientPosted . '.smokey.rocks/cms/admin/';
            }
        }

        if ($typePosted == 'client') {

            exec('/home/SmokeyTools/bin/addClientFolders ' . $clientPosted . ' AuthPHPSciptFromSmokeyTools');

            $endResult = '<p><strong>Setup Completed</strong></p>';
            $endResult .= "<p>Your client folder path is:  <br/>$frontURL<br/></p>";

            return $endResult;

        } elseif ($typePosted == 'cms') {

            if ($urlPosted !== false && $urlPosted !== '') {

                exec(
                    '/home/SmokeyTools/bin/addcmsPHP ' . $clientPosted . ' AuthPHPSciptFromSmokeyTools ' . preg_replace(
                        '/https?:\/\/|www./', '', $urlPosted
                    )
                );

                $endResult = '<p><strong>Setup Completed</strong></p>';
                $endResult .= "<p>Your Frontend URL is: <br/><a target='_blank' href='" . $frontURL
                    . "'> $frontURL</a><br/>";
                $endResult .= "<p>Your Admin URL is:  <br/><a target='_blank' href='" . $adminURL
                    . "'> $adminURL</a><br/>";

                return $endResult;

            } else {

                return '<p><strong>Please make sure you have selected the site type, typed in the client directory name and supplied a live URL!</strong></p>';

            }

        } elseif ($typePosted == 'opencart') {

            exec('/home/SmokeyTools/bin/addopencartPHP ' . $clientPosted . ' AuthPHPSciptFromSmokeyTools');
            $endResult = '<p><strong>Setup Completed</strong></p>';
            $endResult .= "<p>Your Frontend URL is:  <br/><a target='_blank' href='" . $frontURL
                . "'> $frontURL</a><br/>";
            $endResult .= "<p>Your Admin URL is:  <br/><a target='_blank' href='" . $adminURL . "'> $adminURL</a><br/>";

            return $endResult;
        } elseif ($typePosted == 'opencart2') {

            exec('/home/SmokeyTools/bin/addopencart2PHP ' . $clientPosted . ' AuthPHPSciptFromSmokeyTools');
            $endResult = '<p><strong>Setup Completed</strong></p>';
            $endResult .= "<p>Your Frontend URL is:  <br/><a target='_blank' href='" . $frontURL
                . "'> $frontURL</a><br/>";
            $endResult .= "<p>Your Admin URL is:  <br/><a target='_blank' href='" . $adminURL . "'> $adminURL</a><br/>";

            return $endResult;
        } elseif ($typePosted == 'mobile') {

            exec('/home/SmokeyTools/bin/addionicPHP ' . $clientPosted . ' AuthPHPSciptFromSmokeyTools');
            $endResult = '<p><strong>Setup Completed</strong></p>';
            $endResult .= "<p>Your Ionic Mobile App path is:  <br/>$frontURL<br/></p>";

            return $endResult;
        } elseif ($typePosted == 'website') {

            exec('/home/SmokeyTools/bin/addsitePHP ' . $clientPosted . ' AuthPHPSciptFromSmokeyTools');
            $endResult = '<p><strong>Setup Completed</strong></p>';
            $endResult .= "<p>Your Frontend URL is: <br/><a target='_blank' href='" . $frontURL
                . "'> $frontURL</a><br/>";
            $endResult .= "<p>Your Admin URL is:  <br/><a target='_blank' href='" . $adminURL . "'> $adminURL</a><br/>";

            return $endResult;

        } elseif ($typePosted == 'silverstripe') {

            exec('/home/SmokeyTools/bin/addsilverstripePHP ' . $clientPosted . ' AuthPHPSciptFromSmokeyTools');
            $endResult = '<p><strong>Setup Completed</strong></p>';
            $endResult .= "<p>Your Frontend URL is: <br/><a target='_blank' href='" . $frontURL
                . "'> $frontURL</a><br/>";
            $endResult .= "<p>Your Admin URL is:  <br/><a target='_blank' href='" . $adminURL . "'> $adminURL</a><br/>";

            return $endResult;

        } else {

            return '<p><strong>Please select the site type or type in client directory</strong></p>';

        }

    }

    public static function importClient($siteToImport)
    {

        $isCMS = false;

        if ($siteToImport != 'select') {
            $siteToImport = htmlspecialchars($siteToImport);
        }

        if ($siteToImport != 'select') {

            $clientDir = '/home/Clients/' . $siteToImport;
            $clientSiteDir = '/home/Clients/' . $siteToImport . '/WWW/_site';

            if (is_dir($clientDir . '/WWW/_site/cms/')) {
                $isCMS = true;
            }

            if ($isCMS) {

                //Start final output
                $endResult = '<br><p><strong>Import Complete!</strong></p>';

                $dir = $clientSiteDir . '/*.webflow*.zip';
                $webflowFiles = array_filter(glob($dir), 'is_file');
                $webflowFilesCount = count($webflowFiles);
                if ($webflowFilesCount) {

                    if ($webflowFilesCount == 1) {
                        $endResult .= '<pre>' . $webflowFilesCount . ' Webflow file found!</pre>';
                    } else {
                        $endResult .= '<pre>' . $webflowFilesCount . ' Webflow files found!</pre>';
                    }

                    $webflowFiles = array_combine($webflowFiles, array_map("filemtime", $webflowFiles));
                    arsort($webflowFiles);
                    $webflowFile = key($webflowFiles);

                    $endResult .= '<pre>Webflow file being used => ' . $webflowFile . '</pre>';

                    $webflowFileExtract = $clientSiteDir . '/' . basename($webflowFile, '.zip') . '/';

                    $endResult .= '<pre>Webflow file extract dir => ' . $webflowFileExtract . '</pre>';

                    if (!is_dir($webflowFileExtract)) {

                        //Extract the zip file
                        exec('unzip ' . $webflowFile . ' -d ' . $webflowFileExtract);
                        self::fixPermissions($siteToImport);
                        $endResult .= '<pre>Webflow file extracted</pre>';

                        $cssSQL = '';
                        $cssDir = $webflowFileExtract . 'css/';
                        $cssFiles = glob($cssDir . '*.css');
                        foreach ($cssFiles as $cssFile) {
                            $cssBase = basename($cssFile);

                            if ($cssBase == 'normalize.css') {
                                $cssContent = file_get_contents($cssFile);
                                // Remove comments
                                $cssContent = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $cssContent);
                                // Remove space after colons
                                $cssContent = str_replace(': ', ':', $cssContent);
                                // Remove whitespace
                                $cssContent = str_replace(
                                    array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $cssContent
                                );
                                file_put_contents($cssFile, $cssContent);
                                $cssContent = str_replace("url('..", "url('[[assets_url]]farmeworks/wf", $cssContent);
                                $cssContent = str_replace("url(..", "url([[assets_url]]farmeworks/wf", $cssContent);
                                $cssContent = str_replace("'", "\'", $cssContent);
                                $cssSQL .= 'UPDATE `cms_css` SET `css_text` = \'' . $cssContent
                                    . '\' WHERE `css_name` = \'Normalize\';';
                            } else {
                                if ($cssBase == 'webflow.css') {
                                    $cssContent = file_get_contents($cssFile);
                                    // Remove comments
                                    $cssContent = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $cssContent);
                                    // Remove space after colons
                                    $cssContent = str_replace(': ', ':', $cssContent);
                                    // Remove whitespace
                                    $cssContent = str_replace(
                                        array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $cssContent
                                    );
                                    file_put_contents($cssFile, $cssContent);
                                    $cssContent = str_replace(
                                        "url('..", "url('[[assets_url]]farmeworks/wf", $cssContent
                                    );
                                    $cssContent = str_replace("url(..", "url([[assets_url]]farmeworks/wf", $cssContent);
                                    $cssContent = str_replace("'", "\'", $cssContent);
                                    $cssSQL .= 'UPDATE `cms_css` SET `css_text` = \'' . $cssContent
                                        . '\' WHERE `css_name` = \'Webflow\';';
                                } else {
                                    $cssContent = file_get_contents($cssFile);
                                    // Remove comments
                                    $cssContent = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $cssContent);
                                    // Remove space after colons
                                    $cssContent = str_replace(': ', ':', $cssContent);
                                    // Remove whitespace
                                    $cssContent = str_replace(
                                        array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $cssContent
                                    );
                                    file_put_contents($cssFile, $cssContent);
                                    $cssContent = str_replace(
                                        "url('..", "url('[[assets_url]]farmeworks/wf", $cssContent
                                    );
                                    $cssContent = str_replace("url(..", "url([[assets_url]]farmeworks/wf", $cssContent);
                                    $cssContent = str_replace("'", "\'", $cssContent);
                                    $cssSQL .= 'UPDATE `cms_css` SET `css_text` = \'' . $cssContent
                                        . '\' WHERE `css_name` = \'Webflow Custom\';';
                                }
                            }

                            $endResult .= '<pre>CSS files minified</pre>';

                            file_put_contents($webflowFileExtract . 'css/css.sql', $cssSQL);
                            exec(
                                '/home/SmokeyTools/bin/runSQLPHP ' . $siteToImport . ' AuthPHPSciptFromSmokeyTools '
                                . $webflowFileExtract . 'css/css.sql'
                            );

                            $endResult .= '<pre>CSS imported to the database</pre>';

                        }

                        $jsDir = $webflowFileExtract . 'js/';
                        $jsFiles = glob($jsDir . '*.js');
                        foreach ($jsFiles as $jsFile) {
                            // setup the URL
                            $url = 'https://javascript-minifier.com/raw';
                            //Build Post Data
                            $postData = array('http' => array(
                                'method'  => 'POST',
                                'header'  => 'Content-type: application/x-www-form-urlencoded',
                                'content' => http_build_query(array('input' => file_get_contents($jsFile)))));

                            $minified = file_get_contents($url, false, stream_context_create($postData));

                            // output the $minified
                            file_put_contents($jsFile, $minified);
                        }

                        $endResult .= '<pre>JS files minified</pre>';

                    } else {
                        return '<p><strong>The webflow file has already been extracted!</strong></p>';
                    }

                    $webflowFrameworkDir = $clientSiteDir . '/cms/assets/frameworks/wf';

                    //Remove old framework and copy new one
                    if (is_dir($webflowFrameworkDir)) {

                        $endResult .= '<pre>Webflow Framework dir => ' . $webflowFrameworkDir . '</pre>';
                        //As a backup lets archive it so we have something to go back to
                        self::archiveLocal($siteToImport);
                        $endResult .= '<pre>Archived the site to ensure we have a backup ' . $webflowFrameworkDir
                            . '</pre>';
                        self::recurse_dir_delete($webflowFrameworkDir);
                        $endResult .= '<pre>Removed old WF Framework Directory</pre>';

                    } else {

                        return '<p>No webflow framework directory found!</p>';

                    }

                    self::recurse_dir_copy($webflowFileExtract, $webflowFrameworkDir);
                    $endResult .= '<pre>Copied new webflow assets directory</pre>';

                    $faviconsDir = $clientSiteDir . '/cms/assets/favicons';
                    $endResult .= '<pre>Favicons dir => ' . $faviconsDir . '</pre>';

                    //New Master Favicon From Webflow File!
                    $masterFavicon = $webflowFileExtract . 'images/master-favicon.png';
                    $endResult .= '<pre>Master Favicon file => ' . $masterFavicon . '</pre>';

                    if (file_exists($masterFavicon)) {

                        list($width, $height) = getimagesize($masterFavicon);

                        if (($width * $height) < 67600) {
                            $endResult .= '<pre><strong style="color:#ff0000">Favicon file must be 260 x 260 px!</strong></pre>';
                        } else {

                            //Now lets remove the old Favicons
                            if (is_dir($faviconsDir)) {
                                self::recurse_dir_delete($faviconsDir);
                                $endResult .= '<pre>Removed old favicons directory</pre>';
                            }

                            //Create a new clean favicons folder
                            mkdir($faviconsDir);
                            $endResult .= '<pre>Created new favicons directory</pre>';

                            //Current set of favicon sizes and file names
                            $favicon_sizes = array(
                                '16'  => array(
                                    'favicon.ico',
                                    'favicon-16x16.png'
                                ),
                                '32'  => array(
                                    'favicon-32x32.png'
                                ),
                                '96'  => array(
                                    'favicon-96x96.png'
                                ),
                                '57'  => array(
                                    'apple-touch-icon-57x57.png'
                                ),
                                '60'  => array(
                                    'apple-touch-icon-60x60.png'
                                ),
                                '72'  => array(
                                    'apple-touch-icon-72x72.png'
                                ),
                                '76'  => array(
                                    'apple-touch-icon-76x76.png'
                                ),
                                '114' => array(
                                    'apple-touch-icon-114x114.png'
                                ),
                                '120' => array(
                                    'apple-touch-icon-120x120.png'
                                ),
                                '144' => array(
                                    'apple-touch-icon-144x144.png',
                                    'mstile-144x144.png'
                                ),
                                '152' => array(
                                    'apple-touch-icon-152x152.png'
                                ),
                                '180' => array(
                                    'apple-touch-icon-180x180.png'
                                ),
                                '192' => array(
                                    'android-chrome-192x192.png'
                                )
                            );

                            //Foreach favicon size
                            foreach ($favicon_sizes as $size => $files) {
                                //Foreach favicon file
                                foreach ($files as $file) {
                                    //Get new file name
                                    $fav_file = $faviconsDir . '/' . $file;
                                    //If not ico file
                                    if (substr_compare($file, 'ico', strlen($file) - strlen('ico'), strlen('ico'))
                                        === 0
                                    ) {
                                        //Save as PNG
                                        self::resize_image($masterFavicon, $size, $size, $fav_file);
                                    } else {
                                        //Else save as BMP (same as ICO)
                                        self::resize_image($masterFavicon, $size, $size, $fav_file, true);
                                    }
                                    //Report file created
                                    $endResult .= '<pre>Created favicon file: "' . $file . '"</pre>';
                                }
                            }

                        }

                    } else {
                        $endResult .= '<pre><strong>Could not locate the "master-favicon.png" file!</strong></pre>';
                    }

                    self::deleteTmpFiles($siteToImport);
                    $endResult .= '<pre>Cleared temp files</pre>';

                    return $endResult
                    . '<br><p><strong>Next Steps:</strong></p><p><ol><li>Find and replace the font families in the head of the template</li><li>Build tempalte layout from index.html file</li></ol></p><br>';

                } else {

                    return '<p>No webflow zip files found!</p>';

                }

            } else {
                return '<p>Not Supported for this website</p>';
            }

        } else {

            return '<p><strong>Please select the site</strong></p>';

        }

    }

    /*public static function uploadStaging($siteToUpload) {

        $isCMS  = false;
        $isCart = false;
        $isCart2 = false;
        $response = '';

        if ($siteToUpload != 'select') {
            $siteToUpload = htmlspecialchars($siteToUpload);
        }

        if ($siteToUpload != 'select') {

            $clientDir = '/home/Clients/' . $siteToUpload;

            if (is_dir($clientDir . '/WWW/_site/cms/')) {
                $isCMS = true;
            } elseif (is_dir($clientDir . '/WWW/_site/cart/')) {
                $isCart = true;
            } elseif (is_dir($clientDir . '/WWW/_site/cart2/')) {
                $isCart2 = true;
            }

            if ($isCMS || $isCart || $isCart2) {

                $dir = $clientDir . '/WWW/_site/site_to_upload_*';

                $theDir = glob($dir, GLOB_ONLYDIR);
                $exportDir = array_pop($theDir);

                if ($exportDir) {
                    $response .= '<pre>Directory Used For Upload => ' . $exportDir . '</pre>';

                    $web_files = $exportDir . '/website.zip';

                    if (file_exists($web_files)) {
                        $response .= '<pre>Zipped Files Used => ' . $web_files . '</pre>';
                    } else {
                        return '<p><strong style="color:#ff0000;">Could Not Access Zipped File => '. $web_files .'</strong></p>';
                    }

                    $web_db = $exportDir . '/database.sql.gz';

                    if (file_exists($web_db)) {
                        $response .= '<pre>Database Used => ' . $web_db . '</pre>';
                    } else {
                        return '<p><strong style="color:#ff0000;">Could Not Access Database => '. $web_db .'</strong></p>';
                    }

                    $liveServer = 'Tart';
                    $liveServerIP = '210.48.67.67';

                    $server = new LiveServer(initServer::configServer($liveServer));
                    $response .= '<pre>Server Used => '.$liveServer.'</pre>';

                    $accounts = $server->getMyDomains();
                    $staging_acc = array();

                    foreach($accounts as $index => $account) {
                        if($account['domain'] == 'staging.smokeylemon.com') {
                            $staging_acc = $accounts[$index];
                        }
                    }

                    $liveUserAcct = $staging_acc['user'];
                    $liveUserPass = 'iIfzF)7_V7@;';
                    $liveDomainAcct = $staging_acc['domain'];

                    if ($staging_acc != array()) {
                        $response .= '<pre>Server Account Used => Username: ' . $liveUserAcct . ' - Domain: ' . $liveDomainAcct . '</pre>';
                    } else {
                        return '<p><strong style="color:#ff0000;">Could Not Retrieve Staging Server Account</strong></p>';
                    }

                    $theAccount = new Account($liveUserAcct, $liveServer);

                    if ($theAccount) {

                        $response .= '<pre>Generate new SSH Key for ==> ' . $liveUserAcct . '</pre>';

                        $newSSHKey = $theAccount->getServerAPIRequest('cpanel?cpanel_jsonapi_user=' . $liveUserAcct . '&cpanel_jsonapi_module=SSH&cpanel_jsonapi_func=genkey&bits=1024&name=' . $liveUserAcct . '&pass='.urlencode($liveUserPass).'&type=rsa');
                        $json_a = json_decode($newSSHKey, true);
                        $newSSHKey = $json_a['cpanelresult']['event']['result'];
                        if ($newSSHKey == 1) {
                            $response .= '<pre  style="color:#009933;">New SSH Key has been generated!</pre>';
                        } else {
                            return '<p><strong style="color:#ff0000;">Could not generate new SSH Key!</strong></p>';
                        }

                        $response .= '<pre>Fetch new SSH Key for ==> ' . $liveUserAcct . '</pre>';

                        $newSSHKeyGet = $theAccount->getServerAPIRequest('cpanel?cpanel_jsonapi_user=' . $liveUserAcct . '&cpanel_jsonapi_module=SSH&cpanel_jsonapi_func=fetchkey&name=' . $liveUserAcct . '&pub=0');
                        $json_a = json_decode($newSSHKeyGet, true);
                        $newSSHKey = $json_a['cpanelresult']['data'][0]['key'];
                        $newSSHKeyGet = $json_a['cpanelresult']['event']['result'];
                        if ($newSSHKey != null && $newSSHKeyGet == 1) {
                            $response .= '<pre  style="color:#009933;">New SSH Key has been fetched!</pre>';
                        } else {
                            return '<p><strong style="color:#ff0000;">Could not fetch new SSH Key!</strong></p>';
                        }

                        $response .= '<pre>Write new SSH Key to file</pre>';

                        $sshKeyFileName = $exportDir . '/idsa_remote_rsa.pem';

                        $sshKeyFile = fopen( $sshKeyFileName , 'w') or die('Unable to open new ssh permission file!');
                        fwrite($sshKeyFile, $newSSHKey);
                        fclose($sshKeyFile);
                        self::fixPermissions($siteToUpload);

                        $response .= '<pre style="color:#009933;">SSH Key ==> '.$sshKeyFileName.'</pre>';


                        // Database - Upload
                        $siteToUploadDB = 'stagings_' . $siteToUpload;
                        $databases = $theAccount->getServerAPIRequest('cpanel?cpanel_jsonapi_user=' . $liveUserAcct . '&cpanel_jsonapi_module=MysqlFE&cpanel_jsonapi_func=listdbs');
                        $json_a = json_decode($databases, true);
                        $databases = $json_a['cpanelresult']['data'];
                        if ($databases) {

                            $response .= '<pre>Check if db for site to upload already exists in staging => '.$siteToUploadDB.'</pre>';

                            foreach($databases as $index => $database) {
                                if($database['db'] == $siteToUploadDB) {
                                    return '<p><strong style="color:#ff0000;">DB for site to upload already exists in staging!</strong></p>';
                                }
                            }

                            $response .= '<pre style="color:#009933;">DB does not yet exist in staging!</pre>';

                            $response .= '<pre>Create DB on Staging</pre>';

                            $newDB = $theAccount->getServerAPIRequest('cpanel?cpanel_jsonapi_user=' . $liveUserAcct . '&cpanel_jsonapi_module=MysqlFE&cpanel_jsonapi_func=createdb&db='.$siteToUploadDB);
                            $json_a = json_decode($newDB, true);
                            $newDB = $json_a['cpanelresult']['event']['result'];
                            if ($newDB == 1) {
                                $response .= '<pre  style="color:#009933;">DB has been created on staging ==> '.$siteToUploadDB.'</pre>';
                            } else {
                                return '<p><strong style="color:#ff0000;">Could not create DB on staging ==> '.$siteToUploadDB.'</strong></p>';
                            }

                            $response .= '<pre>Set DB User Privileges on Staging</pre>';

                            $newDBUsr = $theAccount->getServerAPIRequest('cpanel?cpanel_jsonapi_user=' . $liveUserAcct . '&cpanel_jsonapi_module=MysqlFE&&cpanel_jsonapi_func=setdbuserprivileges&db='.$siteToUploadDB.'&dbuser=stagings_db&privileges=ALL%20PRIVILEGES');
                            $json_a = json_decode($newDBUsr, true);
                            $newDBUsr = $json_a['cpanelresult']['event']['result'];
                            if ($newDBUsr == 1) {
                                $response .= '<pre  style="color:#009933;">DB User Privileges has been granted on staging ==> '.$siteToUploadDB.'</pre>';
                            } else {
                                return '<p><strong style="color:#ff0000;">Could not grant DB User Privileges on staging ==> '.$siteToUploadDB.'</strong></p>';
                            }

                        } else {
                            return '<p><strong style="color:#ff0000;">Could Not Retrieve Staging Server Account Databases</strong></p>';
                        }

                        // $response .= '<pre>Load DB File ==> '.$web_db.'</pre>';

                        // $ssh_command_sql = 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i '.$sshKeyFileName.' '.$liveUserAcct.'@'.$liveServerIP.' "mysql -u stagings_db -p'.$liveUserPass.' '.$siteToUploadDB.' < '.$web_db.'"';
                        // shell_exec($ssh_command_sql);
                        // $response .= '<pre style="color:#ff0000;">'.$ssh_command_sql.'</pre>';

                        // TODO ERROR? => ssh_exchange_identification: read: Connection reset by peer

                        //Files - Upload
                        $currDir = $theAccount->getServerAPIRequest('cpanel?cpanel_jsonapi_user=' . $liveUserAcct . '&cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=listfiles&checkleaf=1&dir=%2Fhome%2F' . $liveUserAcct . '%2Fpublic_html&types=dir');
                        $json_a = json_decode($currDir, true);
                        $currDir = $json_a['cpanelresult']['data'];
                        if ($currDir) {

                            $response .= '<pre>Check if site to upload has a folder on staging => '.$siteToUpload.'</pre>';

                            foreach($currDir as $index => $curr) {
                                if($curr['file'] == $siteToUpload) {
                                    return '<p><strong style="color:#ff0000;">Folder for site to upload already exists on staging! => '.$curr['fullpath'].'</strong></p>';
                                }
                            }

                            $response .= '<pre  style="color:#009933;">Folder does not yet exist on staging!</pre>';

                            $response .= '<pre>Create new folder on Staging => '.$siteToUpload.'</pre>';

                            $newDir = $theAccount->getServerAPIRequest('cpanel?cpanel_jsonapi_user=' . $liveUserAcct . '&cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=mkdir&path=public_html&name='.$siteToUpload);
                            $json_a = json_decode($newDir, true);
                            $newDir = $json_a['cpanelresult']['event']['result'];
                            if ($newDir == 1) {
                                $response .= '<pre  style="color:#009933;">Folder has been created on staging!</pre>';
                            } else {
                                return '<p><strong style="color:#ff0000;">Could not create folder for site to upload on staging!</strong></p>';
                            }

                            $response .= '<pre>Upload Zip File To Staging Folder</pre>';

                            // TODO: Remove FTP Upload (PASSWORD!!) ? (Faster than RSYNC!)

                            // connect and login to FTP server
                            $ftp_conn = ftp_connect($liveServerIP) or die("Could not connect to $liveServerIP");
                            $login = ftp_login($ftp_conn, $liveUserAcct, $liveUserPass);
                            ftp_set_option($ftp_conn,FTP_TIMEOUT_SEC,300);

                            // open file for reading
                            $fp = fopen($web_files,'r');

                            // upload file
                            if (ftp_fput($ftp_conn, 'public_html/'.$siteToUpload.'/website.zip', $fp, FTP_BINARY))
                            {
                                $response .= '<pre style="color:#009933;">Successfully uploaded web files!</pre>';
                            }
                            else
                            {
                                return '<p><strong style="color:#ff0000;">Error uploading web file!</strong></p>';
                            }

                            $ftp_file = 'public_html/' . $siteToUpload . '/website.zip';

                            // CHMOD the zip file
                            $response .= '<pre>Change the zip file permissions to 0655</pre>';
                            if (ftp_chmod($ftp_conn, 0655, $ftp_file) !== false) {
                                $response .= '<pre style="color:#009933;">Successfully changed the zipped file permissions!</pre>';
                            } else {
                                return '<p><strong style="color:#ff0000;">Error changing the zip file permissions!</strong></p>';
                            }

                            // close this connection and file handler
                            ftp_close($ftp_conn);
                            fclose($fp);

                            // TODO UNZIP FILE - This works but looses directory and permissions

//                            $fileUnzip = $theAccount->getServerAPIRequest('cpanel?cpanel_jsonapi_user=' . $liveUserAcct . '&cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=fileop&op=extract&sourcefiles=public_html%2F'.$siteToUpload.'%2Fwebsite.zip&destfiles=.');
//                            $response .= '<pre>'.$fileUnzip.'</pre>';

                        } else {
                            return '<p><strong style="color:#ff0000;">Could Not Retrieve Staging Server Public HTML Directory Listing</strong></p>';
                        }

                        $response .= '<pre>Remove SSH Key file</pre>';

                        unlink($sshKeyFileName);

                        $response .= '<pre style="color:#009933;">Done!</pre>';


                    } else {
                        return '<p><strong style="color:#ff0000;">Could Not Access Staging Server Account</strong></p>';
                    }

                }

                return '<p>'.$response.'</p>';

            } else {

                return '<p>Not Supported for this website</p>';

            }

        } else {

            return '<p><strong>Please select the site</strong></p>';

        }

    }*/

    public static function exportLocal($siteToExport)
    {
        //TODO : Add image compression !!

        $isCMS = false;
        $isCart = false;
        $isCart2 = false;

        if ($siteToExport != 'select') {
            $siteToExport = htmlspecialchars($siteToExport);
        }

        if ($siteToExport != 'select') {

            
            $clientDir = Local::buildLocalDir($siteToExport);

            if (is_dir($clientDir . 'cms/')) {
                $isCMS = true;
            } elseif (is_dir($clientDir . 'cart/')) {
                $isCart = true;
            } elseif (is_dir($clientDir . 'cart2/')) {
                $isCart2 = true;
            }

            date_default_timezone_set('NZ');
            $exportDir = $clientDir . 'site_to_upload_' . date('Y_m_d_H_i_s');

            if ($isCMS) {
                mkdir($exportDir);
                MiscTools::Zip($clientDir . 'cms/', $exportDir . '/website');
               /* exec(
                    'mysqldump -u smokeyexport -pninja2010 ' . $siteToExport . '_cms  > ' . $exportDir
                    . '/database.sql.gz'
                );*/
                exec(
                    'mysqldump -u root -proot ' . $siteToExport . '_cms  > ' . $exportDir
                    . '/database.sql.gz'
                );
                if (file_exists($exportDir . '/website.tar.gz')) {
                    $endResult = '<p><strong>Export Ready</strong></p>';
                    $endResult .= '<p>Files are located: ' . $exportDir . '</p>';
                    return $endResult;
                } else {
                    return '<p><strong>Please fix file permissions first</strong></p>';
                }
            } elseif ($isCart) {
                mkdir($exportDir);
                MiscTools::Zip($clientDir . 'cart/', $exportDir . '/website');
                exec(
                    'mysqldump -u smokeyexport -pninja2010 ' . $siteToExport . '_cart  > ' . $exportDir
                    . '/database.sql.gz'
                );
                if (file_exists($exportDir . '/website.tar.gz')) {
                    $endResult = '<p><strong>Export Ready</strong></p>';
                    $endResult .= '<p>Files are located: ' . $exportDir . '</p>';
                    return $endResult;
                } else {
                    return '<p><strong>Please fix file permissions first</strong></p>';
                }
            } elseif ($isCart2) {
                mkdir($exportDir);
                MiscTools::Zip($clientDir . 'cart2/', $exportDir . '/website');
                exec(
                    'mysqldump -u smokeyexport -pninja2010 ' . $siteToExport . '_cart2  > ' . $exportDir
                    . '/database.sql.gz'
                );
                if (file_exists($exportDir . '/website.tar.gz')) {
                    $endResult = '<p><strong>Export Ready</strong></p>';
                    $endResult .= '<p>Files are located: ' . $exportDir . '</p>';
                    return $endResult;
                } else {
                    return '<p><strong>Please fix file permissions first</strong></p>';
                }
            } else {
                return '<p>Not Supported for this website</p>';
            }

        } else {

            return '<p><strong>Please select the site</strong></p>';

        }

    }

    public static function archiveLocal($siteToArchive)
    {

        $isCMS = false;
        $isCart = false;

        if ($siteToArchive != 'select') {
            $siteToArchive = htmlspecialchars($siteToArchive);
        }

        if ($siteToArchive != 'select') {

            $clientDir = '/home/Clients/' . $siteToArchive;
            $archiveDir = $clientDir . '/Archives';
            $archiveWWWDir = $archiveDir . '/WWW';

            if (!file_exists($archiveDir)) {
                mkdir($archiveDir);
            }

            if (!file_exists($archiveWWWDir)) {
                mkdir($archiveWWWDir);
            }

            if (is_dir($clientDir . '/WWW/_site/cms/')) {
                $isCMS = true;
            } elseif (is_dir($clientDir . '/WWW/_site/cart/')) {
                $isCart = true;
            } elseif (is_dir($clientDir . '/WWW/_site/cart2/')) {
                $isCart2 = true;
            } elseif (is_dir($clientDir . '/WWW/_site/vendor/laravel/')) {
                $isLaravel = true;
            }

            date_default_timezone_set('NZ');
            $exportDir = $archiveWWWDir . '/' . date('Y_m_d_H_i_s');

            if ($isCMS) {

                mkdir($exportDir);
                MiscTools::Zip($clientDir . '/WWW/_site/cms/', $exportDir . '/website.zip');
                exec(
                    'mysqldump -u smokeyexport -pninja2010 ' . $siteToArchive . '_cms  > ' . $exportDir
                    . '/database.sql.gz'
                );

                if (file_exists($exportDir . '/website.zip')) {
                    $endResult = '<br><p><strong>CMS Site Archived</strong></p>';
                    $endResult .= '<p>Archived Files are located: ' . $exportDir;
                    return $endResult;
                } else {
                    return '<p><strong>Please fix file permissions first</strong></p>';
                }
            } elseif ($isCart) {
                mkdir($exportDir);
                MiscTools::Zip($clientDir . '/WWW/_site/cart/', $exportDir . '/website.zip');
                exec(
                    'mysqldump -u smokeyexport -pninja2010 ' . $siteToArchive . '_cart  > ' . $exportDir
                    . '/database.sql.gz'
                );

                if (file_exists($exportDir . '/website.zip')) {
                    $endResult = '<br><p><strong>Cart Site Archived</strong></p>';
                    $endResult .= '<p>Archived Files are located: ' . $exportDir . '</p>';
                    return $endResult;
                } else {
                    return '<p><strong>Please fix file permissions first</strong></p>';
                }
            } elseif ($isCart2) {
                mkdir($exportDir);
                MiscTools::Zip($clientDir . '/WWW/_site/cart2/', $exportDir . '/website.zip');
                exec(
                    'mysqldump -u smokeyexport -pninja2010 ' . $siteToArchive . '_cart2  > ' . $exportDir
                    . '/database.sql.gz'
                );

                if (file_exists($exportDir . '/website.zip')) {
                    $endResult = '<br><p><strong>Cart2 Site Archived</strong></p>';
                    $endResult .= '<p>Archived Files are located: ' . $exportDir . '</p>';
                    return $endResult;
                } else {
                    return '<p><strong>Please fix file permissions first</strong></p>';
                }
            } elseif ($isLaravel) {
                mkdir($exportDir);
                MiscTools::Zip($clientDir . '/WWW/_site/', $exportDir . '/website.zip');
                exec(
                    'mysqldump -u smokeyexport -pninja2010 ' . $siteToArchive . '  > ' . $exportDir . '/database.sql.gz'
                );

                if (file_exists($exportDir . '/website.zip')) {
                    $endResult = '<br><p><strong>Laravel Site Archived</strong></p>';
                    $endResult .= '<p>Archived Files are located: ' . $exportDir . '</p>';
                    return $endResult;
                } else {
                    return '<p><strong>Please fix file permissions first</strong></p>';
                }
            } else {
                return '<p>Not Supported for this website</p>';
            }

        } else {

            return '<p><strong>Please select the site</strong></p>';

        }

    }

    public static function removeLocal($clientToRemove)
    {

        if ($clientToRemove != 'select') {
            $clientToRemove = htmlspecialchars($clientToRemove);
        }

        if ($clientToRemove != 'select') {

            $result = shell_exec('/home/SmokeyTools/bin/run_command.py cleanup ' . $clientToRemove);

            if (strpos($result, "[[True, '/") !== false) {
                return '<p>The client folder has successfully been removed!</p><small class="error">' . $result
                . '</small>';
            } else {
                return "<small class='error'>Something went wrong! " . $result . "</small>";
            }

            return;

        } else {

            return '<small class="error">Please select the site</small>';

        }

    }

}