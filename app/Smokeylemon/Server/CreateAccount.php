<?php
/**
 * Created by PhpStorm.
 * User: cam
 * Date: 18/12/13
 * Time: 2:17 PM
 */

namespace Smokeylemon\Server;

use Smokeylemon\Config\initServer;
//use Smokeylemon\WFM\WFMAccount;
//use Smokeylemon\Filezilla\Filezilla;

/**
 * Class Account
 * @package Smokeylemon\Server
 */
class CreateAccount extends LiveServer
{


    public $username;
    public $domain;
    public $pkgname;
    public $mxcheck;
    public $server;
    public $serverIp;


    function __construct($server)
    {
        parent::__construct(initServer::configServer($server));

        $this->server = $server;

        $this->serverIp = $this->ip;

    }

    public function doAccountCreate($username, $domain,  $email)
    {

        // set some 'deafults'..
        $pkgname = 'lemon';
        $mxcheck = 'remote';

        $password = $this->generatePassword();

        $stringToSend = 'username=' . $username;
        $stringToSend .= '&domain=' . $domain;
        $stringToSend .= '&password=' . $password;
        $stringToSend .= '&pkgname=' . $pkgname;
        $stringToSend .= '&mxcheck=' . $mxcheck;
        $stringToSend .= '&contactemail=' . $email;

      /*  // Add these details to WFM Account..
        $wfm = new WFMAccount($wfmid);

        $xmldata = '<CustomFields>' .
            '<CustomField>' .
            '<ID>6681</ID>' .
            '<Text>' . $username . ' | ' . $password . '</Text>' .
            '</CustomField>' .
            '<CustomField>' .
            '<ID>37708</ID>' .
            '<Text>' . $this->server . '</Text>' .
            '</CustomField>' .
            '</CustomFields>';

        $sendData = $wfm->updateWFMCustomField($xmldata);*/

        //if ($sendData == 'success') {
            // Return success to visitor and do the account creation for Cpanel..
            // Do the CPanel and Filezilla account creation..
            //dd($stringToSend);
            $tryCreate = $this->getServerAPIRequest('createacct?'.$stringToSend);
            $result = json_decode($tryCreate, true);
            $status = $result['result'][0]['status'];
            //$ftp = new Filezilla();
            //$ftp->createAccount($username, $password, $this->serverIp, $domain);
        if($status == 1 ){
            $data = '<span class="label success">Success!</span><p>Please note the following details:<br/><strong>Password:</strong> ' . $password . '<br/> ' . '<strong>Username:</strong> ' . $username . '</p>';
        } else {
            $data = '<span class="label error">Error</span><p>It appears that there is an issue with account creation</p>';
        }
        return $data;

    }

    private function generatePassword($length = 10)
    {
        //$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        //$password = substr(str_shuffle($chars), 0, $length);

        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_-=+;:,.?';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string



        //return $password;

    }


}