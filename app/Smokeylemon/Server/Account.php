<?php
/**
 * Created by PhpStorm.
 * User: cam
 * Date: 18/12/13
 * Time: 2:17 PM
 */

namespace Smokeylemon\Server;

use Smokeylemon\Config\initServer;
use Touki\FTP\Connection\Connection;
use Touki\FTP\FTPWrapper;
use Touki\FTP\FTP;
use Touki\FTP\Model\Directory;
use Touki\FTP\Model\File;
use Touki\FTP\FTPFactory;

/**
 * Class Account
 *
 * @package Smokeylemon\Server
 */
class Account extends LiveServer
{

    /*
        *
        *   TODO:  Live to Local Download
        *
        *   1) Zip up/export database on Production Server
        *   2) Detect cms or cart
        *   3) Rename local cms/cart and Export local cms/cart database to /_site/cms_before_DATESTAMP
        *   4) Create new cms/cart folder and Rsync the live files to the local
        *   5) Create/modify config files to suit local server
        *
        */


    /**
     * @var array
     */
    public $account_name;
    /**
     * @var
     */
    public $account_domain_name;
    /**
     * @var
     */
    public $server;
    /**
     * @var
     */
    public $dbname;

    /**
     * @param array $account_name
     * @param       $server
     */
    function __construct($account_name, $server)
    {
        parent::__construct(initServer::configServer($server));

        $this->account_name = $account_name;

        $this->account_domain_name = $this->getMyDomain($this->account_name);

    }

    /**
     * @param $account_name
     *
     * @return mixed
     */
    private function getMyDomain($account_name)
    {

        $json = $this->getServerAPIRequest('accountsummary?user=' . $account_name);


        $jsonRes = $json;

        $json_a = json_decode($jsonRes, true);

        $acct = $json_a['acct'];

        $DomainResults = array();


        foreach ($acct as $value) {
            $tmp = array(
                'domain' => $value["domain"]
            );

            array_push($DomainResults, $tmp);
        }


        return $DomainResults[0]['domain'];


    }

    /**
     * @return null|string
     */
    function cmsOrCart()
    {
        $databases = $this->getServerAPIRequest(
            'cpanel?cpanel_jsonapi_user=' . $this->account_name
            . '&cpanel_jsonapi_module=MysqlFE&cpanel_jsonapi_func=listdbs'
        );

        $jsonRes = $databases;

        $json_a = json_decode($jsonRes, true);

        $result = $json_a['cpanelresult']['data'][0]['userlist'][0]['db'];

        $type = null;

        if (strpos($result, '_cms') !== false) {
            $type = 'cms';
        } elseif (strpos($result, '_cart') !== false) {
            $type = 'cart';
        } else {
            $type = 'cart2';
        }

        return $type;


    }


    /**
     * @param $localDir
     * @param $type
     */
    function renameMove($localDir, $type)
    {

        Local::renameMove($localDir, $type);


    }


    /**
     * @param $URL
     *
     * @return string
     */
    function get_content($URL)
    {

        $filename = $URL;
        $handle = fopen($filename, "r");
        $contents = fread($handle, filesize($filename));
        fclose($handle);

        //dd($contents);

        return $contents;
    }

    public function createTmpDirectory($directoryFolder)
    {
        // create a TMP dir first..
        $backupTmpFolder = $directoryFolder;
        $createBackupTmpFolder = $this->getServerAPIRequest(
            'cpanel?cpanel_jsonapi_user=' . $this->account_name
            . '&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=mkdir&path=public_html&name='
            . $backupTmpFolder
        );
        $jsonRes = $createBackupTmpFolder;
        $json_a = json_decode($jsonRes, true);
        if ($json_a['cpanelresult']['event']['result'] == 1) {
            $result = 'Success';
        } else {
            $result = 'Failed to create folder';
        }
        return $result;

    }


    public function backupLiveSQL($toDirectroyFolder, $type)
    {
        $filePath = $toDirectroyFolder;
        $request = 'cpanel?cpanel_jsonapi_user=' . $this->account_name
            . '&cpanel_jsonapi_apiversion=3&cpanel_jsonapi_module=SmokeylemonCustomPackage&cpanel_jsonapi_func=exportDb&filepath='
            . $filePath
            . '&type='
            . $type;
        $this->getServerAPIRequest($request);
        $result = 'Success';
        /*$jsonRes = $create_backup;
        $json_a = json_decode($jsonRes, true);
        dd($json_a);
        if ($json_a['result']['status'] == 1) {
            $result = 'Success';
        } else {
            $result = 'Failed to create folder';
        }*/
        return $result;

    }

    public function importToLiveSQL($filename, $password, $type)
    {
        $filePath = $filename;
        $request = 'cpanel?cpanel_jsonapi_user=' . $this->account_name
            . '&cpanel_jsonapi_apiversion=3&cpanel_jsonapi_module=SmokeylemonCustomPackage&cpanel_jsonapi_func=importDB&filepath='
            . $filePath
            . '&password='
            . $password
            . '&type='
            . $type;
        $this->getServerAPIRequest($request);

        // delete the sql file, its extracted now
        $this->getServerAPIRequest(
            'cpanel?cpanel_jsonapi_user=' . $this->account_name
            . '&cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=fileop&op=unlink&sourcefiles=public_html%2f'
            . $filePath
        );

        $exists = Local::remoteFileExists('http://' . $this->account_domain_name . '/' . $filePath);


        if (!$exists) {
            $result = true;
        } else {
            $result = false;
            //dd($fileExtract, $delZip);
            trigger_error("Error with filemanagerUnzip Method ", E_USER_ERROR);
        }

        return $result;
    }

    public function deletePublicHtml($confirm = false)
    {
        if ($confirm == 'true') {
            $request = 'cpanel?cpanel_jsonapi_user=' . $this->account_name
                . '&cpanel_jsonapi_apiversion=3&cpanel_jsonapi_module=SmokeylemonCustomPackage&cpanel_jsonapi_func=cleanPublicHtml&confirm='
                . $confirm;
            $this->getServerAPIRequest($request);
            $result = 'Success';
        } else {
            $result = 'Failed, not confirmed';
        }
        return $result;
    }

    public function backupLiveDirectory($toDirectroyFolder)
    {
        $filePath = $toDirectroyFolder;
        $request = 'cpanel?cpanel_jsonapi_user=' . $this->account_name
            . '&cpanel_jsonapi_apiversion=3&cpanel_jsonapi_module=SmokeylemonCustomPackage&cpanel_jsonapi_func=createBackup&filepath='
            . $filePath;


        $this->getServerAPIRequest($request);
        $result = 'Success';
        return $result;
    }

    public function deleteTmpDirectory($toDirectroyFolder)
    {
        $filePath = $toDirectroyFolder;
        $request = 'cpanel?cpanel_jsonapi_user=' . $this->account_name
            . '&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=fileop&op=unlink&sourcefiles=/public_html/'
            . $filePath;
        $deleteTmp = $this->getServerAPIRequest($request);
        $jsonRes = $deleteTmp;
        $json_a = json_decode($jsonRes, true);
        if ($json_a['cpanelresult']['event']['result'] == 1) {
            $result = 'Success';
        } else {
            $result = 'Failed to delete folder';
        }
        return $result;


    }


    public function sendFilesToLiveSite($accountPassword, array $files)
    {


        $connection = new Connection(
            '210.48.67.46', $this->account_name, $accountPassword, 21, 90, true
        );

        //$connection->open();
        $factory = new FTPFactory;
        $ftp = $factory->build($connection);

        foreach ($files as $file) {
            $fileName = basename($file);
            $ftp->upload(new File('www/' . $fileName), $file);
        }


        //$ftp->upload(new File($fileName), $file);
        $connection->close();

    }

    public function makeTmpFolder($accountPassword)
    {
        $connection = new Connection(
            '210.48.67.46', $this->account_name, $accountPassword, 21, 90, true
        );
        $options = array(
            FTP::RECURSIVE => true
        );
        //$connection->open();
        $factory = new FTPFactory;
        $ftp = $factory->build($connection);
        $path = 'public_html/silverstripe-cache';
        $ftp->create(new Directory($path), $options);
        $connection->close();

    }


    public function downloadBackupFile($domain, $tmpFullFilePath, $toDir, $isDB = false)
    {
        $toDir = '/home/Clients/' . $toDir . '/_tmp-from-live-site/';
        // the floowing WORKS: curl --verbose --header 'Host: foobaz.com' 'http://210.48.67.46/_BU_TMP/amp.html' -A "Mozilla"
        $ip = $this->getIp();
        $fromUrl = 'http://' . $ip . '/' . $tmpFullFilePath;
        if (!file_exists($toDir)) {
            mkdir($toDir);
        }
        if ($isDB) {
            $toFile = $toDir . '/cpmovesql-' . $this->account_name . '.tar.gz';
        } else {
            $toFile = $toDir . '/cpmove-' . $this->account_name . '.tar.gz';
        }

        \MiscTools::download($fromUrl, $toFile, $domain);

        // do file checking to see if it exists...
        if (file_exists($toFile)) {
            $result = 'Success';
        } else {
            $result = 'Failed to download';
        }
        return $result;

    }

    public function extractImportDB($direction, $username = 'smokeyexport', $password = 'smokey2010')
    {

        // First step, lets get the database...

        $databases = $this->getServerAPIRequest(
            'cpanel?cpanel_jsonapi_user=' . $this->account_name
            . '&cpanel_jsonapi_module=MysqlFE&cpanel_jsonapi_func=listdbs'
        );

        $jsonRes = $databases;

        $json_a = json_decode($jsonRes, true);

        $result = array();

        $result['db_user'] = $json_a['cpanelresult']['data'][0]['userlist'][0]['user'];

        $result['db_table'] = $json_a['cpanelresult']['data'][0]['userlist'][0]['db'];


        // Second step, lets export the database wiuth our speecial user credentials setup... here is where we required the WFM details...


        $USERNAMEDB = $username;

        $USERNAMEPASSWORD = $password;


        $DBTABLENAME = $result['db_table'];

        $ACCOUNTNAME = $this->account_name;

        $PHP = 'SmokeytoolsAuth';

        $DIRECTION = $direction;

        $serverIp = $this->ip;

        $keycase = 'remote';

        shell_exec(
            "sh " . app_path()
            . "/Smokeylemon/Bash/ssh.sh $USERNAMEDB $USERNAMEPASSWORD $DBTABLENAME $ACCOUNTNAME $PHP $serverIp $keycase $DIRECTION"
        );

        $exists = Local::remoteFileExists('http://www.' . $this->account_domain_name . '/SQLdump.sql.gz');


        if ($exists) {
            $result = true;
        } else {
            $result = false;
            //$error  = dd($DBTABLENAME, $ACCOUNTNAME, $exists, $this->account_domain_name);
            trigger_error("Error with DB extraction " . $this->account_domain_name, E_USER_ERROR);


        }

        return $result;
    }


    public function rSync($type, $localDir, $direction, $zipFolder = false)
    {


        if ($zipFolder) {
            $localPath = $localDir;
        } else {
            $localPath = '/home/Clients/' . $localDir . '/WWW/_site/' . $type;
        }

        $serverIp = $this->ip;
        $servername = 'remote';
        $remotePath = '/home/' . $this->account_name . '/www';
        $res = shell_exec(
            "sh " . app_path() . "/Smokeylemon/Bash/rsync.sh $servername $serverIp $remotePath $localPath $direction"
        );

        if ($direction == 'toLocal') {
            Local::fixPermissions($localDir);
        }

        //simple check test...
        if ($res) {
            $result = true;
        } else {
            $result = false;
            trigger_error("Error with rSync", E_USER_ERROR);
        }

        if ($direction == 'toLive') {
            $accountName = $this->account_name;
            shell_exec("sh " . app_path() . "/Smokeylemon/Bash/sshChangePerm.sh  $serverIp $accountName");
        }

        return $result;

    }


    /**
     *
     * Changes the config.php file
     *
     * @param $dir
     * @param $type
     * @param $direction
     *
     * @return bool
     */
    public function changeConfig($dir, $type, $direction)
    {
//TODO: Silverstipe
        $configFile = Local::buildLocalDir($dir, $type) . '/config.php';

        if (file_exists($configFile)) {


            if ($direction == 'toLocal') {

                $dbuser = Local::getMysqlRootUser();
                $dbpass = Local::getMysqlRootPassword();
                $db_name = $dir . '_' . $type;
                $urlHttp = "http://${dir}.clients.smokeylemon.com/${type}";
                $urlHttps = "http://${dir}.clients.smokeylemon.com/${type}";
                $path = "/home/Clients/${dir}/WWW/_site/${type}/";
                $live = 'N';

            } elseif ($direction == 'toLive') {

                $dbuser = $this->account_name . '_' . $type;
                $dbpass = '';
                $db_name = $dbuser;
                $urlHttp = "http://www.$this->account_domain_name";
                $urlHttps = "https://www.$this->account_domain_name";
                $path = '/home/' . $this->account_name . '/www/';
                $live = 'Y';

            }

            // CMS Vars..

            if ($type == 'cms') {

                $find = array(
                    'db_username' => '/[^\n]*db_username[^\n]*/',
                    'db_password' => '/[^\n]*db_password[^\n]*/',
                    'db_name'     => '/[^\n]*db_name[^\n]*/',
                    'root_url'    => '/[^\n]*root_url[^\n]*/',
                    'site_live'   => '/[^\n]*site_live[^\n]*/',
                );

                $replace = array(
                    'db_username' => "\$config['db_username'] = '${dbuser}';",
                    'db_password' => "\$config['db_password'] = '${dbpass}';",
                    'db_name'     => "\$config['db_name'] = '${db_name}';",
                    'root_url'    => "\$config['root_url'] = '${urlHttp}';",
                    'site_live'   => "\$site_live = '${live}';"
                );

            }

            // Opencart Vars..

            if ($type == 'cart') {

                $find = array(
                    'HTTP_SERVER'  => '/[^\n]*HTTP_SERVER[^\n]*/',
                    'HTTPS_SERVER' => '/[^\n]*HTTPS_SERVER[^\n]*/',
                    'DIR_PATH'     => '/[^\n]*DIR_PATH[^\n]*/',
                    'DB_USERNAME'  => '/[^\n]*DB_USERNAME[^\n]*/',
                    'DB_PASSWORD'  => '/[^\n]*DB_PASSWORD[^\n]*/',
                    'DB_DATABASE'  => '/[^\n]*DB_DATABASE[^\n]*/'
                );

                $replace = array(
                    'HTTP_SERVER'  => "define('HTTP_SERVER', '${urlHttp}/');",
                    'HTTPS_SERVER' => "define('HTTPS_SERVER', '${urlHttps}/');",
                    'DIR_PATH'     => "define('DIR_PATH', '${path}');",
                    'DB_USERNAME'  => "define('DB_USERNAME', '${dbuser}');",
                    'DB_PASSWORD'  => "define('DB_PASSWORD', '${dbpass}');",
                    'DB_DATABASE'  => "define('DB_DATABASE', '${dir}_${type}');"
                );


                $adminFind = array(
                    'HTTP_SERVER'  => '/[^\n]*HTTP_SERVER[^\n]*/',
                    'HTTP_CATALOG' => '/[^\n]*HTTP_CATALOG[^\n]*/',
                    'HTTP_IMAGE'   => '/[^\n]*HTTP_IMAGE[^\n]*/',
                    'HTTPS_SERVER' => '/[^\n]*HTTPS_SERVER[^\n]*/',
                    'HTTPS_IMAGE'  => '/[^\n]*HTTPS_IMAGE[^\n]*/',
                    'DB_USERNAME'  => '/[^\n]*DB_USERNAME[^\n]*/',
                    'DB_PASSWORD'  => '/[^\n]*DB_PASSWORD[^\n]*/',
                    'DB_DATABASE'  => '/[^\n]*DB_DATABASE[^\n]*/'
                );

                $adminReplace = array(
                    'HTTP_SERVER'  => "define('HTTP_SERVER', '${urlHttp}/cartadmin/');",
                    'HTTP_CATALOG' => "define('HTTP_CATALOG', '${urlHttp}/');",
                    'HTTP_IMAGE'   => "define('HTTP_IMAGE', '${urlHttp}/image/');",
                    'HTTPS_SERVER' => "define('HTTPS_SERVER', '${urlHttps}/');",
                    'HTTPS_IMAGE'  => "define('HTTPS_IMAGE', '${urlHttps}/image/');",
                    'DB_USERNAME'  => "define('DB_USERNAME', '${dbuser}');",
                    'DB_PASSWORD'  => "define('DB_PASSWORD', '${dbpass}');",
                    'DB_DATABASE'  => "define('DB_DATABASE', '${dir}_${type}');"
                );
            }


            $orgconfig = file_get_contents($configFile);

            $string = $orgconfig;

            $newconfig = preg_replace($find, $replace, $string, 1);

            file_put_contents($configFile, $newconfig);

            // cart has 2 configs..
            if ($type == 'cart') {

                $configFile = '/home/Clients/' . $dir . '/WWW/_site/' . $type . '/cartadmin/config.php';

                $orgconfig = file_get_contents($configFile);

                $string = $orgconfig;

                $newconfig = preg_replace($adminFind, $adminReplace, $string, 1);

                file_put_contents($configFile, $newconfig);

            }

            $result = false;

            //simple check test...
            if (file_exists($configFile)) {
                $result = true;
            } else {
                $result = false;
                trigger_error("Error with config change", E_USER_ERROR);
            }


        } else {
            // no config found...
            $result = true;
        }
        return $result;
    }


    /**
     *
     * Method: filemanagerUnzip
     *
     * Will create a NEWSITE dir under the accounts www folder
     *
     * @param $file
     *
     * @return bool
     */
    public function filemanagerUnzip($file)
    {
        //// create a NEWSITE dir under the WWW folder
        //$newDir = $this->getServerAPIRequest('cpanel?cpanel_jsonapi_user=' . $this->account_name . '&cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=mkdir&path=www%2f&name=NEWSITE');

        // extact the files..
        $fileExtract = $this->getServerAPIRequest(
            'cpanel?cpanel_jsonapi_user=' . $this->account_name
            . '&cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=fileop&op=extract&sourcefiles=public_html%2f'
            . $file
            . '&destfiles=%2fpublic_html'
        );
        //dd($fileExtract);

        // delete the zip file, its extracted now
        $delZip = $this->getServerAPIRequest(
            'cpanel?cpanel_jsonapi_user=' . $this->account_name
            . '&cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=fileop&op=unlink&sourcefiles=public_html%2f'
            . $file
        );

        $exists = Local::remoteFileExists('http://' . $this->account_domain_name . '/index.php');

        $result = false;

        if ($exists) {
            $result = true;
        } else {
            $result = false;
            //dd($fileExtract, $delZip);
            trigger_error("Error with filemanagerUnzip Method ", E_USER_ERROR);
        }

        return $result;

    }


    public function backupCurrentLiveSite()
    {

        $today = date("d.m.y"); // 03.10.01

        // create a backup! Makes sence.. require api v1
        $oldDir = $this->getServerAPIRequest(
            'cpanel?cpanel_jsonapi_apiversion=1&cpanel_jsonapi_user=' . $this->account_name
            . '&cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=fullbackup'
        );

        $result = false;

        if ($oldDir) {
            $result = true;
        } else {
            $result = false;
            trigger_error("Error with backupOldSite Method ", E_USER_ERROR);
            //dd($oldDir);
        }

        return $result;

    }


    public function enableHtaccess($localDir, $type)
    {
        $path = '/home/Clients/' . $localDir . '/WWW/_site/' . $type . '/';

        $orgFile = $path . '.htaccessORG';

        $newFile = $path . '.htaccess';

        $result = true;

        if (file_exists($orgFile)) {
            rename($orgFile, $newFile);

            $result = false;

            if (file_exists($newFile)) {
                $result = true;
            } else {
                $result = false;
                trigger_error("Error with htacess change", E_USER_ERROR);
            }

        }

        return $result;


    }


}