<?php
/**
 * Created by PhpStorm.
 * User: cam
 * Date: 24/01/14
 * Time: 2:33 PM
 */

namespace Smokeylemon\Server;

use Smokeylemon\Config\initServer;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;


class Listings extends LiveServer
{

    public $server;


    function __construct($server)
    {
        parent::__construct(initServer::configServer($server));
        $this->server = $server;

    }


    public function getCmsSites()
    {

        $filecacheName = 'getCmsSites_' . $this->server;


        if (Cache::has($filecacheName)) {

            $results = Cache::get($filecacheName);

            return $results;

        } else {

            $expiresAt = Carbon::now()->addMinutes(240);

            $latest = $this->getData('http://dev.cmsmadesimple.org/latest_version.php');

            //$LatestMajorVersion = substr($latest['message'], -6);

            $LatestMajorVersion = preg_replace("/[^0-9.]/", "", $latest['message']);


            $LatestMajorVersionConversion = round($LatestMajorVersion, 3);

            $LMV = $LatestMajorVersionConversion;

            $urlsToTest = $this->getMyDomains(true);

            $result = '';

            $domainResTmp = array();

            foreach ($urlsToTest as $domain) {

                $result = $this->getData('www.' . $domain['domain'] . '/version.php', false);

                if ($result['status'] === true) {


                    $result = $this->getData('www.' . $domain['domain'] . '/vcheck.php', true);

                    $version = str_replace('VERSION:', '', $result['message']);

                    //$version = round($version);

                    $versionHolder = substr($version, 0, 4); // returns "a hundred value"

                    if ($result['message'] != '<b>Check not found</b>') {

                        // there needs to be a special conversion of version numbers due to the way the versions are specified,  1.10 is greater than 1.9 , so will just remove the decimal points and add 100 to add a new 0...

                        if (strpos($LatestMajorVersionConversion, '1.11.') !== false) {
                            $LatestMajorVersion = $LatestMajorVersionConversion + 100;
                        }

                        if (strpos($versionHolder, '1.10.') !== false) {
                            $versionHolder = $versionHolder + 100;
                        }


                        $CurrentCmsVersion = (string)$LatestMajorVersion;
                        $CurrentCmsVersionFlat = str_replace('.', '', $CurrentCmsVersion);

                        //$SiteCmsVersion = $version;

                        $SiteCmsVersion = (string)$versionHolder;
                        $SiteCmsVersionFlat = str_replace('.', '', $SiteCmsVersion);


                        //  $breakpoint = $CurrentCmsVersionFlat - 200; // just a generic algarythem to detect 'old' sites

                        $breakpoint = $SiteCmsVersionFlat;

                        //dd($breakpoint);


                        switch ($breakpoint) {
                            case  $SiteCmsVersionFlat <= 18: //v 0 - 1.8 series
                                $action = '<span class="redWarning">Upgrade Vital</span>';
                                break;
                            case  $SiteCmsVersionFlat <= 19: //v 1.9 - 1.8 series
                                $action = '<span class="yellowWarning">Upgrade Recomended</span>';
                                break;
                            case $SiteCmsVersionFlat <= 100: //v 1.10 series
                            case $SiteCmsVersionFlat >= 100: // current
                                $action = '<span class="greenWarning">Upgrade Not Required</span>';
                                break;

                        }


                        //dd($CurrentCmsVersionFlat, $breakpoint);


                        $domainResTmp[] = array(
                            'domain'  => $domain['domain'],
                            'version' => $version,
                            'action'  => $action
                        );

                    }
                }

            }


            $data = array(
                'LMV' => $LMV,
                'Res' => $domainResTmp
            );

            // dd($data);

            Cache::put($filecacheName, $data, $expiresAt);

            return $data;

        }

    }


    public function getCartSites()
    {

        $filecacheName = 'getCartSites_' . $this->server;


        if (Cache::has($filecacheName)) {

            $results = Cache::get($filecacheName);

            return $results;

        } else {

            $expiresAt = Carbon::now()->addMinutes(240);

            $latest = 1.5;

            $LatestMajorVersion = 1.5;

            $LatestMajorVersionConversion = round($LatestMajorVersion, 3);

            $LMV = $LatestMajorVersionConversion;

            $urlsToTest = $this->getMyDomains(true);

            $result = '';

            $domainResTmp = array();

            foreach ($urlsToTest as $domain) {

                $result = $this->getData('www.' . $domain['domain'] . '/ocversion.php', false);

                if ($result['status'] === true) {


                    $result = $this->getData('www.' . $domain['domain'] . '/ocvcheck.php', true);

                    $version = str_replace('VERSION:', '', $result['message']);


                    if ($result['message'] != '<b>Check not found</b>') {

                        $SiteVersionFlat = number_format((float)$version);

                        $breakpoint = $SiteVersionFlat;

                        switch ($breakpoint) {
                            case  $SiteVersionFlat <= 18: //v 0 - 1.8 series
                                $action = '<span class="redWarning">Upgrade Vital</span>';
                                break;
                            case  $SiteVersionFlat <= 19: //v 1.9 - 1.8 series
                                $action = '<span class="yellowWarning">Upgrade Recomended</span>';
                                break;
                            case $SiteVersionFlat <= 100: //v 1.10 series
                            case $SiteVersionFlat >= 100: // current
                                $action = '<span class="greenWarning">Upgrade Not Required</span>';
                                break;

                        }


                        $domainResTmp[] = array(
                            'domain'  => $domain['domain'],
                            'version' => $version,
                            'action'  => $action
                        );

                    }
                }

            }


            $data = array(
                'LMV' => $LMV,
                'Res' => $domainResTmp
            );

            // dd($data);

            Cache::put($filecacheName, $data, $expiresAt);

            return $data;

        }

    }


}