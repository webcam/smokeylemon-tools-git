<?php
/**
 * Created by PhpStorm.
 * User: cam
 * Date: 28/03/14
 * Time: 10:23 AM
 */

namespace Smokeylemon\Google;




class Drive {

    /**
     * Print a file's metadata.
     *
     * @param Google_DriveService $service Drive API service instance.
     * @param string $fileId ID of the file to print metadata for.
     */
    public  function printFile($service, $fileId) {
        try {
            $file = $service->files->get($fileId);

            print "Title: " . $file->getTitle();
            print "Description: " . $file->getDescription();
            print "MIME type: " . $file->getMimeType();
        } catch (Exception $e) {
            print "An error occurred: " . $e->getMessage();
        }
    }

    /**
     * Download a file's content.
     *
     * @param Google_DriveService $service Drive API service instance.
     * @param File $file Drive File instance.
     * @return String The file's content if successful, null otherwise.
     */
public function downloadFile($service, $file) {
        $downloadUrl = $file->getDownloadUrl();
        if ($downloadUrl) {
            $request = new Google_HttpRequest($downloadUrl, 'GET', null, null);
            $httpRequest = Google_Client::$io->authenticatedRequest($request);
            if ($httpRequest->getResponseHttpCode() == 200) {
                return $httpRequest->getResponseBody();
            } else {
                // An error occurred.
                return null;
            }
        } else {
            // The file doesn't have any content stored on Drive.
            return null;
        }
    }


} 