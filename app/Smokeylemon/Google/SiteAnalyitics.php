<?php
/**
 * Created by PhpStorm.
 * User: cam
 * Date: 30/01/14
 * Time: 1:06 PM
 */

namespace Smokeylemon\Google;

use Smokeylemon\Google\GWTdata;
use utilphp\util;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;


/**
 * Class SiteAnalyitics
 * @package Smokeylemon\Google
 */
class SiteAnalyitics
{


    function __construct()
    {
        //  $this->retrive();
    }

    public function retrive()
    {
        $filecacheName = 'SiteAnalyitics';


        if (Cache::has($filecacheName)) {

            $results = Cache::get($filecacheName);


        } else {

            $expiresAt = Carbon::now()->addMinutes(240);

            $myCachedPath = storage_path() . '/cache/GoogleAPI/';

            //dd($myCachedPath);

            if (!is_dir($myCachedPath)) {

                // dd($myCachedPath);
                mkdir($myCachedPath, 0777, true);
            }


            try {
                $email = 'support@smokeylemon.com';
                $password = 'coffee2011';

                $gdata = new GWTdata();
                if ($gdata->LogIn($email, $password) === true) {
                    $sites = $gdata->GetSites();
                    foreach ($sites as $site) {

                        $oldfolder = util::sanitize_string($site);

                        $newfolder = str_replace('http', '', $oldfolder);

                        $myCachedPathNow = $myCachedPath . $newfolder;

                        if (!is_dir($myCachedPathNow)) {

                            // dd($myCachedPath);
                            mkdir($myCachedPathNow, 0777, true);
                        }

                        //dd($myCachedPath);

                        $gdata->DownloadCSV($site, $myCachedPathNow);
                        Cache::put($filecacheName, $site, $expiresAt);
                    }
                }
            } catch (Exception $e) {
                die($e->getMessage());
            }


            /*

                    try {
                        $email = 'support@smokeylemon.com';
                        $password = 'coffee2011';

                        # If hardcoded, don't forget trailing slash!
                        //$website = "http://www.smokeylemon.com/";

                        $gdata = new GWTdata();
                        if ($gdata->LogIn($email, $password) === true) {
                            $gdata->DownloadCSV($website, $myCachedPath);
                            Cache::put($filecacheName, $gdata, $expiresAt);
                        }
                    } catch (Exception $e) {
                        die($e->getMessage());
                    }*/


            $results = $gdata;


        }

        return $results;

    }

    public function build()
    {
        // todo: go throguth the GoogleApi Folder and retrive the domain names...

        $myCachedPath = storage_path() . '/cache/GoogleAPI/*';



        $domian = array();
        // Open a known directory, and proceed to read its contents
        foreach (glob($myCachedPath, GLOB_ONLYDIR) as $file) {


            //dd($latestFile);
            foreach (glob($file . '/CONTENT_KEYWORDS-*') as $realfile) {

            //$latest =  date("Ymd-His");


                $filename = basename($realfile);
                $domian[] = substr($filename, 17, -20);
            }


        }



        return $domian;


    }


    public function generateGraph($domain)
    {

        $filesWanted = array(
            'Content Keywords' => 'CONTENT_KEYWORDS-' . $domain,
            'External Links' => 'EXTERNAL_LINKS-' . $domain,
            'Internal Links' => 'INTERNAL_LINKS-' . $domain,
            'Top Pages' => 'TOP_PAGES-' . $domain,
            'Top Queries' => 'TOP_QUERIES-' . $domain
        );

        $domain = util::sanitize_string($domain);

        $result = array();

        foreach ($filesWanted as $type => $file) {

            $thefiles = storage_path() . '/cache/GoogleAPI/' . $domain . '/' . $file . '*';


            foreach (glob($thefiles) as $csv) {
               if (file_exists($csv)){
                    $result[][$type] = $this->readCSV($csv);
                }

            }

            $newArray = array_merge_recursive($result, $filesWanted);

        }

        return $newArray;

    }

    private function readCSV($csvFile)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle);
        }

        fclose($file_handle);

        return $line_of_text;
    }

    /**
     * @param $string
     * @return mixed|string
     */
    private function sanitize($string)
    {
        $match = array("/\s+/", "/[^a-zA-Z0-9\-]/", "/-+/", "/^-+/", "/-+$/");
        $replace = array("-", "", "-", "", "");
        $string = preg_replace($match, $replace, $string);
        $string = strtolower($string);
        return $string;
    }


}