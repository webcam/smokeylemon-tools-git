<?php
/**
 * Created by PhpStorm.
 * User: Cameron
 * Date: 28/11/13
 * Time: 9:33 PM
 */

namespace Smokeylemon\WFM;

use WfmTools;


class WFMAccount
{


    function __construct($accountID)
    {
        $this->accountID = $accountID;
    }

    public function accountName()
    {

        $acc = $this->getWFMAccount();

        return $acc->Client->Name;
        //$address = $xml->Client->Address . ' ' . $xml->Client->City;

    }


    /*

          $endResult = '<h4>' . $name . '</h4><p>' . $address . '</p>';


          $xmlCustom = simplexml_load_file($cacheClientCustom);
          $endResult .= '<p>';
          foreach ($xmlCustom->CustomFields->CustomField as $node) {
              $endResult .= '<strong>' . $node->Name . '</strong>: ' . $node->Text . '<br/>';
              //var_dump($node);
          }
          $endResult .= '</p>';

          return $endResult;

      }*/

    public function getWFMAccount($custom = false)
    {
        // This file just grabs the all the availbe job data and caches it
        //$BaseDir = public_path() . '/WFMcache';
        $BaseDir = storage_path() . '/cache/WFMcache';
        if (!is_dir($BaseDir)) {
            mkdir($BaseDir);
        }
        $clientid = $this->accountID;
        //Jobs List
        $downloadClientData = 'https://api.workflowmax.com/client.api/get/' . $clientid . '?apiKey=' . WfmTools::$APIKey . '&accountKey=' . WfmTools::$AccKey . '&detailed=true';
        $cacheClientName = $BaseDir . '/resultClient_' . $clientid . '.xml';

        $downloadClientCustomData = 'https://api.workflowmax.com/client.api/get/' . $clientid . '/customfield?apiKey=' . WfmTools::$APIKey . '&accountKey=' . WfmTools::$AccKey;
        $cacheClientCustom = $BaseDir . '/resultClient_Custom' . $clientid . '.xml';

        // generate the cache version if it doesn't exist or it's too old!
        $ageInSeconds = 1800; // twelve hours

        if (!file_exists($cacheClientName) || filemtime($cacheClientName) > time() + $ageInSeconds) {
            $contents = file_get_contents($downloadClientData);
            file_put_contents($cacheClientName, $contents);
        }

        if (!file_exists($cacheClientCustom) || filemtime($cacheClientCustom) > time() + $ageInSeconds) {
            $contents = file_get_contents($downloadClientCustomData);
            file_put_contents($cacheClientCustom, $contents);
        }


        if ($custom) {
            // return  simplexml_load_file($cacheClientCustom);
            return $cacheClientCustom;
        } else {
            // return  simplexml_load_file($cacheClientName);
            return $cacheClientName;
        }


    }

    protected  function killCache()
    {
        $BaseDir = storage_path() . '/cache/WFMcache';
        $clientid = $this->accountID;
        $cacheClientName = $BaseDir . '/resultClient_' . $clientid . '.xml';
        $cacheClientCustom = $BaseDir . '/resultClient_Custom' . $clientid . '.xml';
        if (file_exists($cacheClientName)){
            unlink($cacheClientName);
        }
        if (file_exists($cacheClientCustom)){
            unlink($cacheClientCustom);
        }
        return 'File(s) Deleted';
    }


    public function readXMLData($accountPath, $customPath)
    {
        $xml = simplexml_load_file($accountPath);

        $name = $xml->Client->Name;
        $address = $xml->Client->Address . ' ' . $xml->Client->City;
        $xml = simplexml_load_file($customPath);
        $customAttr = $xml->CustomFields->CustomField;

        $data = array(
            'Name' => $name,
            'Address' => $address
        );

        foreach ($customAttr as $key) {
            // search for new line from xml to obtain a textarea..
            if (preg_match("/\n/", $key->Text)) {
                $key['InputType'] = 'Textarea';
            }
            array_push($data, $key);
        }


        return $data;
    }


    public function updateWFMCustomField($xmldata)
    {

        $clientid = $this->accountID;

        $urlTopPost = 'https://api.workflowmax.com/client.api/update/' . $clientid . '/customfield?apiKey=' . WfmTools::$APIKey . '&accountKey=' . WfmTools::$AccKey;

        //dd($xmldata);

        //setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlTopPost);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xmldata");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $data = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        //convert the XML result into array
        $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

        if ($http_status == 200) {
            return 'success';
        } else {
            return 'failed';
        }

    }


    public function getWHMCustomField($id, $valueOnly = false)
    {

        $cacheClientCustom = $this->getWFMAccount('custom');

        $xmlCustom = simplexml_load_file($cacheClientCustom);

        foreach ($xmlCustom->CustomFields->CustomField as $node) {

            if ($node->ID == $id) {

                if ($valueOnly == true) {
                    $endResult = $node->Text;
                } else {
                    $endResult = '<strong>' . $node->Name . '</strong>: ' . $node->Text . '<br/>';
                }

            }
        }

        $result = $endResult;

        // if wanting password value only.....
        if ($id == 6681 && $valueOnly == true) {
            $password = explode("|", $endResult);

            if (isset($password[1])) {
                $result = $password[1];
            } else {
                $password = explode("\n", $endResult);
                $result = $password[1];
            }


        }


        return $result;


    }


}