<?php
/**
 * Created by PhpStorm.
 * User: cam
 * Date: 18/12/13
 * Time: 2:17 PM
 */

namespace Smokeylemon\WFM;

use Smokeylemon\Config\initServer;
use Smokeylemon\WFM\WFMAccount;
use Smokeylemon\Filezilla\Filezilla;
use Smokeylemon\Server\LiveServer;
use Smokeylemon\Server\EditAccount as CpanelEdit;

/**
 * Class Account
 * @package Smokeylemon\Server
 */
class EditAccount extends WFMAccount
{

    function __construct($accountID)
    {
        $this->accountID = $accountID;
        //new WFMAccount($accountID);
    }

    public function doAccountEdit($data)
    {
        //dd($data);

        $xmldata = $this->generateXml($data);
        $sendData = $this->updateWFMCustomField($xmldata);
        if ($sendData == 'success') {
            foreach ($data as $node) {
                // ID 6681 = Username & Password..
                if ($node['name'] == 6681) {
                    // correct format to be: username | password
                    //force the correct formatting e.g remove any new lines and replace with ....
                    $string = preg_replace('~[\r\n]+~', ' | ', $node['value']);
                    $nodeStr = explode(' | ', $string);
                    $username = $nodeStr[0]; // name
                    $password = $nodeStr[1]; // password
                }
                // ID 37708 = Server..
                if ($node['name'] == 37708) {
                    $server = $node['value']; // server name
                }
                //dd($server);
            }
            // to update the filezilla and cpanel....
            if ( isset($username) && !empty($username) && isset($password) && !empty($password) && isset($server) && !empty($server)) {
                $ftp = new Filezilla();
                $ftp->updateAccount($username, $password);

                // TODO: Update Cpanel details ONLY IF REGISTERED CPANEL!, cant do username, that Must be kept, so lets just update Passwords, even for sub databases, then we need to change config, oh this is gona be fun..

                $allowed = array(
                    //'Tart' => true,
                    'Mint' => true
                );
                // cpanel update..
                if ($allowed) {
                    $cpanel = new CpanelEdit($username, $server);
                    $cpanel->updateAccount($username, $password);
                }
                $data = '<span class="label success">Success!</span>';
            } else {
                $data = '<span class="label secondary">Success!, However, Please Note, Filezilla and CPanel have not been updated</span>';
            }
        } else {
            $data = '<span class="label error">Error</span><p>It appears that there is an issue with WorkFlow Max at this time, the account has NOT been created on the server</p>';
        }

        // Clear the cached file!
        $this->killCache();

        return $data;
    }

    private function generateXml($xmlData)
    {
        $xml = '<CustomFields>';
        foreach ($xmlData as $node) {
            $xml .= '<CustomField>' .
                '<ID>' . $node['name'] . '</ID>' .
                '<Text>' . $node['value'] . '</Text>' .
                '</CustomField>';
        }
        $xml .= '</CustomFields>';
        return $xml;
    }

    private function generatePassword($length = 10)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^*()_-=+;:,.?";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }


}