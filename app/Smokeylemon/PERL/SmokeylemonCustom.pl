package Cpanel::API::SmokeylemonCustomPackage;

use strict;

our $VERSION = '1.0';

# Your comments about this custom module.

# Cpanel Dependencies
use Cpanel                   ();
use Cpanel::API              ();
use Cpanel::Locale           ();
use Cpanel::Logger           ();
use Cpanel::AdminBin         ();
use Cpanel::AdminBin::Call   ();
use Cpanel::JSON             ();
# Other dependencies go here.
# Defaults go here.
# Constants go here.

# Globals
my $logger;
my $locale;

$logger = Cpanel::Logger->new();

sub _cpanel_user {
    return $Cpanel::user;
}

#-------------------------------------------------------------------------------------------------
# Name:
#   createBackup - Choose a function name that describes the function's action.
# Desc:
#   Creates a Backup Of Files.
# Arguments:
#   $filepath - string - Filepath.
# Returns:
#   $result1 - string - A description of the $result1 parameter.
#-------------------------------------------------------------------------------------------------

sub createBackup {
    my ( $args, $result) = @_;
    my $cmd;
    my $cpUser = _cpanel_user();
    my ( $filepath) = $args->get( 'filepath' );
    if (defined $filepath) {
        $cmd = "tar -zcvf /home/$cpUser/public_html/$filepath --directory=/home/$cpUser/public_html/ . 2>&1";
        system( $cmd );
        $result = 1;
        return $result;
    }
    else {
        $logger->warn( $filepath );
        return 0;
    }
}

#-------------------------------------------------------------------------------------------------
# Name:
#   function - exportDb
# Desc:
#   Exports a database
# Arguments:
#   $filepath - string - Filepath.
# Returns:
#   $result1 - string - A description of the $result1 parameter.
#-------------------------------------------------------------------------------------------------
sub exportDb {
    my ( $args, $result) = @_;
    my $cmd;
    my $databaseName;
    my $cpUser = _cpanel_user();
    my ( $filepath) = $args->get( 'filepath' );
    my ( $type) = $args->get( 'type' );
    if (defined $filepath) {
        #
        $databaseName = substr( $cpUser, 0, 8 )."_".$type;
        $cmd = "mysqldump -u BACKUPUSER -plemonninja $databaseName  > /home/$cpUser/public_html/$filepath 2>&1";
        system( $cmd );
        $result = 1;
        return $result;
    }
    else {
        $logger->warn( $filepath );
        return 0;
    }
}

sub importDB {
    my ( $args, $result) = @_;
    my $cmd;
    my $cpUser = _cpanel_user();
    my $databaseName;
    my ( $filepath) = $args->get( 'filepath' );
    my ( $password) = $args->get( 'password' );
    my ( $type) = $args->get( 'type' );
    if (defined $filepath) {
        #
        $databaseName = substr( $cpUser, 0, 8 )."_".$type;
        $cmd = "mysql -u $cpUser -p$password $databaseName  < /home/$cpUser/public_html/$filepath 2>&1";
        system( $cmd );
        $result = 1;
        return $result;
    }
    else {
        $logger->warn( $filepath );
        return 0;
    }
}

sub cleanPublicHtml {
    my ( $args, $result) = @_;
    my $cmd;
    my $cpUser = _cpanel_user();
    my ( $confirm) = $args->get( 'confirm' );
    if (defined $confirm) {
        if ($confirm eq 'true') {
            #
            my $errors;
            while ($_ = glob( "/home/$cpUser/public_html/*.*" )) {
                next if -d $_;
                unlink( $_ )
                    or ++$errors, warn( "Can't remove $_: $!" );
            }
            exit( 1 ) if $errors;
            $result = 1;
            return $result;
        } else {
            $logger->warn( $confirm );
            return 0;
        }
    }
    else {
        $logger->warn( $confirm );
        return 0;
    }
}

1;