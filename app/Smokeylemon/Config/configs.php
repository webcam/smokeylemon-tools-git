<?php

namespace Smokeylemon\Config;

class initServer
{
    public static function configServer($name)
    {
        switch ($name) {
            case "Tart":
                $server = array(
                    'username' => 'root',
                    'hash'     => 'de5867cbfb7cf48b4499d976f1a8b79c
2eeb26edc20d95e9a7b9dda00d35693d
873471da9aa6749fa43d0e0e3bb0f787
572c51bd5d26704679976851d169c1e0
b699fd56f3a11422379dc92f47c146f1
3a4a03a3d208427bcb7b887ae97a914a
1c7b794ff182c33f7e1ab3b1c113d065
1a207d351bc0acc1b5a4bc5f8160dc12
e6216ec569e25d0842cb79a41bacadd9
82025eb07d844417a9ea459078e17ac4
10c08363a8d906d4ae22dd3dc1d99233
9b651c8897efa7cb9c7ecb67662a6061
c8d4d354e28c0266923187d07ad1846b
6521e5c94b4a19bc0d2008f8b3022ea2
a2984d7cd045f8e4afe1d9a8b475a976
5b17619c5887f77609f4c07d626615a7
1fee7a99426e7fb37a9ae9b99659eefe
59d10eb28cd420b52842155150f514c2
dca9516b50908fc9b3ccd3d0bf23f804
81d16adce0f29a4ae7227f270d0974fd
d1f0cb0c430728e7c4edc8281d1ba89e
f9477c845491ad265b4d4a110b8ca004
82c435d36be2bf5c5e27016cb94e97b5
3468eaa8cf4f755de0c8c0ba0de07a70
2c307063e02e3480802ee85ec7cbfb70
4cf68b24d51c602410f7b85956804527
b54084b6a718b7a911f041113530f188
e5627b53abca2194906a339386469857
8d64200384925bd895c7e2995e9e546f',
                    'domain'   => 'tart.smokeylemon.com:2087',
                    'ip'       => '210.48.67.67',
                    'keycase'  => 'tart'
                );
                break;
            case "Mint":
                $server = array(
                    'username' => 'root',
                    'hash'     => '4c22d74f9860e54665407c5fc0ffa349
296a0179536b51faf7ba340be48360ab
1ae68ea4609a006caa4804d591cf02d1
65d0a5b0d733806fda068cdfd2b17de5
e95caff70e794102bda40e64837e27e3
27fa0de94081f460ba693b313412d1e6
8b408b7f32ba267e7af590224ee170ee
11eedcd79a01a0146a1bc7f831d6c302
29cfb400498976b679f938e8d9aa5c16
338f1ade5ba5242c59f7fa50299a7f59
231470980f346db65f0a745bccd9aaf1
b7b1cfa7d8745a7cc694fcd479332b63
0600d02e6e0e54cf789e8ed151f1067e
64e7b7b9c437c4bab8b56474714dd413
1200d605c16493c12751f4da73716b45
244bb9c2cdcedfffdff8df4877ff53e6
7e18a51c0b17f9c922d1d11cb3cf46a4
5950d83ea8b9b394e4b5fcb5cebaba5e
25341c0da3432aa4e92eef2cae1691f3
bb33a04ea791e206d502e114227d615b
c995d75c840a35e9e8b834fe397ca712
499262a209730a6e873f54cfebd55e7b
4589d93260108d28dd8eb55c77ca7a95
98cb5e97b9b06cef58690c23470b0a35
8e4ceb2b4eca5d80e19484fd007b5912
c3dab1acc11d3c657df3b645057d47d3
dd38d413136901f5c94afc0441a94654
55ab7686501f69a183e119f128f57035
f8da3ff222c966f96983d57fc0d90cc2',
                    'domain'   => 'mint.smokeylemon.com:2087',
                    'ip'       => '210.48.67.46',
                    'keycase'  => 'mint'
                );
                break;
            case "Other":
                $server = array(
                    'username' => 'root',
                    'hash'     => '',
                    'domain'   => 'other.smokeylemon.com:2087'
                );
                break;
            default:
                die();
        }

        return $server;


    }

}


