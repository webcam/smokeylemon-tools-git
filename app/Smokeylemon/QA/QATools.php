<?php
namespace Smokeylemon\QA;

use GuzzleHttp\Message\Response;
use Smokeylemon\GTMetrix\Services_WTF_Test;
use GlLinkChecker\GlLinkChecker;
use Symfony\Component\Finder\Finder;
use utilphp\util;

/**
 * Created by PhpStorm.
 * User: cameron
 * Date: 20/04/16
 * Time: 5:47 PM
 */
class QATools
{

    public function IFramely($url)
    {
        $finalUrl = 'http://iframe.ly/api/iframely?url=' . urlencode($url) . "&api_key=b661ee5ccda6c478ef20c3";
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $finalUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;

        
        

    }

    public function GTMetrix($url)
    {

        $gtService = new Services_WTF_Test("webcam@smokeylemon.com", "a4ec7710f0dbfc7a2f1b54e7a8251ea7");
        $domain = $url;
// To test a site, run the test() method, and pass in at minimum a url to test. Returns
// the testid on success, or false and error messsage in $test->error if failure.
        //$url_to_test = "http://gtmetrix.com/";
        //echo "Testing $url_to_test\n";
        $testid = $gtService->test(
            array(
                'url' => $domain
            )
        );
        if (!$testid) {
            die("Test failed: " . $gtService->error() . "\n");
        }

// Other options include:
//
//      location => 4  - test from the Dallas test region (see locations below)
//      login-user => 'foo',
//      login-pass => 'bar',  - the test requires http authentication
//      x-metrix-adblock => 1 - use the adblock plugin during this test
//
// For more information on options, see http://gtmetrix.com/api/

// After calling the test method, your URL will begin testing. You can call:
        // echo "Waiting for test to finish\n";
        $gtService->get_results();

// which will block and return once your test finishes. Alternatively, can call:
//     $state = $test->state()
// which will return the current state. Please don't check more than once per second.

// Once your test is finished, chech that it completed ok, otherwise get the results.
// Note: you must check twice. The first ->test() method can fail if url is malformed, or
// other immediate error. However, if you get a job id, the test itself may fail if the url
// can not be reached, or some pagespeed error.
        if ($gtService->error()) {
            die($gtService->error());
        }
        //$testid = $gtService->get_test_id();
        //echo "Test completed succesfully with ID $testid\n";
        $results = $gtService->results();


        /*foreach ($results as $result => $data) {
            echo "  $result => $data\n";
        }*/
        /* echo "\nResources\n";
         $resources = $gtService->resources();
         foreach ($resources as $resource => $url) {
             echo "  Resource: $resource $url\n";
         }*/

// Each test has a unique test id. You can load an existing / old test result using:
        /*      echo "Loading test id $testid\n";
              $gtService->load($testid);*/

// If you no longer need a test, you can delete it:
        //echo "Deleting test id $testid\n";
        $result = $gtService->delete();
        if (!$result) {
            die("error deleting test: " . $gtService->error());
        }

        /*// To list possible testing locations, use locations() method:
                echo "\nLocations GTmetrix can test from:\n";
                $locations = $gtService->locations();
        // Returns an array of associative arrays:
                foreach ($locations as $location) {
                    echo "GTmetrix can run tests from: " . $location["name"] . " using id: " . $location["id"] . " default ("
                        . $location["default"] . ")\n";
                }*/

        /* Sample output:

        Testing http://gtmetrix.com/
        Test started with PnV4kAr2
        Waiting for test to finish
        Test completed succesfully with ID PnV4kAr2
          page_load_time => 480
          html_bytes => 3346
          page_elements => 16
          report_url => http://gtmetrix.com/reports/gtmetrix.com/1r5AHf9E
          html_load_time => 28
          page_bytes => 90094
          pagespeed_score => 95
          yslow_score => 98

        Resources
          Resource: report_pdf https://gtmetrix.com/api/0.1/test/PnV4kAr2/report-pdf
          Resource: pagespeed https://gtmetrix.com/api/0.1/test/PnV4kAr2/pagespeed
          Resource: har https://gtmetrix.com/api/0.1/test/PnV4kAr2/har
          Resource: pagespeed_files https://gtmetrix.com/api/0.1/test/PnV4kAr2/pagespeed-files
          Resource: yslow https://gtmetrix.com/api/0.1/test/PnV4kAr2/yslow
          Resource: screenshot https://gtmetrix.com/api/0.1/test/PnV4kAr2/screenshot
        Loading test id PnV4kAr2
        Deleting test id PnV4kAr2

        Locations GTmetrix can test from:
        GTmetrix can run tests from: Vancouver, Canada using id: 1 default (1)
        GTmetrix can run tests from: London, UK using id: 2 default ()
        GTmetrix can run tests from: Sydney, Australia using id: 3 default ()
        GTmetrix can run tests from: Dallas, USA using id: 4 default ()
        GTmetrix can run tests from: Mumbai, India using id: 5 default ()

        */


        //var_dump($domain);
        return $results;
    }

    public function w3cValidation($url)
    {
        // w3c Validation
        $tmpDir = storage_path() . '/cache/validator';
        if (!file_exists($tmpDir)) {
            mkdir($tmpDir, 0777);
        }
        $appPath = app_path();
        //$url = 'http://www.smokeylemon.com';
        $fileReport = $tmpDir . DIRECTORY_SEPARATOR . util::sanitize_string($url) . '-result.xml';
        exec("php $appPath/../vendor/bin/w3c-validator.php --report-checkstyle=$fileReport $url");

        $xml = simplexml_load_file($fileReport) or die("Error: Cannot create object");
        //dd($xml);
        $result = array();
        foreach ($xml->file->error as $error) {
            $result[] = '<span class="label label-danger">Warning!</span> <strong>' . $error['message']
                . '</strong></br>(Found on line: ' . $error['line'] . ')';
        }

        return $result;
        //return response()->json(['name' => 'Abigail', 'state' => 'CA']);
        //return Response::json($result);
    }

}