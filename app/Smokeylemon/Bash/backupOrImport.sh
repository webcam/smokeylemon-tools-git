#!/bin/bash

## NOTE: THIS IS REQUIRED ON THE LIVE SERVER. LOCATED AT: /scripts/custom/backupOrImport.sh

#Requirements
#$1 = username_db 
#$2 = password
#$3 = db_table
#$4 = account_name
#$5 = PHP AUTH CODE

USERNAMEDB="$1"
USERNAMEPASSWORD="$2"
DBTABLENAME="$3"
ACCOUNTNAME="$4"
PHP="$5"
DIRECTION="$6"

if [ -z $USERNAMEDB ]; then
echo no USERNAMEDB!
exit 0
fi

if [ -z $USERNAMEPASSWORD ]; then
echo no USERNAMEPASSWORD!
exit 0
fi

if [ -z $DBTABLENAME ]; then
echo no DBTABLENAME!
exit 0
fi

if [ -z $PHP ]; then
echo no PHP!
exit 0
fi

if [ -z $ACCOUNTNAME ]; then
echo no ACCOUNTNAME!
exit 0
fi

if [ -z $DIRECTION ]; then
echo no DIRECTION!
exit 0
fi

if [ $DIRECTION = "toLocal" ]; then
mysqldump --lock-tables=false -u ${USERNAMEDB} -p${USERNAMEPASSWORD} ${DBTABLENAME}  > /home/${ACCOUNTNAME}/www/SQLdump.sql.gz
else
mysql -u ${USERNAMEDB} -p${USERNAMEPASSWORD} ${DBTABLENAME}  < /home/${ACCOUNTNAME}/www/SQLdump.sql.gz
fi