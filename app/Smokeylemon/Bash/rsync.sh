#!/bin/bash

SERVERNAME="$1"
SERVERIP="$2"
REMOTEPATH="$3"
LOCALPATH="$4"
DIRECTION="$5"

if [ -z $SERVERNAME ]; then
echo no SERVERNAME!
exit 0
fi

if [ -z $SERVERIP ]; then
echo no SERVERIP!
exit 0
fi

if [ -z $REMOTEPATH ]; then
echo no REMOTEPATH!
exit 0
fi

if [ -z $LOCALPATH ]; then
echo no LOCALPATH!
exit 0
fi

if [ -z $DIRECTION ]; then
echo no DIRECTION!
exit 0
fi




#--dry-run will fake it..

if [ $DIRECTION = "toLocal" ]; then
#Remote to Local
rsync -avz -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /home/cam/special/idsa_${SERVERNAME}_rsa.pem" --progress root@${SERVERIP}:${REMOTEPATH}/ ${LOCALPATH}
else
#Local to Remote
rsync -avz  -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /home/cam/special/idsa_${SERVERNAME}_rsa.pem" --progress ${LOCALPATH}/ root@${SERVERIP}:${REMOTEPATH}
fi

#check the permissions match the www-data user!!!