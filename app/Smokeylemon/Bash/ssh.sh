#!/bin/bash

USERNAMEDB="$1"
USERNAMEPASSWORD="$2"
DBTABLENAME="$3"
ACCOUNTNAME="$4"
PHP="$5"
SERVERIP="$6"
SERVERNAME="$7"
DIRECTION="$8"

if [ -z $USERNAMEDB ]; then
echo no USERNAMEDB!
exit 0
fi

if [ -z $USERNAMEPASSWORD ]; then
echo no USERNAMEPASSWORD!
exit 0
fi

if [ -z $DBTABLENAME ]; then
echo no DBTABLENAME!
exit 0
fi

if [ -z $ACCOUNTNAME ]; then
echo no ACCOUNTNAME!
exit 0
fi

if [ -z $PHP ]; then
echo no PHP!
exit 0
fi

if [ -z $SERVERIP ]; then
echo no SERVERIP!
exit 0
fi

if [ -z $SERVERNAME ]; then
echo no SERVERNAME!
exit 0
fi

if [ -z $DIRECTION ]; then
echo no DIRECTION!
exit 0
fi

ssh root@${SERVERIP} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i /home/cam/special/idsa_${SERVERNAME}_rsa.pem "/scripts/custom/backupOrImport.sh ${USERNAMEDB} ${USERNAMEPASSWORD} ${DBTABLENAME} ${ACCOUNTNAME} ${PHP} ${DIRECTION} && exit"

#check the permissions match the www-data user!!!