#!/bin/bash


SERVERIP="$1"
ACCOUNTNAME="$2"

if [ -z $SERVERIP ]; then
echo no SERVERIP!
exit 0
fi

if [ -z $ACCOUNTNAME ]; then
echo no ACCOUNTNAME!
exit 0
fi


ssh root@${SERVERIP} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i /home/cam/special/idsa_remote_rsa.pem " /scripts/custom/permissionfix.sh ${ACCOUNTNAME} && exit"

#check the permissions match the www-data user!!!