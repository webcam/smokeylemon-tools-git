#!/bin/bash


SERVERIP="$1"

if [ -z $SERVERIP ]; then
echo no SERVERIP!
exit 0
fi

ssh root@${SERVERIP} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i /home/cam/special/idsa_remote_rsa.pem " /usr/sbin/tmpwatch -am 24 /tmp && exit"

#check the permissions match the www-data user!!!