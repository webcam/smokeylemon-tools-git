<?php 

return array( 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session', 

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Facebook
		 */
        'Facebook' => array(
            'client_id'     => '',
            'client_secret' => '',
            'scope'         => array(),
        ),

        'Google' => array(
            'client_id'     => '489617391848.apps.googleusercontent.com',
            'client_secret' => 'x4EPE4MyszIRQXVI9GJSJH9k',
            'scope'         => array('userinfo_email', 'userinfo_profile'),
        ),

    )

);