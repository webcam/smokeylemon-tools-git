<!-- Feed Entry -->
<!-- Feed Entry -->
<div class="row">
    <div class="large-2 columns mobile-one">{{ HTML::image('assets/smokeytoolsv2/img/server.png') }}</div>
    <div class="large-10 columns">
            <fieldset>
                <legend>Deploy to or Download From</legend>
                <label>Server Name</label>
                {{ Form::select('serverName', array_merge(array('default' => 'Please Select'),$servers),'default', array('id' => 'serverName')) }}
                <div id="downloadResults"></div>
            </fieldset>
    </div>
</div>
<!-- End Feed Entry -->
<hr/>
