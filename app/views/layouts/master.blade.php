<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Smokeylemon Tools Dashbord</title>
{{--        <!-- Bootstrap core CSS -->
    {{ HTML::style('assets/smokeytoolsv2/css/bootstrap.min.css') }}
    {{ HTML::style('assets/smokeytoolsv2/fonts/css/font-awesome.min.css') }}
    {{ HTML::style('assets/smokeytoolsv2/css/animate.min.css') }}
    <!-- Custom styling plus plugins -->
        {{ HTML::style('assets/smokeytoolsv2/css/custom.css') }}
        {{ HTML::style('assets/smokeytoolsv2/css/sl-overrides.css') }}
        {{ HTML::style('assets/smokeytoolsv2/css/maps/jquery-jvectormap-2.0.3.css') }}
        {{ HTML::style('assets/smokeytoolsv2/css/icheck/flat/green.css') }}
        {{ HTML::style('assets/smokeytoolsv2/css/floatexamples.css') }}

        {{ HTML::script('assets/smokeytoolsv2/js/jquery.min.js') }}
        {{ HTML::script('assets/smokeytoolsv2/js/nprogress.js') }}--}}

        {{ HTML::script('assets/smokeytoolsv2/js/custom.min.js') }}
    <!-- Bootstrap -->
        {{ HTML::style('assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}
        <!-- Font Awesome -->
        {{ HTML::style('assets/vendors/font-awesome/css/font-awesome.min.css') }}
        <!-- iCheck -->
        {{ HTML::style('assets/vendors/iCheck/skins/flat/green.css') }}
        <!-- bootstrap-progressbar -->
        {{ HTML::style('assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}
        <!-- jVectorMap -->
        {{ HTML::style('assets/smokeytoolsv2/css/custom.min.css') }}
        {{ HTML::style('assets/smokeytoolsv2/css/sl-overrides.css') }}

    </head>
<body>
<!-- Navigation -->


<!-- BOF New template -->

<body class="nav-md">

<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-dashboard"></i>
                        <span>Lime Admin Tools</span></a>
                </div>
                <div class="clearfix"></div>

                <br/>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">

                            <li><a href="{{ action('DocumentationController@index') }}" class="title {{ (Route::currentRouteNamed('documentation') ? 'active' : '') }} ">
                                    <i class="fa fa-file-text" aria-hidden="true"></i> In House Documentation</a></li>

                            <li><a href="{{ action('WikiController@index') }}" class="title {{ (Route::currentRouteNamed('wiki') ? 'active' : '') }} ">
                                    <i class="fa fa-wikipedia-w" aria-hidden="true"></i> In House Wiki</a></li>

                            <li><a class="title {{ (Route::currentRouteNamed('WfmEditAccount') ? 'active' : '') }}"
                                   href="{{ route('WfmEditAccount') }}">
                                    <i class="fa fa-users" aria-hidden="true"></i>
                                    Edit/View WFM Accounts</a></li>



                            {{--<<li><a><i class="fa fa-home"></i>Site Listings<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    li><a href="{{ action('CmslistingsController@index') }}"
                                           class="title {{ (Route::currentRouteNamed('cmsListings') ? 'active' : '') }}">
                                            CMS Listings</a>
                                    </li>
                                    <li><a href="{{ action('CartlistingsController@index') }}"
                                           class="title {{ (Route::currentRouteNamed('cartListings') ? 'active' : '') }} ">
                                            Opencart Listings</a>
                                    </li>
                                </ul>
                            </li>--}}
                            <li><a><i class="fa fa-edit"></i> Tools <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">

                                    <li>
                                        <a  href="{{ action('QAToolsController@index') }}" class="title {{ (Route::currentRouteNamed('qatools') ? 'active' : '') }} ">
                                            QA Tools</a>
                                    </li>

                                    <li><a href="{{ action('AdminToolsController@index') }}"
                                           class="title {{ (Route::currentRouteNamed('admintools') ? 'active' : '') }} ">
                                            Admin Tools</a>
                                    </li>
                                    <li><a href="{{ action('ServerController@index') }}"
                                           class="title {{ (Route::currentRouteNamed('server') ? 'active' : '') }} ">
                                            Server Status Tools</a>
                                    </li>
                                    <li>
                                        <a href="{{ action('create-an-account') }}"
                                           class="title {{ (Route::currentRouteNamed('create-an-account') ? 'active' : '') }} ">
                                            Create an Account</a>
                                    </li>

                                    <li>
                                        <a href="{{ action('website-mappings') }}"
                                           class="title {{ (Route::currentRouteNamed('website-mappings') ? 'active' : '') }} ">
                                            Local and Live Websites</a>
                                    </li>
                                </ul>


                            </li>

                            <li><a><i class="fa fa-search"></i> Digital Marketing Tools <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">

                                    <li>
                                        <a href="{{ route('GoogleTagManager') }}"
                                           class="title {{ (Route::currentRouteNamed('GoogleTagManager') ? 'active' : '') }} ">
                                            Google Tag Manager</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('GoogleAnalyitics') }}"
                                           class="title {{ (Route::currentRouteNamed('GoogleAnalyitics') ? 'active' : '') }} ">
                                            Google Analyitics</a>
                                    </li>
                                    <li>
                                        <a href="{{ action('BingController@index') }}"
                                           class="title {{ (Route::currentRouteNamed('bing') ? 'active' : '') }} ">
                                            Bing Analyitics</a>
                                    </li>

                                   {{-- <li>
                                        <a  href="{{ action('QAToolsController@index') }}" class="title {{ (Route::currentRouteNamed('qatools') ? 'active' : '') }} ">
                                            QA Tools</a>
                                    </li>--}}


                                </ul>


                            </li>



                            {{-- <li class="has-submenu">
      <a href="#">SEO Reporting</a>
      <ul class="submenu menu vertical">
          <li class="section">
              <a href="{{ route('GoogleTagManager') }}"
                 class="title {{ (Route::currentRouteNamed('GoogleTagManager') ? 'active' : '') }} ">
                  Google Tag Manager</a>
          </li>
          <li class="section">
              <a href="{{ route('GoogleAnalyitics') }}"
                 class="title {{ (Route::currentRouteNamed('GoogleAnalyitics') ? 'active' : '') }} ">
                  Google Analyitics</a>
          </li>
          <li class="section">
              <a href="{{ action('BingController@index') }}"
                 class="title {{ (Route::currentRouteNamed('bing') ? 'active' : '') }} ">
                  Bing Analyitics</a>
          </li>
      </ul>
  </li>--}}

                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>
                            @yield('title')
                        </h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                               {{-- <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>--}}
                        </span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <!-- content starts here -->
                        @yield('content')
                        <!-- content ends here -->
                     {{--  EXAMPL BLOCK

                       <div class="x_panel">
                            <div class="x_title">
                                <h2>Page title <small>Page subtile </small></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                Content...
                            </div>
                        </div>

                        --}}

                    </div>
                </div>

            </div>
        </div>

        <!-- /page content -->

    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<!-- EOF New template -->
<!-- jQuery -->
{{ HTML::script('assets/vendors/jquery/dist/jquery.min.js') }}
<!-- Bootstrap -->
{{ HTML::script('assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}
<!-- FastClick -->
{{ HTML::script('assets/vendors/fastclick/lib/fastclick.js') }}
<!-- NProgress -->
{{ HTML::script('assets/vendors/nprogress/nprogress.js') }}
<!-- iCheck -->
{{ HTML::script('assets/vendors/iCheck/icheck.min.js') }}
<!-- Custom Theme Scripts -->
{{ HTML::script('assets/smokeytoolsv2/js/custom.min.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/validator/validator.min.js') }}
@yield('inlineScripts')

{{--
{{ HTML::script('assets/smokeytoolsv2/js/bootstrap.min.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/validator/validator.min.js') }}
<!-- gauge js -->
{{ HTML::script('assets/smokeytoolsv2/js/gauge/gauge.min.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/gauge/gauge_demo.js') }}
<!-- chart js -->
{{ HTML::script('assets/smokeytoolsv2/js/chartjs/chart.min.js') }}
<!-- bootstrap progress js -->
{{ HTML::script('assets/smokeytoolsv2/js/progressbar/bootstrap-progressbar.min.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/nicescroll/jquery.nicescroll.min.js') }}
<!-- icheck -->
{{ HTML::script('assets/smokeytoolsv2/js/icheck/icheck.min.js') }}
<!-- daterangepicker -->
{{ HTML::script('assets/smokeytoolsv2/js/moment/moment.min.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/datepicker/daterangepicker.js') }}

{{ HTML::script('assets/smokeytoolsv2/js/custom.js') }}

<!-- flot js -->
{{ HTML::script('assets/smokeytoolsv2/js/flot/jquery.flot.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/flot/jquery.flot.pie.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/flot/jquery.flot.orderBars.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/flot/jquery.flot.time.min.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/flot/jquery.date.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/flot/jquery.flot.spline.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/flot/jquery.flot.stack.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/flot/curvedLines.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/flot/jquery.flot.resize.js') }}
<script>
    $(document).ready(function () {
        // [17, 74, 6, 39, 20, 85, 7]
        //[82, 23, 66, 9, 99, 6, 2]
        var data1 = [
            [gd(2012, 1, 1), 17],
            [gd(2012, 1, 2), 74],
            [gd(2012, 1, 3), 6],
            [gd(2012, 1, 4), 39],
            [gd(2012, 1, 5), 20],
            [gd(2012, 1, 6), 85],
            [gd(2012, 1, 7), 7]
        ];

        var data2 = [
            [gd(2012, 1, 1), 82],
            [gd(2012, 1, 2), 23],
            [gd(2012, 1, 3), 66],
            [gd(2012, 1, 4), 9],
            [gd(2012, 1, 5), 119],
            [gd(2012, 1, 6), 6],
            [gd(2012, 1, 7), 9]
        ];
        $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
            data1, data2
        ], {
            series: {
                lines: {
                    show: false,
                    fill: true
                },
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.4
                },
                points: {
                    radius: 0,
                    show: true
                },
                shadowSize: 2
            },
            grid: {
                verticalLines: true,
                hoverable: true,
                clickable: true,
                tickColor: "#d5d5d5",
                borderWidth: 1,
                color: '#fff'
            },
            colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
            xaxis: {
                tickColor: "rgba(51, 51, 51, 0.06)",
                mode: "time",
                tickSize: [1, "day"],
                //tickLength: 10,
                axisLabel: "Date",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 10
                //mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
            },
            yaxis: {
                ticks: 8,
                tickColor: "rgba(51, 51, 51, 0.06)",
            },
            tooltip: false
        });

        function gd(year, month, day) {
            return new Date(year, month - 1, day).getTime();
        }
    });
</script>

<!-- worldmap -->
{{ HTML::script('assets/smokeytoolsv2/js/maps/jquery-jvectormap-2.0.3.min.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/maps/gdp-data.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/maps/jquery-jvectormap-world-mill-en.js') }}
{{ HTML::script('assets/smokeytoolsv2/js/maps/jquery-jvectormap-us-aea-en.js') }}
<!-- pace -->
{{ HTML::script('assets/smokeytoolsv2/js/pace/pace.min.js') }}
<script>
    $(function () {
        $('#world-map-gdp').vectorMap({
            map: 'world_mill_en',
            backgroundColor: 'transparent',
            zoomOnScroll: false,
            series: {
                regions: [{
                    values: gdpData,
                    scale: ['#E6F2F0', '#149B7E'],
                    normalizeFunction: 'polynomial'
                }]
            },
            onRegionTipShow: function (e, el, code) {
                el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
            }
        });
    });
</script>
<!-- skycons -->
{{ HTML::script('assets/smokeytoolsv2/js/skycons/skycons.min.js') }}
<script>
    var icons = new Skycons({
                "color": "#73879C"
            }),
            list = [
                "clear-day", "clear-night", "partly-cloudy-day",
                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                "fog"
            ],
            i;

    for (i = list.length; i--;)
        icons.set(list[i], list[i]);

    icons.play();
</script>

<!-- dashbord linegraph -->
<script>
    var doughnutData = [{
        value: 30,
        color: "#455C73"
    }, {
        value: 30,
        color: "#9B59B6"
    }, {
        value: 60,
        color: "#BDC3C7"
    }, {
        value: 100,
        color: "#26B99A"
    }, {
        value: 120,
        color: "#3498DB"
    }];
    var myDoughnut = new Chart(document.getElementById("canvas1").getContext("2d")).Doughnut(doughnutData);
</script>
<!-- /dashbord linegraph -->
<!-- datepicker -->
<script type="text/javascript">
    $(document).ready(function () {

        var cb = function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
        }

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function () {
            console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
            console.log("cancel event fired");
        });
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });
    });
</script>
<script>
    NProgress.done();
</script>--}}


</body>
</html>