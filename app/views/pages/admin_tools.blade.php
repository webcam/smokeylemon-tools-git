@extends('layouts.master')

@section('title', 'Lime Admin Tools')

@section('content')
    <!-- Feed Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Create a Client site</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/createsite.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">

                        <form class="form-horizontal form-label-left" name="createClient"
                              id="createClient">

                            <script type="text/javascript">
                                function changeFunc() {
                                    var selectBox = document.getElementById("siteType");
                                    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
                                    if (selectedValue != 'select' && selectedValue != 'client') {
                                        document.getElementById("createSiteActionControls").style.display = 'block';
                                    } else {
                                        document.getElementById("createSiteActionControls").style.display = 'none';
                                    }
                                }
                            </script>

                            <div class="form-group">
                                <label>Action:</label>
                                <select name="siteType" id="siteType" onchange="changeFunc();" class="form-control">
                                    <option value="select" selected>Select One</option>
                                    <option value="client">Create Client Folders Only</option>
                         {{--           <option value="cms">Create CMS</option>
                                    <option value="opencart">Create Shopping Cart</option>--}}
                                    <option value="opencart2">Create Shopping Cart (OpenCart2)</option>
                                    <option value="silverstripe">Create Silverstipe CMS</option>
                                    <!--option value="mobile">Mobile Application</option-->
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Client Name - A valid client name (aplahabetical, lowercase, no spaces, max 8
                                    char)</label>
                                <input type="text" name="clientName" id="clientName" placeholder="Client Name"
                                       class="form-control" required="" data-parsley-maxlength="8"
                                       data-parsley-error-message="A valid client name (alphabetical, lowercase, no spaces, max 8 char) is required">
                            </div>
                            <div id="createSiteActionControls" style="display:none">
                                <div class="form-group">
                                    <label>Client Live URL - A valid url (aplahabetical, lowercase, no spaces, NO
                                        protocol [HTTP or HTTPS], NO sub-domain [WWW])</label>
                                    <input type="text" name="liveURL" id="liveURL" placeholder="Live URL"
                                           class="form-control" required=""
                                           data-parsley-error-message="A valid url (alphabetical, lowercase, no spaces, NO protocol [HTTP or HTTPS], NO sub-domain [WWW]) is required.">
                                </div>
                            </div>
                            <div id="createResults"></div>
                            <br/>
                            <button class="btn btn-primary createClient">Create</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Feed Entry -->


    <!-- Feed Entry -->
    <!-- Feed Entry -->
    {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
        {{--<div class="x_panel">--}}
            {{--<div class="x_title">--}}
                {{--<h2>Import Webflow Assets & Favicon Files (CMS Only)</h2>--}}
                {{--<div class="clearfix"></div>--}}
            {{--</div>--}}
            {{--<div class="x_content">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/webflow.png', '', array('class' => 'img-circle img-responsive')) }}</div>--}}
                    {{--<div class="col-sm-9">--}}

                        {{--<form name="importClient" class="form-horizontal form-label-left">--}}
                            {{--<p><i>For this tool to work there has to be a Webflow Zipped file (called something like--}}
                                    {{--<strong>'name.webflow.zip'</strong>)--}}
                                    {{--in the <strong>'_site'</strong> directory of the client!</i></p>--}}
                            {{--<div class="form-group">--}}
                                {{--{{ Form::label('siteToImport', 'Client Name (Local Lime Server)') }}--}}
                                {{--{{ Form::select('siteToImport', array('select' => '-- Select One --') + $localSites, null, array('class'=>'form-control')) }}--}}
                            {{--</div>--}}

                            {{--<div id="importResults"></div>--}}
                            {{--<button class="btn btn-primary importClient">Import</button>--}}
                        {{--</form>--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!-- End Feed Entry -->



    <!-- Feed Entry -->
    <!--div class="row">
        <div class="large-2 columns mobile-one">{{ HTML::image('assets/smokeytoolsv2/img/server.png') }}</div>
        <div class="large-10 columns">
            <form name="uploadClient">
                <fieldset>
                    <legend>Upload To Staging</legend>
                    {{ Form::label('siteToUpload', 'Client Name (Local Lime Server)') }}
    {{ Form::select('siteToUpload', array('select' => '-- Select One --') + $localSites, null, array('class'=>'form-control')) }}


            <div id="uploadResults"></div>
            <button class="btn btn-primary uploadClient">Upload</button>
        </fieldset>
    </form>
</div>
</div-->
    <!-- End Feed Entry -->

    <div class="clearfix"></div>
    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Fix File Permissions</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/fixpermissions.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">

                        <form name="fixClient" class="form-horizontal form-label-left">
                            <div class="form-group">
                                {{ Form::label('siteTypeFix', 'Client Name (Local Lime Server)') }}
                                {{ Form::select('siteTypeFix', array('select' => '-- Select One --') + $localSites, null, array('class'=>'form-control')) }}
                            </div>
                            <div id="fixResults"></div>
                            <button class="btn btn-primary fixClient">Fix</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->
    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Fix File Permissions & Export Site for Deployment</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/export_site.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">

                        <form name="exportSite" class="form-horizontal form-label-left">
                            <div class="form-group">
                                {{ Form::label('siteToExport', 'Client Name (Lime Server)') }}
                                {{ Form::select('siteToExport', array('select' => '-- Select --') + $localSites, null, array('class'=>'form-control')) }}
                            </div>
                            <div id="exportResults"></div>
                            <button class="btn btn-primary exportSite">Export</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->
    <div class="clearfix"></div>
    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Archive Site</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/database.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">
                        <form name="archiveSite" class="form-horizontal form-label-left">
                            <div class="form-group">
                                {{ Form::label('siteToArchive', 'Client Name (Lime Server)') }}
                                {{ Form::select('siteToArchive', array('select' => '-- Select --') + $localSites, null, array('class'=>'form-control')) }}
                            </div>
                            <div id="archiveResults"></div>
                            <button class="btn btn-primary archiveSite">Archive</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->
    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Remove Client Folder</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/delete.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">

                        <form name="removeClient">
                            <div class="form-group">
                                {{ Form::label('clientToRemove', 'Client Name (Lime Server)') }}
                                {{ Form::select('clientToRemove', array('select' => '-- Select --') + $localSites, null, array('class'=>'form-control')) }}
                            </div>
                            <div id="removeResults"></div>
                            <button type="button" class="btn btn-primary btn-danger" data-toggle="modal"
                                    data-target=".bs-example-modal-lg">Remove
                            </button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->
    <div class="clearfix"></div>
    <!-- EOF Entry -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="lightboxRemoveClient">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Remove Client Folder</h4>
                </div>
                <div class="modal-body">

                    <form name="removeClientConfirm">
                        {{ Form::label('clientToRemoveConfirm', 'Please type the client name to remove!') }}
                        {{ Form::text('clientToRemoveConfirm') }}
                        {{ Form::label('clientToRemovePass', 'Please enter the pass phrase') }}
                        {{ Form::password('clientToRemovePass') }}
                        <button class="btn btn-primary removeClient">Remove</button>
                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div>{{ $myErrorLog }}</div>
@stop

@section('inlineScripts')
    <script type="text/javascript">
        // Function to restart a specific service when clicked via button. Prompts user with Foundations Reveal Modal.  Also return current service status's

        /*
        $(document).foundation({
            abide: {
                validators: {
                    username: function (el, required, parent) {
                        var valid,
                                thisVal = $(el).val();
                        if (thisVal.length >= 5 && thisVal.length <= 8) {
                            valid = true;
                        } else {
                            valid = false;
                        }
                        return valid;
                    }
                }
            }
        });
        */


        $(document).ready(function ($) {

            var button = $('.createClient'),
                    clientName = $('#clientName'),
                    liveURL = $('#liveURL'),
                    createResults = $('#createResults'),
                    siteType = $("#siteType"),
                    siteTypeVal = siteType.val(),
                    clientNameVal = clientName.val(),
                    liveURLVal = liveURL.val();

            $(siteType).change(function () {
                siteTypeVal = $(this).val();
            });

            $(clientName).on('keyup change', function () {
                clientNameVal = $(this).val();
            });

            $(liveURL).on('keyup change', function () {
                liveURLVal = $(this).val();
            });

            $(button).click(function (e) {
                $('#createClient').on('valid submit', function (e) {
                    //console.log(siteTypeVal + ' ' + clientNameVal + ' ' + liveURLVal);
                    //if (clientNameVal.length > 1) {
                    e.stopPropagation();
                    e.preventDefault();
                    $(createResults).html('<strong>Check the Client Name contains no spaces</strong>');
                    $(createResults).empty();
                    $(createResults).html("<p style='text-align: center'><h2>Now Loading...</h2><br/><img src='assets/smokeytoolsv2/img/ring-alt.svg'></p>");
                    $(button).attr('disabled', 'disabled');
                    //disbale button!
                    var url = "<?php echo URL::route("createLocal") ?>";
                    $.ajax({
                        type: 'POST', // type of request either Get or Post
                        url: url, // Url of the page where to post data and receive response
                        data: {
                            clientName: clientNameVal,
                            siteType: siteTypeVal,
                            liveURL: liveURLVal
                        }, // data to be post
                        success: function (data) {
                            // On sucess, empty the create results div and pouplate with data return..
                            $(createResults).empty();
                            $(createResults).html(data);
                            // enable button again.. nah
                            //$($button).removeAttr('disabled');
                        }
                    });
                    // });
                    // EOF Restart services..
                    // }
                });

            });
        });
    </script>

    <script type="text/javascript">
        // Function to restart a specific service when clicked via button. Prompts user with Foundations Reveal Modal.  Also return current service status's

        $(document).ready(function ($) {

            // Define Global Variables...
            // 1) button
            // 2) rfixResult empty div for population..
            // 3) select drop down

            var $button = $('.importClient'),
                    $importResults = $('#importResults'),
                    $siteToImport = $("#siteToImport option:selected").val();

            $("#siteToImport").change(function () {
                $siteToImport = $("#siteToImport option:selected").val();
            });

            $($button).click(function (event) {
                event.preventDefault();
                $($importResults).empty();
                $($importResults).html("<p style='text-align: center'><h2>Now Loading...</h2><br/><img src='assets/smokeytoolsv2/img/ring-alt.svg'></p>");
                $($button).attr('disabled', 'disabled');
                //disbale button!
                var url = "{{ URL::action('importClient') }}";
                $.ajax({
                    type: 'POST', // type of request either Get or Post
                    url: url, // Url of the page where to post data and receive response
                    data: {
                        siteToImport: $siteToImport
                    }, // data to be post
                    success: function (data) {
                        // On sucess, empty the create results div and pouplate with data return..
                        $($importResults).empty();
                        $($importResults).html(data);
                        // enable button again.. nahhh
                        $($button).removeAttr('disabled');
                    }
                });
            });
            // EOF Restart services..
        });
    </script>

    <!--script-- type="text/javascript">
        // Function to restart a specific service when clicked via button. Prompts user with Foundations Reveal Modal.  Also return current service status's

        {{--$(document).ready(function ($) {--}}

    {{--// Define Global Variables...--}}
    {{--// 1) button--}}
    {{--// 2) rfixResult empty div for population..--}}
    {{--// 3) select drop down--}}

    {{--var $button = $('.uploadClient'),--}}
    {{--$fixResults = $('#uploadResults'),--}}
    {{--$siteType = $("#siteToUpload option:selected").val();--}}

    {{--$("#siteToUpload").change(function () {--}}
    {{--$siteToUpload = $("#siteToUpload option:selected").val();--}}
    {{--});--}}

    {{--$($button).click(function (event) {--}}
    {{--event.preventDefault();--}}
    {{--$($fixResults).empty();--}}
    {{--$($fixResults).html('<p><strong>Please Wait...</strong></p>');--}}
    {{--$($button).attr('disabled', 'disabled');--}}
    {{--//disbale button!--}}

    {{--var exportURL = "{{ URL::action('export-local') }}";--}}
    {{--var uploadURL = "{{ URL::action('upload-staging') }}";--}}

    {{--$.ajax({--}}
    {{--type: 'POST', // type of request either Get or Post--}}
    {{--url: exportURL, // Url of the page where to post data and receive response--}}
    {{--data: {--}}
    {{--siteToExport: $siteToUpload--}}
    {{--}, // data to be post--}}
    {{--success: function (data) {--}}
    {{--// On sucess, empty the create results div and pouplate with data return..--}}
    {{--$($fixResults).empty();--}}
    {{--$($fixResults).html(data);--}}

    {{--$($fixResults).append('<p><strong>Uploading To Staging Server</strong></p>');--}}

    {{--$.ajax({--}}
    {{--type: 'POST', // type of request either Get or Post--}}
    {{--url: uploadURL, // Url of the page where to post data and receive response--}}
    {{--data: {--}}
    {{--siteToUpload: $siteToUpload--}}
    {{--}, // data to be post--}}
    {{--success: function (data) {--}}
    {{--$($fixResults).append(data);--}}
    {{--// enable button again.. nahhh--}}
    {{--$($button).removeAttr('disabled');--}}
    {{--}--}}
    {{--});--}}

    {{--}--}}
    {{--});--}}
    {{--});--}}
            // EOF Restart services..
        });
    </script-->

    <script type="text/javascript">
        // Function to restart a specific service when clicked via button. Prompts user with Foundations Reveal Modal.  Also return current service status's

        $(document).ready(function ($) {

            // Define Global Variables...
            // 1) button
            // 2) rfixResult empty div for population..
            // 3) select drop down

            var $button = $('.fixClient'),
                    $fixResults = $('#fixResults'),
                    $siteType = $("#siteTypeFix option:selected").val();

            $("#siteTypeFix").change(function () {
                $siteType = $("#siteTypeFix option:selected").val();
            });

            $($button).click(function (event) {
                event.preventDefault();
                $($fixResults).empty();
                $($fixResults).html("<p style='text-align: center'><h2>Now Loading...</h2><br/><img src='assets/smokeytoolsv2/img/ring-alt.svg'></p>");
                $($button).attr('disabled', 'disabled');
                //disbale button!
                var url = "<?php echo URL::action('Ajax\AjaxController@fixLocal') ?>";
                $.ajax({
                    type: 'POST', // type of request either Get or Post
                    url: url, // Url of the page where to post data and receive response
                    data: {
                        siteType: $siteType
                    }, // data to be post
                    success: function (data) {
                        // On sucess, empty the create results div and pouplate with data return..
                        $($fixResults).empty();
                        $($fixResults).html(data);
                        // enable button again.. nahhh
                        $($button).removeAttr('disabled');
                    }
                });
            });
            // EOF Restart services..
        });
    </script>

    <script type="text/javascript">
        // Function to EXPORT WEBSITE

        $(document).ready(function ($) {

            // Define Global Variables...
            // 1) button
            // 2) rfixResult empty div for population..
            // 3) select drop down

            var $button = $('.exportSite'),
                    $exportResults = $('#exportResults'),
                    $siteToExport = $("#siteToExport option:selected").val();

            $("#siteToExport").change(function () {
                $siteToExport = $("#siteToExport option:selected").val();
            });


            $($button).click(function (event) {
                event.preventDefault();
                $($exportResults).empty();
                $($exportResults).html("<p style='text-align: center'><h2>Now Loading...</h2><br/><img src='assets/smokeytoolsv2/img/ring-alt.svg'></p>");
                $($button).attr('disabled', 'disabled');
                var url = "{{ URL::action('export-local') }}";

                $.ajax({
                    type: 'POST', // type of request either Get or Post
                    url: url, // Url of the page where to post data and receive response
                    data: {
                        siteToExport: $siteToExport
                    }, // data to be post
                    success: function (data) {
                        // On sucess, empty the create results div and pouplate with data return..
                        $($exportResults).empty();
                        $($exportResults).html(data);
                        // enable button again.. nahhh
                        $($button).removeAttr('disabled');
                    }
                });
            });
            // EOF Restart services..
        });
    </script>

    <script type="text/javascript">
        // Function to EXPORT WEBSITE

        $(document).ready(function ($) {

            // Define Global Variables...
            // 1) button
            // 2) rfixResult empty div for population..
            // 3) select drop down

            var $button = $('.archiveSite'),
                    $archiveResults = $('#archiveResults'),
                    $siteToArchive = $("#siteToArchive option:selected").val();

            $("#siteToArchive").change(function () {
                $siteToArchive = $("#siteToArchive option:selected").val();
            });


            $($button).click(function (event) {
                event.preventDefault();
                $($archiveResults).empty();
                $($archiveResults).html("<p style='text-align: center'><h2>Now Loading...</h2><br/><img src='assets/smokeytoolsv2/img/ring-alt.svg'></p>");
                $($button).attr('disabled', 'disabled');
                var url = "{{ URL::action('archive-local') }}";

                $.ajax({
                    type: 'POST', // type of request either Get or Post
                    url: url, // Url of the page where to post data and receive response
                    data: {
                        siteToArchive: $siteToArchive
                    }, // data to be post
                    success: function (data) {
                        // On sucess, empty the create results div and pouplate with data return..
                        $($archiveResults).empty();
                        $($archiveResults).html(data);
                        // enable button again.. nahhh
                        $($button).removeAttr('disabled');
                    }
                });
            });
            // EOF Restart services..
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('input').bind('copy paste', function (e) {
                e.preventDefault();
            });
        });


        // Function to remove WEBSITE

        $(document).ready(function ($) {

            // Define Global Variables...
            // 1) button
            // 2) rfixResult empty div for population..
            // 3) select drop down

            var $lightbox = $('.lightboxRemoveClient'),
                    $lightboxClose = $('#lightboxRemoveClient .w-lightbox-control.w-lightbox-close'),
                    $button = $('.removeClient'),
                    $removeResults = $('#removeResults'),
                    $clientToRemove = $("#clientToRemove option:selected").val();

            $("#clientToRemove").change(function () {
                $clientToRemove = $("#clientToRemove option:selected").val();
                if ($clientToRemove != 'select') {
                    $($lightbox).removeAttr('disabled');
                } else {
                    $($lightbox).attr('disabled', 'disabled');
                }
            });

            $($lightbox).click(function (event) {
                event.preventDefault();
                $($removeResults).empty();
                $($removeResults).html("<p style='text-align: center'><h2>Now Loading...</h2><br/><img src='assets/smokeytoolsv2/img/ring-alt.svg'></p>");
                $("#clientToRemove").attr('disabled', 'disabled');
                $("#clientToRemoveConfirm").attr('placeholder', $clientToRemove);
                $($lightbox).attr('disabled', 'disabled');
                $("#lightboxRemoveClient").css('display', 'block').css('opacity', '1');
            });

            $($lightboxClose).click(function (event) {
                event.preventDefault();
                $($removeResults).empty();
                $($removeResults).html("<p style='text-align: center'><h2>Now Loading...</h2><br/><img src='assets/smokeytoolsv2/img/ring-alt.svg'></p>");
                $("#lightboxRemoveClient").css('display', 'none').css('opacity', '0');
                $("#clientToRemoveConfirm").attr('placeholder', '');
            });

            $($button).click(function (event) {
                event.preventDefault();
                $clientToRemoveConfirm = $("#clientToRemoveConfirm").val();
                $clientToRemovePass = $("#clientToRemovePass").val();
                if ($clientToRemoveConfirm == $clientToRemove && confirm("Are you sure you want to delete this client?")) {
                    $($removeResults).empty();
                    $($removeResults).html("<p style='text-align: center'><h2>Now Loading...</h2><br/><img src='assets/smokeytoolsv2/img/ring-alt.svg'></p>");
                    $("#clientToRemove").attr('disabled', 'disabled');
                    $($button).attr('disabled', 'disabled');
                    var url = "{{ URL::action('remove-local') }}";

                    $.ajax({
                        type: 'POST', // type of request either Get or Post
                        url: url, // Url of the page where to post data and receive response
                        data: {
                            clientToRemove: $clientToRemove,
                            clientToRemovePass: $clientToRemovePass
                        }, // data to be post
                        success: function (data) {
                            // On sucess, empty the create results div and pouplate with data return..
                            $($removeResults).empty();
                            $($removeResults).html(data);
                            $("#lightboxRemoveClient").css('display', 'none').css('opacity', '0');
                            $("#clientToRemoveConfirm").attr('placeholder', '');
                        }
                    });
                }
            });
            // EOF Restart services..
        });
    </script>

@stop