@extends('layouts.master')

@section('title', 'Important Documentation')

@section('content')
<div class="row">
    <div class="large-12 columns">

        <dl class="sub-nav">
            <dt>Filter:</dt>
            @foreach ($folders as $folder)
            <dd><a href="#{{ $folder['href'] }}">{{ $folder['title'] }}</a></dd>
            @endforeach
        </dl>
        <hr/>

        @foreach ($folders as $folder)
        <div id="{{ $folder['href'] }}">
        <h2>{{ $folder['title'] }}</h2>
            {{ $folder['dir'] }}
        </div>
        @endforeach



</div>
</div>
@stop

@section('inlineScripts')
{{ HTML::script('assets/smokeytoolsv2/js/jquery.gdocsviewer.min.js') }}

<script>
    function printFile(URL) {
        var W = window.open(URL);
        W.window.print();
    }
    $(document).ready(function ($) {

        var tableRowWidth = $('a.embed').parent().width(),
            tableRowHeight = 350;

        console.log(tableRowWidth + ' : ' + tableRowHeight);

        $('a.embed').gdocsViewer({ width: tableRowWidth, height: tableRowHeight });


    });
</script>
@stop