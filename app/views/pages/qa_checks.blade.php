v@extends('layouts.master')

@section('title', 'QA Checks')

@section('content')



    <!-- BOF Entry -->
    <div class="col-xs-10">
        <div class="x_panel">
            <div class="x_title">
                <h2>Speed Test</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/test.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">
                        <form name="qaChecks" id="qaChecks" class="form-horizontal form-label-left"
                              data-toggle="validator" role="form">
                            <div class="form-group">
                                <label>Domain Name</label>
                                {{ Form::text('domain','',array('id' => 'domain','required','data-domain'=>'true','class'=>'form-control')) }}
                            </div>
                            <button type="submit" id="submit" class="btn btn-button btn-primary">Get Results
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->

    <!-- BOF Pagespeed -->
    <div class="col-md-4 col-sm-4 col-xs-10" id="speedResults" style="display:none">
        <div class="x_panel">
            <div class="x_title">
                <h2>YSlow and Pagespeed Score Results</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <!--<div id="echart_guage" style="height:370px;"></div>-->

            </div>
        </div>
    </div>
    <!-- EOF Pagespeed -->


    <!-- BOF Pagespeed -->
    <div class="col-md-6 col-sm-6 col-xs-12" id="validationResults" style="display:none">
        <div class="x_panel">
            <div class="x_title">
                <h2>HTML Validation Results</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">


            </div>
        </div>
    </div>
    <!-- EOF Pagespeed -->


    <!-- BOF IFramely -->
    <div class="col-md-10 col-sm-10 col-xs-12" id="iframelyResults" style="display:none">
        <div class="x_panel">
            <div class="x_title">
                <h2>Iframley Results</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">


            </div>
        </div>
    </div>
    <!-- EOF IFramely -->




@stop


@section('inlineScripts')

    <!-- echart -->
    {{ HTML::script('assets/smokeytoolsv2/js/echart/echarts-all.js') }}
    {{ HTML::script('assets/smokeytoolsv2/js/echart/green.js') }}

    <script type="text/javascript">
        $(document).ready(function ($) {

            $('#qaChecks').validator({
                custom: {
                    domain: function (el) {
                        var valid, content = $(el).val();
                        valid = false;
                        // strip off "http://" and/or "www."
                        content = content.replace("http://", "");
                        content = content.replace("www.", "");

                        //console.log(content);
                        var reg = /^(?!:\/\/)([a-zA-Z0-9]+\.)?[a-zA-Z0-9][a-zA-Z0-9-]+\.[a-zA-Z]{2,6}?$/i;
                        if (reg.test(content)) {
                            valid = true;
                            return valid;
                        }
                    }
                },
                errors: {
                    domain: "Wrong, please check domain"
                }
            });

            var urlToPostValidation = "{{ URL::route('postHTMLValidation') }}",
                    urlToPostSpeed = "{{ URL::route('postSpeedCheck') }}",
                    urlToPostIframley = "{{ URL::route('postIfamely') }}",
            //urlToPostIframley = "http://iframe.ly/api/iframely?url=",
                    domainToCheck = $('#domain'),
                    speedResults = $('#speedResults'),
                    validationResults = $('#validationResults'),
                    iframelyResults = $('#iframelyResults'),
                    submitButton = $('#submit'),
                    loadingElm = "<p style='text-align: center'><h2>Now Loading...</h2><br/><img src='assets/smokeytoolsv2/img/ring-alt.svg'></p>",
                    domainVal = $(domainToCheck).val(),

                    doSpeedTest = function (domainVal) {
                        speedResults.show();
                        speedResults.find('.x_content').html(loadingElm);
                        $.post(urlToPostSpeed, {
                            domain: domainVal
                        })
                                .done(function (data) {
                                    console.log(data);

                                    speedResults.find('.x_content').html('');
                                    if (data.yslow_score !== undefined && data.pagespeed_score !== undefined) {
                                        speedResults.find('.x_content').append("<div id='echart_guage' style='height:370px;'/>");
                                        var calcedValue = (parseInt(data.yslow_score) + parseInt(data.pagespeed_score)) / 2;
                                        var myChart = echarts.init(document.getElementById('echart_guage'), theme);
                                        myChart.setOption({
                                            tooltip: {
                                                formatter: "{a} <br/>{b} : {c}%"
                                            },
                                            toolbox: {
                                                show: true,
                                                feature: {
                                                    restore: {
                                                        show: true
                                                    },
                                                    saveAsImage: {
                                                        show: true
                                                    }
                                                }
                                            },
                                            series: [{
                                                name: 'Speed Performance',
                                                type: 'gauge',
                                                center: ['50%', '50%'],
                                                radius: [0, '75%'],
                                                startAngle: 140,
                                                endAngle: -140,
                                                min: 0,
                                                max: 100,
                                                precision: 0,
                                                splitNumber: 10,
                                                axisLine: {
                                                    show: true,
                                                    lineStyle: {
                                                        color: [
                                                            [0.2, '#D91E18'],
                                                            [0.4, '#F5AB35'],
                                                            [0.6, '#F5AB35'],
                                                            [0.8, '#F4D03F'],
                                                            [1, '#03A678']
                                                        ],
                                                        width: 30
                                                    }
                                                },
                                                axisTick: {
                                                    show: true,
                                                    splitNumber: 5,
                                                    length: 8,
                                                    lineStyle: {
                                                        color: '#eee',
                                                        width: 1,
                                                        type: 'solid'
                                                    }
                                                },
                                                axisLabel: {
                                                    show: true,
                                                    formatter: function (v) {
                                                        switch (v + '') {
                                                            case '10':
                                                                return 'd';
                                                            case '30':
                                                                return 'c';
                                                            case '60':
                                                                return 'b';
                                                            case '90':
                                                                return 'a';
                                                            default:
                                                                return '';
                                                        }
                                                    },
                                                    textStyle: {
                                                        color: '#333'
                                                    }
                                                },
                                                splitLine: {
                                                    show: true,
                                                    length: 30,
                                                    lineStyle: {
                                                        color: '#eee',
                                                        width: 2,
                                                        type: 'solid'
                                                    }
                                                },
                                                pointer: {
                                                    length: '80%',
                                                    width: 8,
                                                    color: 'auto'
                                                },
                                                title: {
                                                    show: true,
                                                    offsetCenter: ['-65%', -10],
                                                    textStyle: {
                                                        color: '#333',
                                                        fontSize: 15
                                                    }
                                                },
                                                detail: {
                                                    show: true,
                                                    backgroundColor: 'rgba(0,0,0,0)',
                                                    borderWidth: 0,
                                                    borderColor: '#ccc',
                                                    width: 100,
                                                    height: 40,
                                                    offsetCenter: ['-60%', 10],
                                                    formatter: '{value}%',
                                                    textStyle: {
                                                        color: 'auto',
                                                        fontSize: 30
                                                    }
                                                },
                                                data: [{
                                                    value: calcedValue,
                                                    name: 'Average Score'
                                                }]
                                            }]
                                        });
                                    } else {
                                        speedResults.find('.x_content').append("<p>" + data + "</p>");
                                    }

                                    //myChart.refresh();

                                    //speedResults.append(results);

                                    //console.log(data);
                                });
                    },

                    doValidationTest = function (domainVal) {
                        validationResults.show();
                        validationResults.find('.x_content').html(loadingElm);
                        $.post(urlToPostValidation, {
                            domain: domainVal
                        })
                                .done(function (data) {
                                    validationResults.find('.x_content').html('');
                                    $.each(data, function (index, data) {
                                        validationResults.find('.x_content').append('<p>' + data + '</p><hr/>');
                                    });

                                    //console.log(data);
                                });
                    },

                    doIfamleyTest = function (domainVal) {
                        iframelyResults.show();
                        iframelyResults.find('.x_content').html(loadingElm);
                        console.log(urlToPostIframley);
                        $.post(urlToPostIframley, {
                            domain: domainVal
                        })
                                .done(function (data) {
                                    data = jQuery.parseJSON(data);
                                    //console.log(data);
                                    iframelyResults.find('.x_content').html('');


                                    $.each(data.meta, function (index, data) {
                                        iframelyResults.find('.x_content').append('<p><strong>' + index + '</strong>: ' + data + '</p><hr/>');
                                    });

                                  /*  $.each(data.links, function (index, dataParent) {
                                        $.each(dataParent, function (index, subData) {
                                            console.log(subData);
                                            iframelyResults.find('.x_content').append('<p><strong>' + index + '</strong>: ' + subData + '</p><hr/>');
                                        });
                                    });

                                    $.each(data.rel, function (index, data) {
                                        iframelyResults.find('.x_content').append('<p><strong>' + index + '</strong>: ' + data + '</p><hr/>');
                                    });*/


                                });
                    };


            // set submit dsabled until valid...

            $(domainToCheck).on('keyup change', function () {
                domainVal = $(domainToCheck).val();
            });

            $(submitButton).click(function (e) {
                domainVal = $(domainToCheck).val();
                if (domainVal.match("^http://")) {
                    // do this if begins with Hello
                } else {
                    domainVal = 'http://' + domainVal;
                }
                $('#qaChecks').validator().on('submit', function (e) {
                    if (e.isDefaultPrevented()) {
                        // handle the invalid form...
                        return false;
                    } else {
                        // everything looks good!
                        console.log('the return is: ' + e.type);
                        if (e.type === "submit") {
                            e.preventDefault();
                            console.log('valid!, geting data for ' + domainVal + '...');
                            doSpeedTest(domainVal);
                            doValidationTest(domainVal);
                            doIfamleyTest(domainVal)
                        }
                    }
                });
            });
        });

    </script>

@stop