@extends('layouts.master')

@section('title', 'Google Anylitics')

@section('content')

<form data-abide>
    <!-- Feed Entry -->
    <!-- Feed Entry -->
    <div class="row">
        <div class="large-2 columns mobile-one">{{ HTML::image('assets/smokeytoolsv2/img/server.png') }}</div>
        <div class="large-10 columns">
            <div id="domainsHolder">
                <select id="domainsSelect"></select>
            </div>

        </div>
    </div>
    <!-- End Feed Entry -->
    <hr/>
    <!-- Feed Entry -->
    <!-- Feed Entry -->
    <div class="row" id="graphHolder">
        <div class="large-2 columns mobile-one">{{ HTML::image('assets/smokeytoolsv2/img/server.png') }}</div>
        <div class="large-10 columns">
            <div id="googleResults"></div>
            <div id="chart_div"></div>
        </div>
    </div>
    <!-- End Feed Entry -->
    <hr/>


</form>


@stop


@section('inlineScripts')


<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages': ['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart() {

    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');
    data.addRows([
        ['Mushrooms', 3],
        ['Onions', 1],
        ['Olives', 1],
        ['Zucchini', 1],
        ['Pepperoni', 2]
    ]);

    // Set chart options
    var options = {'title': 'How Much Pizza I Ate Last Night',
        'width': 400,
        'height': 300};

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}


// some global stuff we want..
var domainsHolder = $('#domainsHolder'),
    domainsSelect = $('#domainsSelect'),
    graphHolder = $('#graphHolder'),
    googleResults = $('#googleResults');

//initOptions('enabled');

$.ajaxSetup({ cache: false });


$(document).on('ready', function () {


    var html = '',
        gen = '<p id="gen">Now Generating...</p>';


    domainsHolder.show().addClass('loading');

    domainsHolder.append(gen);

    $.getJSON('{{ action("GoogleController@DoAnalyitics") }}', function (data) {

        var len = data.domains.length;
        html += '<option value="selectone">Select One</option>';
        for (var i = 0; i < len; i++) {
            html += '<option  value="' + data.domains[i] + '">' + data.domains[i] + '</option>';
        }

        $('#gen').remove();

        domainsSelect.append(html);

        domainsHolder.removeClass('loading');

        // google.setOnLoadCallback(drawChart);
    });

   // $(document).foundation('reflow');

});


$(domainsSelect).on('change', function () {

    var html = '',
        theval = $(this).val();

    googleResults.html(html);

    // if (theval != 'default') {

    graphHolder.show().addClass('loading');
    // console.log(theval);


    $.getJSON('{{ action("GoogleController@DoGraph") }}?domain=' + theval, function (data) {


        /*    array as follows:
         0   'Content Keywords'
         1   'External Links'
         2   'Internal Links'
         3   'Top Pages'
         4   'Top Queries' */


        var keywords = '',
            externalLinks = '',
            internalLinks = '',
            topPages = '',
            topQueries = '',

            keywordsObject = data[0],
            externalLinksObject = data[1],
            internalLinksObject = data[2],
            topPagesObject = data[3],
            topQueriesObject = data[4];


        graphHolder.removeClass('loading');

        //console.log(data[0]);


        $.each(keywordsObject, function (val, key) {

            /*
             0 = "Keyword"
             1 = "Occurrences"
             2 = "Variants encountered"
             3 = "Top URLs"
             */

            keywords += '<h2>' + 'Content Keyword' + '</h2>';

            $.each(key, function (iinerVal, iinerKey) {

                keywords += '<p><strong>' + 'Keyword' + ':</strong> ' + iinerKey[0] + '</p>';
                keywords += '<p><strong>' + 'Occurrences' + ':</strong> ' + iinerKey[1] + '</p>';
                keywords += '<p><strong>' + 'Variants encountered' + ':</strong> ' + iinerKey[2] + '</p>';
                keywords += '<p><strong>' + 'Top URLs' + ':</strong> ' + iinerKey[3] + '</p>';

                keywords += '<hr/>';
            });

            keywords += '<hr/>';


        });

        $.each(externalLinksObject, function (val, key) {

            /*
             0 = "Domains"
             1 = "Links"
             2 = "Variants encountered"
             */

            externalLinks += '<h2>' + 'External Links' + '</h2>';

            $.each(key, function (iinerVal, iinerKey) {

                externalLinks += '<p><strong>' + 'Domains' + ':</strong> ' + iinerKey[0] + '</p>';
                externalLinks += '<p><strong>' + 'Links' + ':</strong> ' + iinerKey[1] + '</p>';
                externalLinks += '<p><strong>' + 'Linked pages' + ':</strong> ' + iinerKey[2] + '</p>';

                externalLinks += '<hr/>';
            });

            externalLinks += '<hr/>';


        });

        $.each(internalLinksObject, function (val, key) {

            /*
             0 = "Target pages"
             1 = "Links"
             */

            internalLinks += '<h2>' + 'Internal Links' + '</h2>';

            $.each(key, function (iinerVal, iinerKey) {

                internalLinks += '<p><strong>' + 'Target pages' + ':</strong> ' + iinerKey[0] + '</p>';
                internalLinks += '<p><strong>' + 'Links' + ':</strong> ' + iinerKey[1] + '</p>';

                internalLinks += '<hr/>';
            });

            internalLinks += '<hr/>';
        });

        $.each(topPagesObject, function (val, key) {

            /*
             0 = "Page"
             1 = "Impressions"
             2 = "Change"
             3 = "Clicks"
             4 = "Change"
             5 = "CTR"
             6 = "Change"
             7 = "Avg. position"
             8 = "Change"
             */


            topPages += '<h2>' + 'Top Pages' + '</h2>';

            $.each(key, function (iinerVal, iinerKey) {

                topPages += '<p><strong>' + 'Page' + ':</strong> ' + iinerKey[0] + '</p>';
                topPages += '<p><strong>' + 'Impressions' + ':</strong> ' + iinerKey[1] + '</p>';
                topPages += '<p><strong>' + 'Change' + ':</strong> ' + iinerKey[2] + '</p>';
                topPages += '<p><strong>' + 'Clicks' + ':</strong> ' + iinerKey[3] + '</p>';
                topPages += '<p><strong>' + 'Change' + ':</strong> ' + iinerKey[4] + '</p>';
                topPages += '<p><strong>' + 'CTR' + ':</strong> ' + iinerKey[5] + '</p>';
                topPages += '<p><strong>' + 'Avg. position' + ':</strong> ' + iinerKey[6] + '</p>';
                topPages += '<p><strong>' + 'Change' + ':</strong> ' + iinerKey[7] + '</p>';

                topPages += '<hr/>';
            });

            topPages += '<hr/>';


        });

        $.each(topQueriesObject, function (val, key) {


            /*
             0 = "Query"
             1 = "Impressions"
             2 = "Change"
             3 = "Clicks"
             4 = "Change"
             5 = "CTR"
             6 = "Change"
             7 = "Avg. position"
             8 = "Change"
             */

            topQueries += '<hr/>';
            topQueries += '<h2>' + val + '</h2>' + key + '<br/>';
        });


        googleResults.append(
            '<dl class="accordion" data-accordion>'
                + '<dd><a href="#panel1">Keywords</a><div id="panel1" class="content">' + keywords + '</div></dd>'
                + '<dd><a href="#panel2">External Links</a><div id="panel2" class="content">' + externalLinks + '</div></dd>'
                + '<dd><a href="#panel3">Internal Links</a><div id="panel3" class="content">' + internalLinks + '</div></dd>'
                + '<dd><a href="#panel4">Top Pages</a><div id="panel4" class="content">' + topPages + '</div></dd>'
                + '<dd><a href="#panel5">Top Queries</a><div id="panel5" class="content">' + topQueries + '</div></dd>'
                + '</dl>'
        )


//

    });
    // }

    //$(document).foundation('reflow');
});


</script>


@stop