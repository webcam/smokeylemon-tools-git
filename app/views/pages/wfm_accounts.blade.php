v@extends('layouts.master')

@section('title', 'Workflow Max Accounts')

@section('content')


<!-- Feed Entry -->
<div class="row" >
    <div class="two columns mobile-one"><img src="/img/wfm.png" /></div>
    <div class="ten columns">
        <p><strong>Workflow Max Account Details</strong></p>
        <span id="load_server" class=" radius"></span>

        <form>

            {{ $dropdown }}

        </form>

        <div id="wfmData"></div>



    </div>
</div>
<!-- End Feed Entry -->

<hr />
@stop


@section('inlineScripts')





<script type="text/javascript">
    // Function to restart a specific service when clicked via button..
    $(document).ready(function ($) {

        var pageToLoad = '{{ URL::route('getwfmaccounts') }}/';

        var $WFMId = $('.whmAccount'),
            $WFMContainer = $('#wfmData');

        $($WFMId).change(function () {
            var $selectedID = $(this).val();
            url = pageToLoad;
            $($WFMContainer).addClass('loading');
            $($WFMContainer).empty();
            //alert($selectedID);

           $.ajax({
                type:'GET', // type of request either Get or Post
               url: url + $selectedID,
                //data:{ clientid:$selectedID}, // data to be post
                success:function (data) {
                    //function to be called on successful reply from server
                    // this method will always return a 'success', vaildate via php for a return...
                    //alert(data);
                    $($WFMContainer).removeClass('loading');
                    $($WFMContainer).html(data);
                }
            });

        });

    });

</script>

@stop