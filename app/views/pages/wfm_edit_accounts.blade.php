v@extends('layouts.master')

@section('title', 'Edit Workflow Max Accounts')

@section('content')



    <!-- BOF Entry -->
    <div class="col-xs-10">
        <div class="x_panel">
            <div class="x_title">
                <h2>Workflow Max Account Details</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/wfm.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">


                        <h3>Editing Notes</h3>
                        <p>Please either use a pipe (|) or new line for the Server Username & Pass Field - ideally the format to be:<br/> username | password</p>
                        <p>Please Note: The utility, will update the following: <Strong>Workflow Max</Strong>, <Strong>Filezilla</Strong> and <Strong>CPanel Account</Strong> (If applicable and not the website database)<br/>
                        <p>For this to happen the following criteria must be met:

                        <ul>
                            <li>Which Server is it On to be entered</li>
                            <li>Server Username & Pass to be entered</li>
                        </ul>

                        <p>To be developed: Auto config file update with all database password updates.</p>
                        <span id="load_server" class=" radius"></span>

                        <form>
                            <div class="form-group">
                            {{ $dropdown }}
                            </div>
                        </form>

                        <div id="wfmData"></div>
                        <div id="wfmResult"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->
@stop


@section('inlineScripts')


<script type="text/javascript">

    function submitForm(data, urlToPost) {
        var submitButton = $('#submitButton'),
            result;

        $(submitButton).attr('disabled','disabled').text('Please Wait...');

        $.ajax({
            type: 'POST', // type of request either Get or Post
            url: urlToPost,
            data: data })  // data to be post
            .done(function (msg) {
                result = msg;
                $('#wfmResult').html(result);
                $(submitButton).text('Saved!');
                //$(submitButton).removeAttr("disabled").text('Save');
            });

    }

    function createInput(name, val, id, type) {
        var input;
        if (type == 'text') {
            input = '<div class="form-group">' +
                '<label>' + name  + '</label>'+
                '<input class="form-control" type="text" name="' + id + '" value="' + val + '"/>' +
                '</div>';
        }
        if (type == 'textarea') {
            input = '<div class="form-group">' +
                '<div class="large-12 columns">' +
                '<label>' + name  + '</label>'+
                '<textarea class="form-control" name="' + id + '">' + val + '</textarea>' +
                '</div>';

        }


        // input = '<input name="' + name + '" value="' + val + '"/>';
        return input;
    }


    //function toTrim(val, id) {
        //if (id==6681){

        //}


        // input = '<input name="' + name + '" value="' + val + '"/>';
       // return input;
    ///}

    // Function to restart a specific service when clicked via button..
    $(document).ready(function ($) {

        var pageToLoad = "{{ URL::route('getwfmaccounts') }}",
            postUrl = "{{ URL::route('postWFMAccount') }}";


        var $WFMId = $('.whmAccount'),
            $WFMContainer = $('#wfmData');


        $($WFMId).change(function () {
            var url = pageToLoad +'/'+ $(this).val() ,
                submitButton = $('<button type="button" class="btn btn-primary" id="submitButton">Save Changes</button>'),
                form = $('<form id="changes" class="form-horizontal form-label-left"></form>');

            $($WFMContainer).addClass('loading');
            $($WFMContainer).empty();
            //alert($selectedID);

            $.ajax({
                type: 'GET', // type of request either Get or Post
                url: url,
                //data:{ clientid:$selectedID}, // data to be post
                success: function (data) {

                    var htmlName, htmlVal, Result, input, html, htmlID;


                    Result = '';
                    html = '<p><strong>' + data.Name[0] + '</strong><br/>';
                    html += data.Address + '</p>';
                    //console.log(data);


                    $.each(data, function (index) {
                        if (data[index].Name) {
                            htmlName = data[index].Name;
                            htmlVal = data[index].Text;
                            htmlID = data[index].ID;
                            //if (data[index]['@attributes']) {
                            if (data[index]['@attributes'] && data[index]['@attributes'].InputType == 'Textarea') {
                                Result += createInput(htmlName, htmlVal, htmlID, 'textarea');
                            } else {
                                Result += createInput(htmlName, htmlVal, htmlID, 'text');
                            }
                            //console.log(Result);
                        }
                    });

                    form.append(html + Result);
                    form.append(submitButton);
                    $($WFMContainer).removeClass('loading');
                    $($WFMContainer).append(form);

                    $(submitButton).on('click', function (event) {
                        // do validation of username | password, this is important.. we know the custom ID is got the input name of 6681
                        //TODO, force newlines removed in PHP side...
                         var theData = {form: $(form).serializeArray(), WFMId: $WFMId.val()};
                        console.log(theData);
                        submitForm(theData, postUrl);
                        event.preventDefault();
                    });


                }
            });
        });






    });

</script>

@stop