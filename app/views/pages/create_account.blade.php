@extends('layouts.master')

@section('title', 'Create An Account')

@section('content')


    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Create An Account on a Server</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/server.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">


        <form name="createAccountForm" id="createAccountForm" class="form-horizontal form-label-left" data-toggle="validator" role="form">
                <div class="form-group">
                    <label>Server Name</label>
                    {{ Form::select('serverName', array_merge(array('default' => 'Please Select'),$servers),'default',
                    array('id' => 'serverName','class'=>'form-control','required')) }}
                </div>
                    <div id="downloadResults"></div>
                    {{--<div class="account-field">
                        <legend>Fill Out The Details</legend>
                        {{ $wfmaccounts }}
                        <small class="error">Not Selected</small>
                    </div>--}}
                    <div class="email-field form-group">
                        <label>Domain Name</label>
                        {{ Form::text('domain','',array('id' => 'domain','required','data-domain'=>'true','class'=>'form-control')) }}
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="name-field form-group">
                        <label>Name</label>
                        {{ Form::text('username','',array('id' => 'username','required','data-username'=>'true','class'=>'form-control')) }}
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="email-field form-group">
                        <label>Contact Email</label>
                        {{ Form::text('email','',array('id' => 'email','required','class'=>'form-control','data-error' => 'Only email addresses only.')) }}
                        <div class="help-block with-errors"></div>
                    </div>
                    <button type="submit" id="createAccount" class="btn btn-button btn-primary" data-reveal-id="confirmAction">Create Account
                    </button>
                    </form>
                    <div id="important"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->


@stop

@section('inlineScripts')
    <script type="text/javascript">
        // Function to restart a specific service when clicked via button. Prompts user with Foundations Reveal Modal.  Also return current service status's
        $(document).ready(function ($) {

            $('#createAccountForm').validator({
                custom: {
                    username: function(el) {
                        var valid,
                                thisVal = $(el).val();
                        if (thisVal.length >= 5 && thisVal.length <= 8) {
                            valid = true;
                        } else {
                            valid = false;
                        }
                        return valid;
                    },
                    domain: function(el) {
                        var valid, content = $(el).val();
                        valid = false;
                        // strip off "http://" and/or "www."
                        content = content.replace("http://","");
                        content = content.replace("www.","");

                        var reg =/^(?!:\/\/)([a-zA-Z0-9]+\.)?[a-zA-Z0-9][a-zA-Z0-9-]+\.[a-zA-Z]{2,6}?$/i;
                        if(reg.test(content)){
                            valid = true;
                            return valid;
                        }
                    }
                },
                errors: {
                    username: "Please check length of username is between 5 and 8 characters",
                    domain: "Wrong, please check domain"
                }
            });




            var serverName = $('#serverName'),
                    urlToPost = "{{ URL::route('create-an-account') }}",
                    domain = $('#domain'),
                    username = $('#username'),
                    email = $('#email'),
                    important = $('#important'),
                    submitButton = $('#createAccount');

            var serverNameVal = $(serverName).val(),
                    domainVal = $(domain).val(),
                    usernameVal = $(username).val(),
                    emailVal = $(email).val();

            // set submit dsabled until valid...

            var checkForSeverSelected = function () {
                if (serverNameVal != 'default') {
                    $(submitButton).removeAttr('disabled');
                } else {
                    $(submitButton).attr('disabled', 'disabled');
                }
            };

            //checkForSeverSelected();

            $('input,select').change(function () {
                serverNameVal = $(serverName).val();
                domainVal = $(domain).val();
                usernameVal = $(username).val();
                emailVal = $(email).val();

               // checkForSeverSelected();


            });

            $(domain).keyup(function () {
                var thisVal = $(this).val(),
                        str = thisVal.replace(/[^a-zA-Z 0-9]+/g, '');
                if ( usernameVal.length > 8) {
                    $(username).val(str.substr(0, 8));
                } else {
                    $(username).val(str.substr(0, 8));
                }
            });

            $(submitButton).click(function (e) {
                // $('#createAccount').on('valid.fndtn.abide', function() {
                usernameVal = $(username).val();
                emailVal = $(email).val();
                console.log(usernameVal.length);
                if (usernameVal.length >= 5 && usernameVal.length <= 8) {
                    $('#createAccountForm').validator().on('submit', function (e) {
                        if (e.isDefaultPrevented()) {
                            // handle the invalid form...
                            return false;
                        } else {
                            // everything looks good!
                            console.log('the return is: ' + e.type);
                            if (e.type === "submit") {
                                console.log('valid!, sending data...');
                                important.html('<p>Processing.. please wait..</p>');
                                $.post(urlToPost, {
                                    serverName: serverNameVal,
                                    domain: domainVal,
                                    username: usernameVal,
                                    email: emailVal
                                })
                                .done(function (data) {
                                     important.html(data);
                                });
                            }
                        }
                    });
                } else {
                    $('.name-field').find('.error').html('Please check length of username is between 5 and 8 characters').show();
                }
            });
        });

    </script>

@stop