@extends('layouts.master')

@section('title', 'Live Server Tools')

@section('content')

    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Select the Server</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/server.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">

                        <form class="form-horizontal form-label-left">
                            <div class="form-group">
                                <label>Server Name</label>
                                {{ Form::select('serverName', array_merge(array('default' => 'Please Select'),$servers),'default',array('id' => 'serverName', 'class'=>'form-control')) }}
                            </div>
                                <div id="downloadResults"></div>

                       </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->



<div id="resultsContainer" style="display: none">

    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Server Load</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/load.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">
                        <p><strong>Realtime <span id="serverNameReplace"></span> Server Load</strong></p>

                        <div class="panel radius">
                            <pre id="load_server"></pre>
                        </div>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->

<div class="clearfix"></div>
    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>MYSQL Service</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/database.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">
                        <p><strong>MYSQL Service</strong></p>

                        <p>Current Status: <span data-service="mysql" class="important servicerequest">Loading..</span></p>

                        <button type="button" class="btn btn-primary restartPrompt" data-toggle="modal" data-target=".bs-example-modal-sm" data-service="mysql">Click to
                            restart MYSQL</button>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->

    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Apache Service</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/apache.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">
                        <p>Current Status: <span data-service="httpd" class="important servicerequest">Loading..</span></p>

                        <button type="button" class="btn btn-primary restartPrompt" data-toggle="modal" data-target=".bs-example-modal-sm" data-service="httpd">Click to
                            restart Apache</button>

                       <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->

    <div class="clearfix"></div>
    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Clear Tmp File Inodes</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/tmp.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">
                        <p>Clear Tmp File Inodes that are older than 24 hours</p>
                        <button type="button" class="btn btn-primary restartPrompt" data-toggle="modal" data-target=".bs-example-modal-sm" data-service="inodes">Click to
                            clear Innodes</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->


</div>


    <div class="modal fade modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="confirmAction">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p id="innerMessage"></p>
                    <p> Once clicked 'Yes' please wait 30 seconds..</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                    <button type="button" class="resartProcedure btn btn-danger">Yes</button>
                </div>
            </div>
        </div>
    </div>



@stop


@section('inlineScripts')
<script type="text/javascript">
    // Function to restart a specific service when clicked via button. Prompts user with Foundations Reveal Modal.  Also return current service status's

    $(document).ready(function ($) {

        // Define Global Variables...
        // 1) restartPrompt button (launches modal)
        // 2) resartProcedure button attached to 'Yes' button
        // 3) popUp refers to modal box
        // 4) servicerequest refers to the span element that shows the service status
        // 5) $selectedService refers to the data attribute of service we are requesting

        var $restartPrompt = $('.restartPrompt'),
            serverName = $('#serverName'),
            $button = $('.resartProcedure'),
            $popUp = $('#confirmAction'),
            $service = $('.servicerequest'),
            $selectedService = $service.data('service'),
            $serviceUrl = "{{ URL::route('service') }}",
            serverNameSelected = '',
            serverLoadUrl = '',
            resultsContainer = $('#resultsContainer');


        $(serverName).change(function () {

            serverNameSelected = $(this).val();

            if (serverNameSelected != 'default') {

                resultsContainer.show();
                // show only if val does not equal 'default'

                serverLoadUrl = "{{ URL::route('pageload') }}" + '?serverName=' + serverNameSelected;
                //serverName = $( this + ' option:selected').val();
                console.log(serverNameSelected);

                $('#serverNameReplace').html(serverNameSelected);


                // BOF Monitoring services..

                $($service).load($serviceUrl + '?serverName=' + serverNameSelected + '&service=' + $selectedService);

                var auto_refresh = setInterval(
                    function () {
                        $($service).load($serviceUrl + '?serverName=' + serverNameSelected + '&service=' + $selectedService).fadeIn("slow");

                        console.log($serviceUrl + '?serverName=' + serverNameSelected + '&service=' + $selectedService);
                    }, 500000); // refresh every 500000 milliseconds

                // EOF Monitoring services..

                // BOF Restart services..

                $($restartPrompt).click(function () {
                    var $selectedService = $(this).data('service');
                    var $message;
                    $button.data('service', $selectedService);

                    if ($selectedService == 'inodes') {
                        $message = 'Are you sure, you wish to delete these files? (Older than 24 Hours)';
                    } else {
                        $message = 'Are you sure, you wish to restart the ' + $selectedService + ' service?';
                    }

                    $('#innerMessage').html($message);

                });

                $($button).click(function () {
                    var $selectedService = $(this).data('service'),
                        confirmBox = $('#confirmAction');
                    url = '{{ URL::route("RestartServices") }}';
                    console.log(serverNameSelected);
                    $(confirmBox).html('<p>Please wait...</p>');
                    $.ajax({
                        type: 'POST', // type of request either Get or Post
                        url: url, // Url of the page where to post data and receive response
                        data: { service: $selectedService, server: serverNameSelected}, // data to be post
                        success: function (data) {
                            //function to be called on successful reply from server
                            // this method will always return a 'success', validate via php for a return...
                            $popUp.trigger('reveal:close');
                            $(confirmBox).html('<p>' + data + '</p><a class="close-reveal-modal">&#215;</a>');
                            //console.log(data);
                            //window.location.reload();
                            //alert('done');
                        }
                    });
                });
                // EOF Restart services..


                // Stuff to do as soon as the DOM is ready;
                $('#load_server').load(serverLoadUrl);
                var auto_refresh2 = setInterval(
                    function () {
                        $('#load_server').load(serverLoadUrl);
                    }, 5000); // refresh every 5000 milliseconds

            } else {
                resultsContainer.hide();
            }

        });

    });

</script>
@stop