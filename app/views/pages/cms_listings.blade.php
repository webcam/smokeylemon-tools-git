@extends('layouts.master')

@section('title', 'CMS Listings')

@section('content')


<!-- Feed Entry -->
<!-- Feed Entry -->
<div class="row">
    <div class="large-2 columns mobile-one">{{ HTML::image('assets/smokeytoolsv2/img/server.png') }}</div>
    <div class="large-10 columns">
        <fieldset>
            <legend>Select the Server</legend>
            <label>Server Name</label>
            {{ Form::select('serverName', array_merge(array('default' => 'Please Select'),$servers),'default', array('id' => 'serverName')) }}
            <div id="downloadResults"></div>
        </fieldset>
    </div>
</div>
<!-- End Feed Entry -->
<hr/>


<div class="row active">

    <div class="large-12 columns" id="versions">

        <!-- ajax load here..-->

    </div>
</div>
@stop

@section('inlineScripts')

<script>

    // some global stuff we want..
    var domainsHolder = $('#domainsHolder'),
        domainsSelect = $('select#domain'),
        serverSelect = $('select#serverName'),
        loadingText = "<p>Please be patient while this is loading...</p>",
        versions    = $('#versions'),
        tableHeader = '<table id="theTable" class="responsive tablesorter"><thead> <tr> <th>Domain Name</th> <th>CMS Version</th> <th>Action Required</th></tr></thead><tbody>',
        tableFooter = '</tbody></table>';

   // initOptions('enabled');

   // $.ajaxSetup({ cache: false });


    $('#serverName').on('change', function () {

        $(loadingText).appendTo(versions);

        var html = '',
            theval = $(this).val();

        versions.html(html);

        if (theval != 'default') {

            versions.show().addClass('loading');
           // console.log(theval);


            $.getJSON('{{ URL::action("CmslistingsController@cmsList") }}?server=' + theval, function (data) {

                //console.log(data, '{{ URL::action("CmslistingsController@cmsList") }}?server=' + theval);

                $(versions).removeClass("loading").empty(loadingText);


                var html = '';


                var lmv = '<div class="row"><div class="six columns"><div class="panel"><p><strong>Latest Major Version:</strong> ' + data.LMV + '</p></div></div></div>';

                html += tableHeader;

               //

                $.each(data.Res, function (index, value) {
                        //console.log(value.domain);
                        //do something with article...
                        html += '<tr><td>' + value.domain + '</td><td>' +  value.version + '</td><td>' +  value.action + '</td></tr>';

                    }
                );

                html += tableFooter;

                $(versions).append(lmv + html);

                $("#theTable").tablesorter();

            });

        } else {

            // its on 'select one', so lets hide things..

            domainsHolder.hide();

        }
    });




</script>



{{ HTML::script('assets/smokeytoolsv2/js/jquery.tablesorter.min.js') }}


@stop