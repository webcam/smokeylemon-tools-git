@extends('layouts.master')

@section('title', 'Lime Admin Tools')

@section('content')

    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Dowload to Local</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/Download.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">
                        <form name="downloadToLocal" id="downloadToLocal" class="form-horizontal form-label-left">
                            <div class="form-group">
                                <label>Server Name</label>
                                {{ Form::select('serverName', array_merge(array('default' => 'Please Select'),$servers),'default', array('class'=>'form-control')) }}
                                <div id="downloadResults"></div>

                                <label>Live Domain Name</label>
                                <select name="domain" required class='form-control'></select>

                                <label>Live Account Password</label>
                                <input name="accountPassword" required class='form-control'>

                                <label>Local Server Directory</label>
                                {{ Form::select('localSite', array_merge(array('default' => 'Please Select'),$localSites),'default',
                                array('id' => 'localSite','class'=>'form-control'),'required') }}
                                <div id="warning"></div>
                                <button id="downloadClient" class="download btn btn-primary archiveSite"><i
                                            class="fi-arrow-down"></i> Download
                                </button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->
    <!-- BOF Entry -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Upload to Live</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3">{{ HTML::image('assets/smokeytoolsv2/img/Upload.png', '', array('class' => 'img-circle img-responsive')) }}</div>
                    <div class="col-sm-9">
                        <form name="uploadToLive" id="uploadToLive" class="form-horizontal form-label-left">
                            <div class="form-group">
                                <label>Server Name</label>
                                {{ Form::select('serverName', array_merge(array('default' => 'Please Select'),$servers),'default', array('class'=>'form-control')) }}
                                <div id="downloadResults"></div>

                                <label>Live Domain Name</label>
                                <select name="domain" required class='form-control'></select>

                                <label>Live Account Password</label>
                                <input name="accountPassword" required class='form-control'>

                                <label>Local Server Directory</label>
                                {{ Form::select('localSite', array_merge(array('default' => 'Please Select'),$localSites),'default',
                                array('id' => 'localSite','class'=>'form-control'),'required') }}
                                <div id="warning"></div>
                                <button id="uploadClient" class="upload btn btn-primary archiveSite"><i
                                            class="fi-arrow-up"></i> Upload
                                </button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EOF Entry -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="lightboxRemoveClient">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Remove Client Folder</h4>
                </div>
                <div class="modal-body">

                    <form name="removeClientConfirm">
                        {{ Form::label('clientToRemoveConfirm', 'Please type the client name to remove!') }}
                        {{ Form::text('clientToRemoveConfirm') }}
                        {{ Form::label('clientToRemovePass', 'Please enter the pass phrase') }}
                        {{ Form::password('clientToRemovePass') }}
                        <button class="btn btn-primary removeClient">Remove</button>
                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{--  <div id="lightboxRemoveClient" class="w-lightbox-backdrop" tabindex="0" style="transition: opacity 300ms;">
          <div class="w-lightbox-container">
              <div class="w-lightbox-content">
                  <div class="w-lightbox-view" style="opacity: 1;">
                      <div class="w-lightbox-frame">
                          <figure class="w-lightbox-figure">
                              <form name="removeClientConfirm">
                                  <fieldset>
                                      <legend>Remove Client Folder</legend>
                                      {{ Form::label('clientToRemoveConfirm', 'Please type the client name to remove!') }}
                                      {{ Form::text('clientToRemoveConfirm') }}
                                      {{ Form::label('clientToRemovePass', 'Please enter the pass phrase') }}
                                      {{ Form::password('clientToRemovePass') }}
                                      <button class="btn btn-primary removeClient">Remove</button>

                                  </fieldset>
                              </form>
                          </figure>
                      </div>
                  </div>
                  <div class="w-lightbox-spinner w-lightbox-hide"></div>
                  <div class="w-lightbox-control w-lightbox-left w-lightbox-inactive"></div>
                  <div class="w-lightbox-control w-lightbox-right w-lightbox-inactive"></div>
                  <div class="w-lightbox-control w-lightbox-close"></div>
              </div>
              <div class="w-lightbox-strip"></div>
          </div>
      </div>--}}

    <!-- End Feed Entry -->
    <div>{{ $myErrorLog }}</div>
@stop

@section('inlineScripts')


    <!-- BOF Download Site -->
    <script>

        // some global stuff we want..
        var domainsHolder = $('#domainsHolder'),
                domainsSelect = $('select[name=domain]'),
                accountPassword = $('input[name=accountPassword]'),
                serverSelect = $('select[name=serverName]'),
                localSelect = $('select[name=localSite]'),
                warning = $('#warning'),
                downloadClientButton = $('#downloadClient'),
                uploadClientButton = $('#uploadClient');

        initOptions('enabled');

        $.ajaxSetup({cache: false});

        $(serverSelect).on('change', function () {
            var html = '',
                    theval = $(this).val();
            domainsSelect.html(html);

            if (theval != 'default') {
                domainsHolder.show().addClass('loading');
                // console.log(theval);
                $.getJSON('{{ URL::action("WebsiteMappingsController@getServer") }}?serverName=' + theval, function (data) {
                    var len = data.length;
                    //console.log(len + data);
                    html += '<option value="selectone">Select One</option>';
                    for (var i = 0; i < len; i++) {
                        html += '<option data-wfmId="' + data[i].wfmId + '" value="' + data[i].user + '">' + data[i].domain + '</option>';
                    }
                    domainsSelect.append(html);
                    domainsHolder.removeClass('loading');
                });

            } else {

                // its on 'select one', so lets hide things..

                domainsHolder.hide();

            }
        });


        // Pre Confirmation Function

        function preConfimation(direction, url, parentElm) {
            var selectedDomain = $('#' + parentElm).find(domainsSelect).val(),
                    selectedLocalSite = $('#' + parentElm).find(localSelect).val(),
                    confirm = '<button data-parent=' + parentElm + ' data-url=' + url + ' id="confirm" class="btn btn-primary"><i class="fi-check"></i>  Confirm Action</button>',
                    cancel = '<button id="cancel" class="radius button red"><i class="fi-x"></i>  Cancel Action</button>';

            if (selectedDomain != 'selectone' && selectedLocalSite != 'default') {

                // disbale them all
                initOptions('disabled');

                downloadClientButton.hide();
                uploadClientButton.hide();


                if (direction == 'upload') {
                    var message = '<p><span class="alert label">WARNING!!! Please Check the following:</span><br/> You are going to <strong>' + direction + '</strong> the following website: <strong>' + selectedDomain + '</strong> from the local client folder <strong>' + selectedLocalSite + '</strong> </p>';
                } else {
                    var message = '<p><span class="alert label">WARNING!!! Please Check the following:</span><br/> You are going to <strong>' + direction + '</strong>  the following website: <strong>' + selectedDomain + '</strong>  to the local client folder <strong>' + selectedLocalSite + '</strong>. Any existing website will be placed in a backup zip file.</p>';
                }

                warning.html(message + '<br/>' + confirm + '&nbsp;&nbsp;' + cancel);


            }

        }


        function confimation(action) {

            if (action == 'confirm') {
                var parentHolderElm = $('#confirm').attr('data-parent');
                selectedDomain = $('#' + parentHolderElm).find(domainsSelect).val(),
                        accountPassword = $('#' + parentHolderElm).find(accountPassword).val(),
                        selectedLocalSite = $('#' + parentHolderElm).find(localSelect).val(),
                        selectedServer = $('#' + parentHolderElm).find(serverSelect).val(),
                        selectedDomainValue = $('#' + parentHolderElm).find('select[name=domain] option:selected').text(),
                        wfmId = $('#' + parentHolderElm).find('select[name=domain] option:selected').attr('data-wfmId'),
                        message = '<p class="warning"><strong>Please Wait.. this will take a while, go have a break or somethin...</strong></p>',
                        urlAction = $('#confirm').attr('data-url');

                console.log(selectedDomainValue);

                warning.html(message);

                $.post(urlAction, {
                    domain: selectedDomain,
                    localSite: selectedLocalSite,
                    server: selectedServer,
                    accountPassword: accountPassword,
                    wfmId: wfmId,
                    selectedDomainValue: selectedDomainValue
                })
                        .done(function (data) {
                            // will return success or error issue..
                            if (data.success) {

                                warning.html('<p><i class="fi-check"></i> ' + data.success + '</span></p>');

                            } else {

                                warning.html('<p><i class="fi-x"></i> ' + data.error + '</span></p>');
                            }
                        });


            } else {
                // do the cancel...

                $('#serverName').val('default');
                window.location.reload();
            }

        }

        // function for disabling/enabling selects,inputs blah blah
        function initOptions(attr) {

            var fields = $('select, input');

            if (attr == 'enabled') {

                fields.removeAttr("disabled");

            }

            if (attr == 'disabled') {

                fields.attr("disabled", "disabled");

            }

        }


        // now for the handlers..

        downloadClientButton.click(function (e) {
            e.preventDefault();
            var url = '{{ URL::action("WebsiteMappingsController@downloadToLocal") }}';
            var parent = 'downloadToLocal';
            preConfimation('download', url, parent);
        });

        uploadClientButton.click(function (e) {
            e.preventDefault();
            var url = '{{ URL::action("WebsiteMappingsController@uploadToLive") }}';
            var parent = 'uploadToLive';
            preConfimation('upload', url, parent);
        });


        $(warning).on('click', '#confirm', function (e) {
            e.preventDefault();
            confimation('confirm');
        });

        $(warning).on('click', '#cancel', function (e) {
            e.preventDefault();
            confimation('cancel');
            //re init everything..
            initOptions('enabled');
            $('#serverName').val('default');
        });


    </script>
    <!-- EOF Download Site -->


@stop