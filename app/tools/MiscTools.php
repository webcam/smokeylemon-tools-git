<?php

/**
 * Created by JetBrains PhpStorm.
 * User: cam
 * Date: 14/08/13
 * Time: 10:59 AM
 * To change this template use File | Settings | File Templates.
 */



class MiscTools
{

    public static function Zip($source, $destination)
    {
        exec("tar -zcvf $destination.tar.gz --directory='$source' .");

    }

    public static function rmdir_recursive($dir)
    {
        foreach (scandir($dir) as $file) {
            if ('.' === $file || '..' === $file) {
                continue;
            }
            if (is_dir("$dir/$file")) {
                rmdir_recursive("$dir/$file");
            } else {
                unlink("$dir/$file");
            }
        }
        rmdir($dir);
    }

    public static function download($url, $path, $host = false)
    {
        $client = new \GuzzleHttp\Client();
        $resource = fopen($path, 'w+');
        $stream = \GuzzleHttp\Stream\Stream::factory($resource);
        $request = $client->createRequest('GET', $url, ['save_to' => $stream]);
        if ($host) {
            $request->setHeader('Host', $host);
        }
        $request->setHeader('User-Agent', 'Mozilla');
        $client->send($request);
        if (filesize($path) > 0) {
            return true;
        }

    }


}