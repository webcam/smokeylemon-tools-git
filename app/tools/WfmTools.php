<?php

/**
 * Created by JetBrains PhpStorm.
 * User: cam
 * Date: 14/08/13
 * Time: 10:59 AM
 * To change this template use File | Settings | File Templates.
 */
class WfmTools
{

    static public $APIKey = '14C10292983D48CE86E1AA1FE0F8DDFE';
    static public $AccKey = 'E2CB7A472CC8405AA80D62FE91A69D79';


    public static function getWFMAccounts($label = 'Account Selection')
    {
        $CacheFile = storage_path() . '/cache/WFMClients.xml';

        $query = 'https://api.workflowmax.com/client.api/list?apiKey=' . self::$APIKey . '&accountKey=' . self::$AccKey;

        // generate the cache version if it doesn't exist or it's too old!
        $ageInSeconds = 1800; // twelve hours

        if (!file_exists($CacheFile) || filemtime($CacheFile) > time() + $ageInSeconds) {
            $contents = file_get_contents($query);
            file_put_contents($CacheFile, $contents);
        }

        $xml = simplexml_load_file($CacheFile);
        $dropDown = '<label for="clientId">'.$label.'</label>';
        $dropDown .= '<select name="clientId" class="whmAccount form-control">';
        $dropDown .= '<option>Select One</option>';
        foreach ($xml->Clients->Client as $Client) {
            $dropDown .= '<option value="' . $Client->ID . '">' . $Client->Name . '</option>';
        }
        $dropDown .= '</select>';

        return $dropDown;

    }


    public static function getWFMAccountsIdXMLtoJSON()
    {
        $CacheFile = storage_path() . '/cache/WFMClients.xml';

        $query = 'https://api.workflowmax.com/client.api/list?apiKey=' . self::$APIKey . '&accountKey=' . self::$AccKey;

        // generate the cache version if it doesn't exist or it's too old!
        $ageInSeconds = 1800; // twelve hours

        if (!file_exists($CacheFile) || filemtime($CacheFile) > time() + $ageInSeconds) {
            $contents = file_get_contents($query);
            file_put_contents($CacheFile, $contents);
        }

        $xml = simplexml_load_file($CacheFile);

        /*      $id = array();

              foreach ($xml->Clients->Client as $Client) {
                  $id[] = (string)$Client->ID;
              }

              $ids = json_encode($id);

              return $ids;*/
        $client = array();
        $tmp = array();

        foreach ($xml->Clients->Client as $Client) {
            $tmp = array((string)$Client->Website => array(
                'id' => (string)$Client->ID,
                'domain' => (string)$Client->Website
            ));


            array_push($client, $tmp);
        }

        // dd($client);

        $clientData = json_encode($client);

        return $clientData;


    }


    public static function getWFMAccount($accountID)
    {

        // This file just grabs the all the availbe job data and caches it

        $BaseDir = public_path() . '/WFMcache';

        if (!is_dir($BaseDir)){
            mkdir($BaseDir);
        }

        $clientid = $accountID;

        //Jobs List
        $downloadClientData = 'https://api.workflowmax.com/client.api/get/' . $clientid . '?apiKey=' . self::$APIKey . '&accountKey=' . self::$AccKey . '&detailed=true';
        $cacheClientName = $BaseDir . '/resultClient_' . $clientid . '.xml';

        $downloadClientCustomData = 'https://api.workflowmax.com/client.api/get/' . $clientid . '/customfield?apiKey=' . self::$APIKey . '&accountKey=' . self::$AccKey;
        $cacheClientCustom = $BaseDir . '/resultClient_Custom' . $clientid . '.xml';


        // generate the cache version if it doesn't exist or it's too old!
        $ageInSeconds = 1800; // twelve hours


        if (!file_exists($cacheClientName) || filemtime($cacheClientName) > time() + $ageInSeconds) {
            $contents = file_get_contents($downloadClientData);
            file_put_contents($cacheClientName, $contents);
        }

        if (!file_exists($cacheClientCustom) || filemtime($cacheClientCustom) > time() + $ageInSeconds) {
            $contents = file_get_contents($downloadClientCustomData);
            file_put_contents($cacheClientCustom, $contents);
        }


        $xml = simplexml_load_file($cacheClientName);


        $name = $xml->Client->Name;
        $address = $xml->Client->Address . ' ' . $xml->Client->City;

        return array($name, $address);

        // return Response::json(array('name' => $name, 'address' => $address));


        /*  echo '<h4>'.$name.'</h4><p>'. $address .'</p>';

          // var_dump($xmlCustom);
          //$dropDown .=  '</select>';


          // $customNode = $xmlCustom->CustomFields->CustomField;

          $xmlCustom = simplexml_load_file($cacheClientCustom);
          echo '<p>';
          foreach ($xmlCustom->CustomFields->CustomField as $node) {
              echo '<strong>' . $node->Name.'</strong>: '.$node->Text.'<br/>';
              //var_dump($node);
          }
          echo '</p>';*/


        //return $dropDown;

        //return $cacheClientName;

    }


}



