<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cam
 * Date: 14/08/13
 * Time: 10:59 AM
 * To change this template use File | Settings | File Templates.
 */


class ServerTools {



static public $whmusername = "root";
static public $whmhash = "de5867cbfb7cf48b4499d976f1a8b79c
2eeb26edc20d95e9a7b9dda00d35693d
873471da9aa6749fa43d0e0e3bb0f787
572c51bd5d26704679976851d169c1e0
b699fd56f3a11422379dc92f47c146f1
3a4a03a3d208427bcb7b887ae97a914a
1c7b794ff182c33f7e1ab3b1c113d065
1a207d351bc0acc1b5a4bc5f8160dc12
e6216ec569e25d0842cb79a41bacadd9
82025eb07d844417a9ea459078e17ac4
10c08363a8d906d4ae22dd3dc1d99233
9b651c8897efa7cb9c7ecb67662a6061
c8d4d354e28c0266923187d07ad1846b
6521e5c94b4a19bc0d2008f8b3022ea2
a2984d7cd045f8e4afe1d9a8b475a976
5b17619c5887f77609f4c07d626615a7
1fee7a99426e7fb37a9ae9b99659eefe
59d10eb28cd420b52842155150f514c2
dca9516b50908fc9b3ccd3d0bf23f804
81d16adce0f29a4ae7227f270d0974fd
d1f0cb0c430728e7c4edc8281d1ba89e
f9477c845491ad265b4d4a110b8ca004
82c435d36be2bf5c5e27016cb94e97b5
3468eaa8cf4f755de0c8c0ba0de07a70
2c307063e02e3480802ee85ec7cbfb70
4cf68b24d51c602410f7b85956804527
b54084b6a718b7a911f041113530f188
e5627b53abca2194906a339386469857
8d64200384925bd895c7e2995e9e546f";
# some hash value



    public static function getServerAPIRequest($queryToSend, $file = false)
    {
        $query = "https://tart.smokeylemon.com:2087/json-api/$queryToSend";

        $curl = curl_init();
        # Create Curl Object
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        # Allow certs that do not match the domain
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        # Allow self-signed certs
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        # Return contents of transfer on curl_exec
        $header[0] = "Authorization: WHM ".self::$whmusername.":" . preg_replace("'(\r|\n)'", "", self::$whmhash);
        # Remove newlines from the hash
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        # Set curl header
        curl_setopt($curl, CURLOPT_URL, $query);
        # Set your URL

        // if $file...
        if ($file){
            $fp = fopen($file, 'w');
           /* fwrite($fp, '<?xml version="1.0" encoding="ISO-8859-1"?>');*/
            fwrite($fp, '');
            curl_setopt($curl, CURLOPT_FILE, $fp);
        }

        $result = curl_exec($curl);
        # Execute Query, assign to $result
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        if ($file){
        fclose($fp);
        }

        return $result;
    }



    public static function getMyDomains() {

        $cachedDays = 4;
        $myCachedFile = public_path() . '/cache.json';

        if ( (!file_exists($myCachedFile)) or ((time() - filemtime($myCachedFile)) > ($cachedDays * 86400)) ) {

        // If older than 4 days, then lets get new info from cpanel...


        //dd(ServerTools::getServerAPIRequest('listaccts', $myXMLCachedFile));
            ServerTools::getServerAPIRequest('listaccts', $myCachedFile);

       }


       /* $xmlDoc = new DOMDocument();
        $xmlDoc->load($myCachedFile);*/


        $doc = file_get_contents($myCachedFile);
        $json_a = json_decode($doc,true);


        $acct = $json_a['acct'];


        $DomainResults = array();

        foreach ($acct as $value) {
            $tmp = $value["domain"];

            //$node = $tmp->item(0)->nodeValue;
            array_push($DomainResults, $tmp);
        }

      //  dd($DomainResults);


       /* $acct = $xmlDoc->getElementsByTagName("acct");
        // for each note tag, parse the document and get values for
        // tasks and details tag.
        $DomainResults = array();
        foreach ($acct as $value) {
            $tmp = $value->getElementsByTagName("domain");

            $node = $tmp->item(0)->nodeValue;
            array_push($DomainResults, $node);
        }*/









        return $DomainResults;
    }



    public static function getData($url, $vcheck = false) {

        // create a new curl resource
        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_VERBOSE, true);
        //$info = curl_getinfo($ch);
        // grab URL and pass it to the browser
        $content = curl_exec($ch);
        // I need the HTTP codes to see if 301 302 0r 404
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($http_code == '404' || $http_code == '401' || $http_code == '403') {
            $results['status'] = false;
            $results['message'] = '<b>Check not found</b>';
            return $results;
        }
        if ($vcheck === true) {
            if (strpos($content, 'VERSION:') === false) {
                $results['status'] = false;
                $results['message'] = '<b>Check not found</b>';
                return $results;
            }
        }

        // close curl resource, and free up system resources
        curl_close($ch);
        if ($content !== false) {

            $results['status'] = true;
            $results['message'] = $content;
            return $results;
        } else {
            $results['status'] = false;
            $results['message'] = '<b>Failed to get data</b>';
            return $results;
        }
    }



































    public function sendMessage()
    {
            $apikey = 'b4df8f01970e0b881c04be1439cc3362';
            $to = '64274180919';
            $msg = 'Warning server high load. This is just a message that Tart (IP: 210.48.67.67) is currently under high load. You may access http://tools.clients.smokeylemon.com to restart any service';

            $url = 'http://app.nz.textahq.com/api/sendSMS.php';

            $data = 'api_key=' . $apikey . '&to=' . $to . '&msg=' . $msg;

    //$dataToSend = urlencode($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $content = curl_exec($ch);
            if ($content == false) {
                error_log("curl_exec threw error \"" . curl_error($ch) . "\" for $query");
            }

            curl_close($ch);
        }


}