<?php

use Smokeylemon\WFM\WFMAccount;

class WfmController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    protected $layout = 'layouts.master';

    /**
     * Show the user profile.
     */
    public function index()
    {

        $dropdown = WfmTools::getWFMAccounts();
        $this->layout->content =  View::make('pages.wfm_accounts', compact('dropdown'));

    }

    function getWFMAccount($accountID)
    {

        $getAccount = new WFMAccount($accountID);
        //$getAccount->getWFMAccounts();
        $accountData = $getAccount->getWFMAccount();
        $customData = $getAccount->getWFMAccount(true);
        //WfmTools::getWFMAccount($accountID);
        $data = $getAccount->readXMLData($accountData, $customData);
       // $data .= $getAccount->readXMLData($accountData);

        return $data;
        //return $accountID;
        // return $getAccount;

        /*


// This file just grabs the all the availbe job data and caches it

        $BaseDir = public_path() . '/WFMcache';

        $clientid = $accountID;

//Jobs List
        $downloadClientData = 'https://api.workflowmax.com/client.api/get/' . $clientid . '?apiKey=' . WfmTools::$APIKey . '&accountKey=' . WfmTools::$AccKey . '&detailed=true';
        $cacheClientName = $BaseDir . '/resultClient_' . $clientid . '.xml';

        $downloadClientCustomData = 'https://api.workflowmax.com/client.api/get/' . $clientid . '/customfield?apiKey=' . WfmTools::$APIKey . '&accountKey=' . WfmTools::$AccKey;
        $cacheClientCustom = $BaseDir . '/resultClient_Custom' . $clientid . '.xml';


// generate the cache version if it doesn't exist or it's too old!
        $ageInSeconds = 1800; // twelve hours


        if (!file_exists($cacheClientName) || filemtime($cacheClientName) > time() + $ageInSeconds) {
            $contents = file_get_contents($downloadClientData);
            file_put_contents($cacheClientName, $contents);
        }

        if (!file_exists($cacheClientCustom) || filemtime($cacheClientCustom) > time() + $ageInSeconds) {
            $contents = file_get_contents($downloadClientCustomData);
            file_put_contents($cacheClientCustom, $contents);
        }


        $xml = simplexml_load_file($cacheClientName);


        $name = $xml->Client->Name;
        $address = $xml->Client->Address . ' ' . $xml->Client->City;

        // return array($name, $address);

        //return Response::json(array('name' => $name, 'address' => $address));


        $endResult = '<h4>' . $name . '</h4><p>' . $address . '</p>';

        // var_dump($xmlCustom);
        //$dropDown .=  '</select>';


        // $customNode = $xmlCustom->CustomFields->CustomField;

        $xmlCustom = simplexml_load_file($cacheClientCustom);
        $endResult .= '<p>';
        foreach ($xmlCustom->CustomFields->CustomField as $node) {
            $endResult .= '<strong>' . $node->Name . '</strong>: ' . $node->Text . '<br/>';
            //var_dump($node);
        }
        $endResult .= '</p>';

        return $endResult;


        //return $dropDown;

        //return $cacheClientName;*/

    }









}