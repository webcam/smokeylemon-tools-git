<?php

use Smokeylemon\QA\QATools;

class QAToolsController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    protected $layout = 'layouts.master';

    /**
     * Show the user profile.
     */
    public function index()
    {
        //$theDir = Local::getListings();
        //$myErrorLog = '';
        $this->layout->content = View::make('pages.qa_checks');
    }


    public function postSpeedCheck()
    {

        $url = Input::get('domain');
        $getService = new QATools();
        $results = $getService->GTMetrix($url);
        return $results;
    }


    /**
     * @return string
     */
    public function postHTMLValidation()
    {

        $url = Input::get('domain');
        $getService = new QATools();
        $results = $getService->w3cValidation($url);
        return $results;

    }


    public function postIfamely()
    {

        $url = Input::get('domain');
        $getService = new QATools();
        $results = $getService->IFramely($url);
        //return Response::json($results);
        //return $results;

        //$response = Response::json($results);
        //$response->header('Content-Type', 'application/json');
        return $results;
    }


}