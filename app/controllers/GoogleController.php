<?php

use Smokeylemon\Google\SiteAnalyitics;

class GoogleController extends BaseController
{
    protected $layout = 'layouts.master';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function GetTagManager()
    {

        $this->layout->content = View::make('pages.google_tag_manager');
    }

    public function GetAnalyitics()
    {

        $this->layout->content = View::make('pages.google_analyitics_reporting');
    }


    public function DoAnalyitics()
    {

        $data = new SiteAnalyitics();

        //get the csv files first from the web
        $data->retrive();

        //take the files and build up an array of domains availbe.
        $domains = $data->build();

        $data = compact('domains');

        return Response::json($data);

    }

    public function DoGraph()
    {

        $domain = Input::get('domain');

        $data = new SiteAnalyitics();

        $data = $data->generateGraph($domain);

        //$data = compact('data');

        return Response::json($data);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('documentations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return View::make('documentations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return View::make('documentations.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
