<?php

use Smokeylemon\Server\Listings;

class CartlistingsController extends BaseController
{


    /**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.master';

    /**
     * Show the user profile.
     */
    public function index()
    {

        $servers = array(
//            'Tart' => 'Tart',
            'Mint' => 'Mint'
        );


        $data = compact('servers');



        $this->layout->content = View::make('pages.cart_listings')->with($data);

    }

    public function cartList()
    {

        $server = Input::get('server');

        $serverquery = new Listings($server);

        $cmsDetails = $serverquery->getCartSites();

        //dd($cmsDetails);

        return Response::json($cmsDetails);


    }


    // Look in AJAX folder now for cms listings...

}