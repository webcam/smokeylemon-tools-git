<?php

use Smokeylemon\WFM\WFMAccount;
use Smokeylemon\WFM\EditAccount;

class WfmEditAccount extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    protected $layout = 'layouts.master';

    /**
     * Show the user profile.
     */
    public function index()
    {
        $dropdown = WfmTools::getWFMAccounts();
        $this->layout->content =  View::make('pages.wfm_edit_accounts', compact('dropdown'));
    }

    function getWFMAccount($accountID)
    {

        $getAccount = new WFMAccount($accountID);
        $accountData = $getAccount->getWFMAccount();
        $customData = $getAccount->getWFMAccount(true);
        $data = $getAccount->readXMLData($accountData, $customData);
        return $data;

    }

    function postWFMAccount()
    {
        $accountID = Input::get('WFMId');
        $formData = Input::get('form');




        $editAccount = new EditAccount($accountID);
        $data = $editAccount->doAccountEdit($formData);
        return $data;

    }









}