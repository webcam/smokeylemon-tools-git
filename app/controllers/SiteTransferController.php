<?php
use Smokeylemon\Config\initServer;
use Smokeylemon\Server\LiveServer;
use Smokeylemon\Server\Local;
use Smokeylemon\Server\Account;
use Smokeylemon\WFM\WFMAccount;

class SiteTransferController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    protected $layout = 'layouts.master';

    public function index()
    {

        $servers = array(
            //'Tart' => 'Tart',
            'Mint' => 'Mint'
        );

        $localSite = Local::getListings();

        $data = compact('servers', 'localSite');

        $this->layout->content = View::make('pages.site_transfer')->with($data);
    }

    public function getServer()
    {
        $serverquery = Input::get('serverName');

        $server = new LiveServer(initServer::configServer($serverquery));

        $accounts = $server->getMyDomains();

        $accounts = array_values(array_sort($accounts, function ($value) {
            return $value['domain'];
        }));


        return Response::json($accounts);

    }

    public function doDownload()
    {

        /*
         *
         *   Method Live to Local Download
         *
         *   1) Zip up/export database on Production Server
         *   2) Detect cms or cart
         *   3) Rename local cms/cart and Export local cms/cart database to /_site/cms_before_DATESTAMP
         *   4) Create new cms/cart folder and Rsync the live files to the local
         *   5) Create/modify config files to suit local server
         *
         */

        $liveDomainAcct = Input::get('domain');

        $liveServer = Input::get('server');

        $localDir = Input::get('localSite');

        $theAccount = new Account($liveDomainAcct, $liveServer);

        // NB direction MUST be toLocal or toLive!
        $direction = 'toLocal';

        // 1) Zip up/export database on Production Server
        $db = $theAccount->extractImportDB($direction);

        // 2) Detect cms or cart
        $type = $theAccount->cmsOrCart();

        // 3) Rename local cms/cart (with DB amd datestamp) and Create new cms/cart folder
        $move = Local::renameMove($localDir, $type);

        // 4)  Rsync the live files to the local
        if ($move == true) {
            $sync = $theAccount->rSync($type, $localDir, $direction);
        }

        // 5) Create/modify config files to suit local server
        if ($sync == true) {
            $theAccount->changeConfig($localDir, $type, $direction);
        }

        // 6) Import the new DB and delete it
        Local::importSQLDB($localDir, $type);

        //7 ) is there an .htaccess file or an index.html?
        Local::disableHtaccessAndIndex($localDir, $type);

        // 8) Delete the TMP files..
        Local::deleteTmpFiles($localDir, $type);

        // I think we all set up now for local ;)
        if ($db == true && $move == true && $sync == true) {
            $success = true;
        } else {
            dd(debug_backtrace());
        }

        $localPath = 'http://' . $localDir . '.clients.smokeylemon.com/' . $type;

        $result = array();

        // please only return the correct one..

        if (isset($success)) {
            $result['success'] = '<strong>Success!</strong> Please find your site now at <a href="' . $localPath . '" target="_blank">' . $localPath . '</a>';

        } else {
            $result['error'] = '<strong>Oh Uh!</strong> , something went a bit wary!';
        }

        return Response::json($result);


    }

    public function doUpload()
    {

        /*
         *
         *   TODO:  Local to Live Upload
         *
         *   1) Zip up/export database on Production Server
         *   2) Detect cms or cart
         *   3) Rename local cms/cart and Export local cms/cart database to /_site/cms_before_DATESTAMP
         *   4) Create new cms/cart folder and Rsync the live files to the local
         *   5) Create/modify config files to suit local server
         *
         */

        $direction = 'toLive';

        $liveDomainAcct = Input::get('domain');

        $liveServer = Input::get('server');

        $localDir = Input::get('localSite');

        $wfmUserid = Input::get('wfmId');


        /// DEBUGGING MODE E.G ONLY TEST.SMOKEYLEMON.COM ALLOWED!!!!

        if ($liveDomainAcct == 'smoktest') {

            $wfmDetails = new WFMAccount($wfmUserid);

            $password = $wfmDetails->getWHMCustomField(6681, true);

            dd($password);

            $theAccount = new Account($liveDomainAcct, $liveServer);

            // 1) Detect cms or cart
            $type = $theAccount->cmsOrCart();

            //2) backup the current live site!
            $theAccount->backupOldSite();

            // 3) lets change the config file
            $theAccount->changeConfig($localDir, $type, $direction);

            // 4) if there was the old org htacess file, lets change it back
            $theAccount->enableHtaccess($localDir, $type);

            // Zip it all up...

            $zippath = "/home/Clients/$localDir/WWW/_site/site_to_upload";

            $zipfile = $zippath . "/website.zip";

            $whatToZip = "/home/Clients/$localDir/WWW/_site/$type/";

            if (!is_dir($zippath)) {
                mkdir($zippath);
            }

            Local::Zip($whatToZip, $zipfile);

            // 3) Upload the files and set permissions to be friendly to work with
            $sync = $theAccount->rSync($type, $zippath, $direction, true);

            // 4) Unzip it all..
            $theAccount->filemanagerUnzip('website.zip');


            // 5) Import the DB database on Production Server
            $db = $theAccount->extractImportDB($direction, $liveDomainAcct . '_' . $type, $password);

            // 6 Do the config change


// EOF DEBUG
        } else
        {
            $result = 'This is still under devlopment only test account availbe at this time';
            return Response::json($result);

        }


        /* // 3) Rename local cms/cart (with DB amd datestamp) and Create new cms/cart folder
         $move = Local::renameMove($localDir, $type);

         // 4)  Rsync the live files to the local
         if ($move == true) {
             $sync = $theAccount->rSync($type, $localDir);
         }

         // 5) Create/modify config files to suit local server
         if ($sync == true) {
             $theAccount->changeConfig($localDir, $type, 'toLocal');
         }

         // 6) Import the new DB and delete it
         Local::importSQLDB($localDir, $type);

         //7 ) is there an .htaccess file or an index.html?
         Local::disableHtaccessAndIndex($localDir, $type);

         // 8) Delete the TMP files..
         Local::deleteTmpFiles($localDir, $type);

         // I think we all set up now for local ;)
         if ($db == true && $move == true && $sync == true) {
             $success = true;
         } else{
             dd(debug_backtrace());
         }

         $localPath = 'http://' . $localDir . '.clients.smokeylemon.com/' . $type;

         $result = array();

         // please only return the correct one..

         if (isset($success)) {
             $result['success'] = '<strong>Success!</strong> Please find your site now at <a href="' . $localPath . '" target="_blank">' . $localPath . '</a>';

         } else {
             $result['error'] = '<strong>Oh Uh!</strong> , something went a bit wary!';
         }

         return Response::json($result);*/


    }


}