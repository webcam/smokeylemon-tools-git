<?php

use Smokeylemon\Bing\SiteAnalyitics;

class BingController extends BaseController
{
    protected $layout = 'layouts.master';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $this->layout->content = View::make('pages.bing');
    }


    public function DoAnalyitics()
    {

        $data = new SiteAnalyitics();

        $domains = $data->build();

        $trafficStats = array();

        foreach ($domains as $url){

        //$trafficStats[] =  $data->GetRankAndTrafficStats($url);

         $trafficStats[] =  $data->GetAService('GetRankAndTrafficStats',$url);




        }

        dd($trafficStats);

        $data = compact('domains','trafficStats');

        return Response::json($data);

    }

    public function DoGraph()
    {

        $domain = Input::get('domain');

        $data = new SiteAnalyitics();

        $data = $data->generateGraph($domain);

        //$data = compact('data');

        return Response::json($data);

    }





    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('documentations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return View::make('documentations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return View::make('documentations.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
