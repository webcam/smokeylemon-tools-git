<?php

use Smokeylemon\Server\Local;

class DocumentationController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $directoryBase = "/home/smokeylemon/_Job Setup Documents/Stuff for tools dashboard";
        $allowedExt = "txt,pdf,doc";

        $dirs = glob($directoryBase . '/*', GLOB_ONLYDIR);

        $folders = array();

        foreach ($dirs as $dir) {
            $folderName = basename($dir);
            $href = $folderName;
            $direct = array(
                'href' => $href,
                'title' => $folderName,
                'dir' => Local::listDirectoryAsTable($dir, $allowedExt)
            );
            array_push($folders, $direct);
        }

        $data = array(
            'folders' => 'folders'
        );

        return View::make('pages.documentation')->with(compact($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('documentations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return View::make('documentations.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return View::make('documentations.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
