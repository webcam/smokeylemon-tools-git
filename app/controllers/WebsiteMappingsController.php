<?php

use Smokeylemon\Server\Local;
use Smokeylemon\Config\initServer;
use Smokeylemon\Server\LiveServer;
use Smokeylemon\Server\Account;

class WebsiteMappingsController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    protected $layout = 'layouts.master';

    /**
     * Show the user profile.
     */
    public function index()
    {
        //$theDir = Local::getListings();
        $servers = array(
           // 'Tart' => 'Tart',
            'Mint' => 'Mint'
        );
        $myErrorLog = '';
        $localSites = Local::getListings();

        $data = compact('servers', 'localSites', 'myErrorLog');

        $this->layout->content = View::make('pages.website_amppings')->with($data);
    }

    public function exportLocal($dir = false)
    {
        if (!$dir) {
            $siteToExport = htmlspecialchars(Input::get('siteToExport'));
        } else {
            $siteToExport = $dir;
        }
        Local::fixPermissions($siteToExport);
        if (is_dir(Local::buildLocalDir($siteToExport, 'cms'))) {
            Local::deleteTmpFiles($siteToExport);
        }
        $result = Local::exportLocal($siteToExport);
        Local::fixPermissions($siteToExport);
        return $result;
    }

    public function getServer()
    {
        $serverquery = Input::get('serverName');

        $server = new LiveServer(initServer::configServer($serverquery));

        $accounts = $server->getMyDomains();

        $accounts = array_values(
            array_sort(
                $accounts, function ($value) {
                return $value['domain'];
            }
            )
        );


        return Response::json($accounts);

    }

    public function downloadToLocal()
    {

        /*
         *
         *   Method Live to Local Download
         *
         *   1) Zip up/export database on Production Server
         *   2) Detect cms or cart
         *   3) Rename local cms/cart and Export local cms/cart database to /_site/cms_before_DATESTAMP
         *   4) Create new cms/cart folder and Rsync the live files to the local
         *   5) Create/modify config files to suit local server
         *
         */

        // Gather input data
        $liveDomainAcct = Input::get('domain');
        $liveServer = Input::get('server');
        $localDir = Input::get('localSite');
        $liveDomain = Input::get('selectedDomainValue');
        //$accPassword = Input::get('accountPassword');
        $theAccount = new Account($liveDomainAcct, $liveServer);
        // 1) Creaste a TMP dir for storage on live account
        $tmpFolder = '_BU_TMP';
        $tmpFilename = 'cpmove-' . $liveDomainAcct . '.tar.gz';
        $tmpSqlFilename = 'cpmovesql-' . $liveDomainAcct . '.tar.gz';

        // 1) a - $toDir = '/home/Clients/' . $toDir . '/_tmp-from-live-site/';
        $localTmpFolder = '/home/Clients/' . $localDir . '/_tmp-from-live-site/';

        $tmpFullFilePath = $tmpFolder . DIRECTORY_SEPARATOR . $tmpFilename;
        $tmpFullSqlFilePath = $tmpFolder . DIRECTORY_SEPARATOR . $tmpSqlFilename;

        $createTmpFolder = $theAccount->createTmpDirectory($tmpFolder);
        // 1) Backup all files/db on Production Server to Tmp Storage
        if ($createTmpFolder == 'Success') {
            $doFileBackup = $theAccount->backupLiveDirectory($tmpFullFilePath);
        }
        if ($doFileBackup == 'Success') {
            $type = $theAccount->cmsOrCart();
            $doSqlBackup = $theAccount->backupLiveSQL($tmpFullSqlFilePath, $type);
        }
        // 2) Download all gzip files/db on Production Server to Local
        if ($doSqlBackup == 'Success') {
            $getWebsiteFiles = $theAccount->downloadBackupFile($liveDomain, $tmpFullFilePath, $localDir);
            $getDBFile = $theAccount->downloadBackupFile($liveDomain, $tmpFullSqlFilePath, $localDir, true);
            if (($getWebsiteFiles == 'Success') && ($getDBFile == 'Success')) {
                // we have downloaded, so lets delete them..
                $deleteTmpFolder = $theAccount->deleteTmpDirectory($tmpFolder);
            }
        }
        // 4) Rename current working local cms/cart (with DB amd datestamp) and Create new cms/cart folder
        if ($deleteTmpFolder) {
            // move the current LOCAL copy TO a BACKUP
            $moveLocalToBackup = Local::moveLocalToBackup($localDir, $type);
            // move the current LIVE version TO the LOCAL setup
            $moveLiveToLocal = Local::moveLiveToLocal($localTmpFolder, $type);
        }

        // 5) extract the files
        if (($moveLocalToBackup == true) && ($moveLiveToLocal == true)) {
            $unzipFile = Local::extractFile($localDir, $tmpFilename, $type);
            // and cleanup the gz files..
            Local::deleteGzFiles($localDir, $type, $tmpFilename, $tmpSqlFilename);
        }
        // 6) Create/modify config files to suit local server
        if ($unzipFile == 'Success') {
            $theAccount->changeConfig($localDir, $type, 'toLocal');
        }

        // 6) Import the new DB and delete it
        //$db = Local::importSQLDB($localDir, $type);

        //7 ) is there an .htaccess file or an index.html?
        Local::disableHtaccessAndIndex($localDir, $type);

        // 8) Delete the TMP files..
        Local::deleteTmpFiles($localDir);

        // I think we all set up now for local ;)
        if ($db == true && $move == true) {
            $success = true;
        } else {
            dd(debug_backtrace());
        }

        $localPath = 'http://' . $localDir . '.smokey.rocks/' . $type;

        $result = array();

        // please only return the correct one..

        if (isset($success)) {
            $result['success'] = '<strong>Success!</strong> Please find your site now at <a href="' . $localPath
                . '" target="_blank">' . $localPath . '</a>';

        } else {
            $result['error'] = '<strong>Oh Uh!</strong> , something went a bit wary!';
        }

        return Response::json($result);


    }

    public function uploadToLive()
    {

        $result = array();
        /*
         *
         *   TODO:  Local to Live Upload
         *
         *   1) Zip up/export database on Local Server
         *   2) Detect cms or cart
         *   3) Rename local cms/cart and Export local cms/cart database to /_site/cms_before_DATESTAMP
         *   4) Create new cms/cart folder and send the local files to the live
         *   5) Create/modify config files to suit live server
         *
         */

        // Gather the input data..
        $liveDomainAcct = Input::get('domain');
        $liveServer = Input::get('server');
        $localDir = Input::get('localSite');
        $accPassword = Input::get('accountPassword');

        /// DEBUGGING MODE E.G ONLY TEST.SMOKEYLEMON.COM ALLOWED!!!!

        if ($liveDomainAcct == 'foobaz') {

            $theAccount = new Account($liveDomainAcct, $liveServer);
            // 1) Detect cms or cart
            $type = $theAccount->cmsOrCart();
            //2) backup the current live site!
            $theAccount->backupCurrentLiveSite();
            // 3) lets change the config file
            $theAccount->changeConfig($localDir, $type, 'toLive');
            // 4) if there was the old org htacess file, lets change it back
            $theAccount->enableHtaccess($localDir, $type);
            // Zip it all up...
            $this->exportLocal($localDir);

            date_default_timezone_set('NZ');
            $exportDir = Local::buildLocalDir($localDir) . 'site_to_upload_' . date('Y_m_d_H_i_s');
            $websiteZip = $exportDir . '/website.tar.gz';
            $websiteSqlZip = $exportDir . '/database.sql.gz';

            // on the live site, we need to upload the zip
            if ((file_exists($websiteZip)) && (file_exists($websiteSqlZip))) {
                if ($type == 'cms') {
                    $theAccount->makeTmpFolder($accPassword);
                }
                // Destroy existing files and database..
                $theAccount->deletePublicHtml('true');
                // Upload the new website to the account
                $theAccount->sendFilesToLiveSite($accPassword, array($websiteZip, $websiteSqlZip));
                // Unzip the files to the www path
                $theAccount->filemanagerUnzip(basename($websiteZip));
                // Import the new DB file
                $theAccount->importToLiveSQL(basename($websiteSqlZip), $accPassword, $type);
            }
            $success = true;
        } else {
            $result = 'This is still under development only test account available at this time';

        }
        // please only return the correct one..

        if (isset($success)) {
            $result['success'] = '<strong>Success!</strong> Please find your site is live';

        } else {
            $result['error'] = '<strong>Oh Uh!</strong> , something went a bit wary!';
        }

        return Response::json($result);


    }


}