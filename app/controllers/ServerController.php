<?php
use Smokeylemon\Config\initServer;
use Smokeylemon\Server\LiveServer;
use Smokeylemon\Server\CreateAccount;


class ServerController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    protected $layout = 'layouts.master';

    /**
     * Show the user profile.
     */
    public function index()
    {

        $servers = array(
//            'Tart' => 'Tart',
            'Mint' => 'Mint'
        );

        $data = compact('servers');

        $this->layout->content = View::make('pages.server_status')->with($data);

    }

    public function service()
    {
        $serverquery = Input::get('serverName');
        $serviceRequest = Input::get('service');

        $server = new LiveServer(initServer::configServer($serverquery));

        $sql = $server->service($serviceRequest);

        return $sql;
        //return Response::json($sql);

    }

    public function pageload()
    {
        $serverquery = Input::get('serverName');

        $server = new LiveServer(initServer::configServer($serverquery));

        $sql = $server->loadAverage();

        return $sql;

        //return Response::json($sql);

    }

    public function getCreateAnAccount()
    {

        $wfmaccounts = WfmTools::getWFMAccounts('Workflow Max Account');

        $servers = array(
            //'Tart' => 'Tart',
            'Mint' => 'Mint'
        );

        $data = compact('servers', 'wfmaccounts');

        $this->layout->content = View::make('pages.create_account')->with($data);

    }

    public function postCreateAnAccount()
    {

        $serverquery = Input::get('serverName');
        $domain = Input::get('domain');
        $username = Input::get('username');
        $email = Input::get('email');

        $account = new CreateAccount($serverquery);

        $data = $account->doAccountCreate($username, $domain, $email);

        return Response::json($data);
    }

    public function postRestartServices()
    {
        $serverquery = Input::get('server');

        $server = new LiveServer(initServer::configServer($serverquery));

        $servicePosted = Input::get('service');

        $result = $server->restartService($servicePosted);

        return $result;

        //dd($serverquery);
    }


}