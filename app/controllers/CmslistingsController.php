<?php

use Smokeylemon\Server\Listings;

class CmslistingsController extends BaseController
{


    /**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.master';

    /**
     * Show the user profile.
     */
    public function index()
    {

        $servers = array(
//            'Tart' => 'Tart',
            'Mint' => 'Mint'
        );


        $data = compact('servers');



        $this->layout->content = View::make('pages.cms_listings')->with($data);

    }

    public function cmsList()
    {

        $server = Input::get('server');

        $serverquery = new Listings($server);

        $cmsDetails = $serverquery->getCmsSites();

        //dd($cmsDetails);

        return Response::json($cmsDetails);


    }


    // Look in AJAX folder now for cms listings...

}