<?php

namespace Ajax;

use BaseController;
use ServerTools;
use WfmTools;
use MiscTools;
use Request;
use Redirect;
use Illuminate\Support\Facades\Response;
use Input;
use Smokeylemon\Server;
use Smokeylemon\Config;

use Smokeylemon\WFM\Accounts;

class AjaxController extends BaseController
{

    function __construct()
    {
        $this->beforeFilter('ajaxOnly');
    }

    public function getCmsSites()
    {
        $theServer = Config\initServer::configServer('Tart');

        $server = new Server\LiveServer($theServer);


        $latest = $server->getData('http://dev.cmsmadesimple.org/latest_version.php');

        $LatestMajorVersion = substr($latest['message'], -6);
        $LatestMajorVersionConversion = number_format((float)$LatestMajorVersion,3);

        $LMV = $LatestMajorVersionConversion;

        $urlsToTest = $server->getMyDomains();

        foreach ($urlsToTest as $urlUnderTest) {


            $result = $server->getData('www.' . $urlUnderTest . '/version.php', false);
            if ($result['status'] === true) {
                $result = $server->getData('www.' . $urlUnderTest . '/vcheck.php', true);
                $sitesWithCMS[$urlUnderTest] = str_replace('VERSION:', '', $result['message']);
            }
        }
        $result = '';
        foreach ($sitesWithCMS as $domain => $message) {
            if ($message != '<b>Check not found</b>') {
                $result .= $domain . ' : ' . $message;
            }
        }


        $data = array(
            'LMV' => $LMV,
            'Res' => $sitesWithCMS
        );

        return Response::json($data);

    }

    function getWFMAccount($accountID)
    {

        $getAccount = new WfmTools($accountID);
        //$getAccount->getWFMAccounts();
        $theName = $getAccount->accountName();

        return $theName;
        //return $accountID;
        // return $getAccount;

        /*


// This file just grabs the all the availbe job data and caches it

        $BaseDir = public_path() . '/WFMcache';

        $clientid = $accountID;

//Jobs List
        $downloadClientData = 'https://api.workflowmax.com/client.api/get/' . $clientid . '?apiKey=' . WfmTools::$APIKey . '&accountKey=' . WfmTools::$AccKey . '&detailed=true';
        $cacheClientName = $BaseDir . '/resultClient_' . $clientid . '.xml';

        $downloadClientCustomData = 'https://api.workflowmax.com/client.api/get/' . $clientid . '/customfield?apiKey=' . WfmTools::$APIKey . '&accountKey=' . WfmTools::$AccKey;
        $cacheClientCustom = $BaseDir . '/resultClient_Custom' . $clientid . '.xml';


// generate the cache version if it doesn't exist or it's too old!
        $ageInSeconds = 1800; // twelve hours


        if (!file_exists($cacheClientName) || filemtime($cacheClientName) > time() + $ageInSeconds) {
            $contents = file_get_contents($downloadClientData);
            file_put_contents($cacheClientName, $contents);
        }

        if (!file_exists($cacheClientCustom) || filemtime($cacheClientCustom) > time() + $ageInSeconds) {
            $contents = file_get_contents($downloadClientCustomData);
            file_put_contents($cacheClientCustom, $contents);
        }


        $xml = simplexml_load_file($cacheClientName);


        $name = $xml->Client->Name;
        $address = $xml->Client->Address . ' ' . $xml->Client->City;

        // return array($name, $address);

        //return Response::json(array('name' => $name, 'address' => $address));


        $endResult = '<h4>' . $name . '</h4><p>' . $address . '</p>';

        // var_dump($xmlCustom);
        //$dropDown .=  '</select>';


        // $customNode = $xmlCustom->CustomFields->CustomField;

        $xmlCustom = simplexml_load_file($cacheClientCustom);
        $endResult .= '<p>';
        foreach ($xmlCustom->CustomFields->CustomField as $node) {
            $endResult .= '<strong>' . $node->Name . '</strong>: ' . $node->Text . '<br/>';
            //var_dump($node);
        }
        $endResult .= '</p>';

        return $endResult;


        //return $dropDown;

        //return $cacheClientName;*/

    }

    public function getServerStatus()
    {

        $statusResult = ServerTools::getServerAPIRequest('loadavg');

        $result = json_decode($statusResult);

        $endResult = $result->one . '&nbsp;' . $result->five . '&nbsp;' . $result->fifteen;

        return $endResult;

        // returns the JSON result..


    }

    /*
    // Date Variables
            $h = date('G');
            $m = date('i');
            $d = date('l');


            $timeNow = date('Y-m-d H:i:s');

            $mysqli = new mysqli('localhost', 'root', 'smokey2-7588909', 'smokey_tools');

            /* check connection */
    /*       if (mysqli_connect_errno()) {
              printf("Connect failed: %s\n", mysqli_connect_error());
              exit();
          }

          $queryTimes = "SELECT * FROM sms WHERE ID = 1";
          $resultTime = $mysqli->query($queryTimes);
          $rowTime = $resultTime->fetch_assoc();
  // then to output
          $smsSent = $rowTime['time'];


  //$endTime =  date('Y-m-d H:i:s', strtotime('+5 minutes'));

          $sendTime = strtotime(date("Y-m-d H:i:s", strtotime($smsSent)) . " + 1 minutes");

          $sendTime = date("Y-m-d H:i:s", $sendTime);


  //echo 'DEBUGGING - <br/>SMS Last Sent: '.$smsSent .'<br/> Current Time: '.$timeNow .' <br/>Send another SMS  at Time: '.$sendTime;

          $load = floatval($result->five);
  // if between the hours of 6am to 11pm then...
          if ($h >= 6 && $h <= 23) {

              // if server load higher than xxx ...
              //if ($result->five > 2.80)
              if ($load > 3.50) { //if the SMS sent in the MYSQL was later than 15 mintutes ago ($endtime) ..
                  if ($sendTime <= $timeNow) {


                      //update the databse with a new timestamp of the time now..
                      $stmt = $mysqli->prepare("UPDATE `sms` SET
                                      `time` = ?,
                                      `load` = ?
                                      WHERE `ID` = 1");
                      $stmt->bind_param('sd', $timeNow, $load);
                      if (!$stmt->execute()) {
                          die("Could not execute<br />");
                      }

                      $stmt->execute();

                      $stmt->close();
                      //and send another SMS..
                      sendMessage();
                  }
              }
          }
          $mysqli->close();*/

    function fixLocal()
    {

        $siteType = null;

        if (Input::get('siteType') != 'select') {
            $siteType = htmlspecialchars(Input::get('siteType'));
        }


        if (!is_null($siteType)) {
            exec('/home/SmokeyTools/bin/run_command.py fixpermissions ' . $siteType);
            return '<p><strong>Fix Completed</strong></p>';

        } else {

            return '<p><strong>Please type in the Client Name or Select the correct Type</strong></p>';

        }
    }

    function createLocal()
    {

        $clientPosted = null;

        if ((Input::post('clientName') != '') && (Input::post('siteType') != 'select')) {
            $clientPosted = htmlspecialchars(Input::post('clientName'));
            $typePosted = Input::post('siteType');
        }


        if (!is_null($clientPosted)) {

            if ($typePosted == 'cms') {
                $frontURL = 'http://' . $clientPosted . '.smokey.rocks/cms/';
                $adminURL = 'http://' . $clientPosted . '.smokey.rocks/cms/cmsadmin/';
            } else {
                $frontURL = 'http://' . $clientPosted . '.smokey.rocks/cart/';
                $adminURL = 'http://' . $clientPosted . '.smokey.rocks/cart/cartadmin/';
            }


            if ($typePosted == 'cms') {

                exec('/home/SmokeyTools/bin/addcmsPHP ' . $clientPosted . ' AuthPHPSciptFromSmokeyTools');
                $endResult = '<p><strong>Setup Completed</strong></p>';
                $endResult .= "<p>Your Frontend URL is: <br/><a target='_blank' href='" . $frontURL . "'> $frontURL</a><br/>";
                $endResult .= "<p>Your Admin URL is:  <br/><a target='_blank' href='" . $adminURL . "'> $adminURL</a><br/>";

                return $endResult;

            } else {

                exec('/home/SmokeyTools/bin/addopencartPHP ' . $clientPosted . ' AuthPHPSciptFromSmokeyTools');
                $endResult = '<p><strong>Setup Completed</strong></p>';
                $endResult .= "<p>Your Frontend URL is:  <br/><a target='_blank' href='" . $frontURL . "'> $frontURL</a><br/>";
                $endResult .= "<p>Your Admin URL is:  <br/><a target='_blank' href='" . $adminURL . "'> $adminURL</a><br/>";

                return $endResult;
            }

        } else {

            return '<p><strong>Please select the site type or type in client directory</strong></p>';

        }

    }

    function serverServices()
    {

        $statusResult = ServerTools::getServerAPIRequest('servicestatus');

        $result = json_decode($statusResult);


        $serviceRequest = Input::get('service');

        if ($serviceRequest == 'mysql') {

            //13 is the mysql JSON DOM element...
            $selectedStatus = $result->service[13]->enabled;


            if ($selectedStatus == 1) {

                return '<span class="label label-success">Running</span>';

            } else {

                return 'Not Enabled';
            }


        }


        if ($serviceRequest == 'httpd') {
            //17 is the httpd JSON DOM element...
            $selectedStatus = $result->service[17]->enabled;

            if ($selectedStatus == 1) {

                return '<span class="label label-success">Running</span>';

            } else {

                return 'Not Enabled';
            }


        }


    }

}