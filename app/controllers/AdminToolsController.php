<?php

use Smokeylemon\Server\Local;
use Smokeylemon\Config\initServer;
use Smokeylemon\Server\LiveServer;
use Smokeylemon\Server\Account;

class AdminToolsController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    protected $layout = 'layouts.master';

    /**
     * Show the user profile.
     */
    public function index()
    {
        //$theDir = Local::getListings();
        $servers = array(
//            'Tart' => 'Tart',
            'Mint' => 'Mint'
        );
        $myErrorLog = '';
        $localSites = Local::getListings();

        $data = compact('servers', 'localSites', 'myErrorLog');

        $this->layout->content = View::make('pages.admin_tools')->with($data);
    }

    public function createLocal()
    {
        $clientPosted = null;
        $clientName = Input::get('clientName');

        $clientName = str_replace(' ', '-', $clientName); // Replaces all spaces with hyphens.
        $clientName = preg_replace('/[^A-Za-z0-9\-]/', '', $clientName); // Removes special chars.
        $clientName = strtolower($clientName); // Convert to lowercase

        $typePosted = Input::get('siteType');

        $urlPosted = Input::get('liveURL');

        if ($clientName != ''
            && in_array(
                $typePosted, array('client', 'cms', 'opencart', 'opencart2', 'mobile', 'website', 'silverstripe')
            )
        ) {

            if (strlen($clientName) <= 8 && ctype_alpha($clientName)) {

                $localDir = '/home/Clients/' . $clientName . '/WWW/_site';

                if (is_dir($localDir . '/cms') || is_dir($localDir . '/cart') || is_dir($localDir . '/cart2')) {

                    $result = Local::archiveLocal($clientName);

                    if ($result && $result != '<p><strong>Please fix file permissions first</strong></p>'
                        && $result != '<p>Not Supported for this website</p>'
                        && $result != '<p><strong>Please select the site</strong></p>'
                    ) {
                        $clientPosted = htmlspecialchars($clientName);
                        $response = Local::createLocal($clientPosted, $typePosted, $urlPosted);
                        Local::fixPermissions($clientName);
                        if ($typePosted == 'cms') {
                            Local::deleteTmpFiles($clientName, $typePosted);
                        }
                        return Response::json($result . $response);
                    } else {
                        return Response::json($result);
                    }
                } else {
                    $clientPosted = htmlspecialchars($clientName);
                    $response = Response::json(Local::createLocal($clientPosted, $typePosted, $urlPosted));
                    Local::fixPermissions($clientName);
                    if ($typePosted == 'cms') {
                        Local::deleteTmpFiles($clientName, $typePosted);
                    }
                    return $response;
                }

            } else {

                return Response::json(
                    '<p><strong>Client names cannot exceed 8 character limit and must contain only alphabetic characters!</strong></p>'
                );

            }


        } else {

            return Response::json('<p><strong>Please enter Client Name and select Type</strong></p>');

        }

    }

    public function importClient()
    {
        $siteToImport = htmlspecialchars(Input::get('siteToImport'));
        Local::fixPermissions($siteToImport);
        $result = Local::importClient($siteToImport);
        Local::fixPermissions($siteToImport);
        return $result;
    }

    /*public function uploadStaging()
    {
        $siteToUpload = htmlspecialchars(Input::get('siteToUpload'));
        $result = Local::uploadStaging($siteToUpload);
        return $result;
    }*/

    public function exportLocal($dir = false)
    {
        if (!$dir) {
            $siteToExport = htmlspecialchars(Input::get('siteToExport'));
        } else {
            $siteToExport = $dir;
        }
        Local::fixPermissions($siteToExport);
        if (is_dir(Local::buildLocalDir($siteToExport, 'cms'))) {
            Local::deleteTmpFiles($siteToExport);
        }
        $result = Local::exportLocal($siteToExport);
        Local::fixPermissions($siteToExport);
        return $result;
    }

    public function archiveLocal()
    {
        $siteToArchive = htmlspecialchars(Input::get('siteToArchive'));
        Local::fixPermissions($siteToArchive);
        $result = Local::archiveLocal($siteToArchive);
        Local::fixPermissions($siteToArchive);
        return $result;
    }

    public function removeLocal()
    {
        $clientToRemovePass = htmlspecialchars(Input::get('clientToRemovePass'));
        $clientToRemove = htmlspecialchars(Input::get('clientToRemove'));

        if ($clientToRemovePass == 'lime2015smokeyrock$') {
            Local::fixPermissions($clientToRemove);

            $result = Local::removeLocal($clientToRemove);

            return $result;
        } else {
            return '<small class="error">Error! Incorrect pass phrase used!' . $clientToRemove . ', '
            . $clientToRemovePass . '</small>';
        }
    }

    public function getServer()
    {
        $serverquery = Input::get('serverName');

        $server = new LiveServer(initServer::configServer($serverquery));

        $accounts = $server->getMyDomains();

        $accounts = array_values(
            array_sort(
                $accounts, function ($value) {
                return $value['domain'];
            }
            )
        );


        return Response::json($accounts);

    }



}