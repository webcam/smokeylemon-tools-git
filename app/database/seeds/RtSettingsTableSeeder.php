<?php

class RtSettingsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		 DB::table('rt_settings')->truncate();

		$rt_settings = array(
            'db_username' => Crypt::encrypt('root'),
            'db_password' => Crypt::encrypt('smokey2-7588909')
		);

		// Uncomment the below to run the seeder
		 DB::table('rt_settings')->insert($rt_settings);
	}

}
