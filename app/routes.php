<?php
//require_once "../vendor/dropbox/dropbox-sdk/lib/Dropbox/autoload.php";
//use \Dropbox as dbx;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Home page..

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));

Route::get('overview/getServerStatus', 'Ajax\AjaxController@getServerStatus');

// CMS Lisitngs..


Route::get('cmslistings/getCmsSites', 'Ajax\AjaxController@getCmsSites');

Route::get('cmslistings', array('as' => 'cmsListings', 'uses' => 'CmslistingsController@index'));
Route::get('cmsList', array('as' => 'cmsList', 'uses' => 'CmslistingsController@cmsList'));

Route::get('cartlistings', array('as' => 'cartListings', 'uses' => 'CartlistingsController@index'));
Route::get('cartList', array('as' => 'cartList', 'uses' => 'CartlistingsController@cartList'));


// WFM Accounts..

Route::get('wfmaccounts', array('as' => 'wfmaccounts', 'uses' => 'WfmController@index'));

Route::get('wfmaccounts/{id?}', array('as' => 'getwfmaccounts', 'uses' => 'WfmController@getWFMAccount'));

Route::get('WfmEditAccount', array('as' => 'WfmEditAccount', 'uses' => 'WfmEditAccount@index'));

Route::post('postWFMAccount', array('as' => 'postWFMAccount', 'uses' => 'WfmEditAccount@postWFMAccount'));

// Admin Tools

Route::get('admintools', array('as' => 'admintools', 'uses' => 'AdminToolsController@index'));

Route::get('qatools', array('as' => 'qatools', 'uses' => 'QAToolsController@index'));
Route::post('postSpeedCheck', array('as' => 'postSpeedCheck', 'uses' => 'QAToolsController@postSpeedCheck'));
Route::post('postIfamely', array('as' => 'postIfamely', 'uses' => 'QAToolsController@postIfamely'));
Route::post(
    'postHTMLValidation', array('as' => 'postHTMLValidation', 'uses' => 'QAToolsController@postHTMLValidation')
);
Route::get('doThis', array('as' => 'doThis', 'uses' => 'QAToolsController@doThis'));

//Route::post('createlocal', 'Ajax\AjaxController@createLocal');
Route::post('createLocal', array('as' => 'createLocal', 'uses' => 'AdminToolsController@createLocal'));
Route::post('importClient', array('as' => 'importClient', 'uses' => 'AdminToolsController@importClient'));

Route::post('downloadToLocal', array('as' => 'doDownload', 'uses' => 'WebsiteMappingsController@downloadToLocal'));
Route::post('uploadToLive', array('as' => 'doUpload', 'uses' => 'WebsiteMappingsController@uploadToLive'));



/*Route::post('upload-staging', array('as' => 'upload-staging', 'uses' => 'AdminToolsController@uploadStaging'));*/
Route::post('fixlocal', 'Ajax\AjaxController@fixLocal');
Route::post('export-local', array('as' => 'export-local', 'uses' => 'AdminToolsController@exportLocal'));
Route::post('archive-local', array('as' => 'archive-local', 'uses' => 'AdminToolsController@archiveLocal'));
Route::post('remove-local', array('as' => 'remove-local', 'uses' => 'AdminToolsController@removeLocal'));

Route::get('getServer', array('as' => 'getServer', 'uses' => 'WebsiteMappingsController@getServer'));

// Server Tools..

Route::get('server', array('as' => 'server', 'uses' => 'ServerController@index'));
Route::get('service', array('as' => 'service', 'uses' => 'ServerController@service'));
Route::get('pageload', array('as' => 'pageload', 'uses' => 'ServerController@pageload'));
Route::get('serverservices', array('as' => 'serverServices', 'uses' => 'Ajax\AjaxController@serverServices'));
Route::post('RestartServices', array('as' => 'RestartServices', 'uses' => 'ServerController@postRestartServices'));

// Account Tools..
Route::get('create-an-account', array('as' => 'create-an-account', 'uses' => 'ServerController@getCreateAnAccount'));
Route::post('create-an-account', array('as' => 'create-an-account', 'uses' => 'ServerController@postCreateAnAccount'));

/*Route::get('transfer', array('as' => 'transfer', 'uses' => 'SiteTransferController@index'));
Route::get('getServer', array('as' => 'getServer', 'uses' => 'SiteTransferController@getServer'));
Route::post('doDownload', array('as' => 'doDownload', 'uses' => 'SiteTransferController@doDownload'));
Route::post('doUpload', array('as' => 'doUpload', 'uses' => 'SiteTransferController@doUpload'));*/

Route::get('documentation', array('as' => 'documentation', 'uses' => 'DocumentationController@index'));

Route::get('wiki', array('as' => 'wiki', 'uses' => 'WikiController@index'));

// SEO...
Route::get('GoogleTagManager', array('as' => 'GoogleTagManager', 'uses' => 'GoogleController@GetTagManager'));
// Google anylitics
Route::get('GoogleAnalyitics', array('as' => 'GoogleAnalyitics', 'uses' => 'GoogleController@GetAnalyitics'));
Route::get('DoAnalyitics', array('as' => 'DoAnalyitics', 'uses' => 'GoogleController@DoAnalyitics'));
Route::get('DoGraph', array('as' => 'DoGraph', 'uses' => 'GoogleController@DoGraph'));

// Bing anylitics
Route::get('bing', array('as' => 'bing', 'uses' => 'BingController@index'));
Route::get('DoAnalyiticsBing', array('as' => 'DoAnalyiticsBing', 'uses' => 'BingController@DoAnalyitics'));

Route::get('DoGraphBing', array('as' => 'DoGraphBing', 'uses' => 'BingController@DoGraph'));

//Website Mappings Controller
Route::get('website-mappings', array('as' => 'website-mappings', 'uses' => 'WebsiteMappingsController@index'));



/*
Route::get('sdktest', function () {
        # Include the Dropbox SDK libraries


        $appInfo = dbx\AppInfo::loadFromJsonFile(app_path().'/config/dropbox.json');

        $webAuth = new dbx\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");

        $authorizeUrl = $webAuth->start();

        //dd($authorizeUrl);

        echo "1. Go to: " . $authorizeUrl . "\n";
        echo "2. Click \"Allow\" (you might have to log in first).\n";
        echo "3. Copy the authorization code.\n";
        $authCode = \trim(\readline("Enter the authorization code here: "));

        list($accessToken, $dropboxUserId) = $webAuth->finish($authCode);
        print "Access Token: " . $accessToken . "\n";

        $dbxClient = new dbx\Client($accessToken, "PHP-Example/1.0");
        $accountInfo = $dbxClient->getAccountInfo();



        $f = fopen("working-draft.txt", "rb");
        $result = $dbxClient->uploadFile("/working-draft.txt", dbx\WriteMode::add(), $f);
        fclose($f);
        print_r($result);

        $folderMetadata = $dbxClient->getMetadataWithChildren("/");
        print_r($folderMetadata);

        $f = fopen("working-draft.txt", "w+b");
        $fileMetadata = $dbxClient->getFile("/working-draft.txt", $f);
        fclose($f);
        print_r($fileMetadata);
    }*/
//);